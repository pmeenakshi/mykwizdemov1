"use strict";

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

const { App } = require("jovo-framework");
const { Alexa } = require("jovo-platform-alexa");
const { GoogleAssistant } = require("jovo-platform-googleassistant");
const { JovoDebugger } = require("jovo-plugin-debugger");
const { FileDb } = require("jovo-db-filedb");
const clj_fuzzy = require("clj-fuzzy");
const app = new App();
app.use(new Alexa(), new GoogleAssistant(), new JovoDebugger(), new FileDb());

var request = require("request");
var Database = require("./db/dbwrapper");
var constants = require("./constants/constants");
var util = require("./inc/util");
var arrayFunctions = require("./inc/ArrayFunctions");
var help = require("./inc/help");
var functionCalls = require("./inc/functionCalls");
var RepeatText = "<s>Please Repeat!</s> ";
var skillExit = ". Thank you For Using Pop Quiz.";
var goodByeMsg = 'Good Bye, see you soon';
var topicName = [];
var questionData = [];
var topicCounter = 0;
var lastIndex = 0;
var nextQuesIndex;
var answerText = [];
var challengLevelSlotValue = "";
let title = '';
let content = '';
let imageUrl = '';
var MqQuestioncount = 0;

// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------

Database.execute = function (callback) {
  const database = new Database();
  return callback(database).then(
    result => database.close().then(() => result),
    err =>
      database.close().then(() => {
        throw err;
      })
  );
};

app.setHandler(
  // require('./modularisedCode/modularCode'),
  // require('./modularisedCode/modularCode2'),
  // require('./modularisedCode/modularCode3'),
  // require('./modularisedCode/modularCode4'),
  // require('./modularisedCode/modularCode5')
  {
    LAUNCH: async function () {
      console.log("Launch intent");
      // this.setSessionAttribute('IntentState', "LAUNCH");
      this.setSessionAttribute("state", constants.states.MAIN);
      this.setSessionAttribute("UserState", constants.UserState.UserSelected);
      this.setSessionAttribute("ChallengeLevelIntent", "");
      this.setSessionAttribute("Topic", "");
      await this.toIntent("IdentifyUser");
    },

    IdentifyUser: async function () {
      console.log("Identify user");
      console.log("Intent state checking -> " + this.getSessionAttribute("IntentState"));
      console.log('  IntentState  ---> ' + this.getSessionAttribute("IntentState"));
      console.log('  state  ---> ' + this.getSessionAttribute("state"));
      console.log('  UserState  ---> ' + this.getSessionAttribute("UserState"));
      var msg, speechOutput, repromptText;
      if (
        help.isValidContext(
          this.getSessionAttribute("IntentState"),
          this.getSessionAttribute("state"),
          this
        )
      ) {
        msg = help.isValidContext(
          this.getSessionAttribute("IntentState"),
          this.getSessionAttribute("state"),
          this
        );
        console.log("TopicIntent isValidContext msg -> " + msg);
        this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
      } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
        this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
        this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
        var txt = help.GetContexBasedHelp(
          this.getSessionAttribute("state"),
          this.getSessionAttribute("UserState"),
          this
        );
        console.log("Help me text -> " + txt);
        console.log("User State-> " + this.getSessionAttribute("UserState"));
        console.log("Quiz State-> " + this.getSessionAttribute("state"));
        this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
        this.getSessionAttribute("IntentState") === "RepeatOptions") &&
        this.getSessionAttribute("state") === "_QUESTIONANSWER") {
        var opt = this.getSessionAttribute("optionsChoice");
        console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
        speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
        repromptText = speechOutput;
        this.ask(speechOutput, repromptText, repromptText, goodByeMsg);
      }
      else {
        var speechOut, repromptText;
        if (this.getSessionAttribute("IntentState") === "alreadyTakenTest") {
          this.setSessionAttribute("TopicId", null);
          speechOut = "<prosody> Hey " +
            this.getSessionAttribute("userName") +
            "! You have already been taken this test. You can't access it again. " +
            "To start the Quiz again, say <s>Open and topic Name</s> or say list topics, or, " +
            "If you want to Switch, say switch user!</prosody>";
          repromptText = speechOut;
        }
        else {
          console.log(
            "gET User details : " +
            this.$request.getDeviceName() +
            " \n deviceID : " +
            this.$user.getId()
          );
          this.setSessionAttribute("IntentState", "IdentifyUser");
          this.setSessionAttribute("state", constants.states.MAIN);
          this.setSessionAttribute("UserState", constants.UserState.UserSelected);
          this.setSessionAttribute("ChallengeLevelIntent", "");
          this.setSessionAttribute("Topic", "");

          this.setSessionAttribute("deviceID", this.$user.getId());

          var queryToCheckUserIdExistOrNot =
            "select endUser.userId, endUser.firstName, endUser.userName, uniqueCode, roleName, courseId, section, organizationId from endUser left join userDevices on endUser.userId = userDevices.endUserId left join endUser_xRef on endUser_xRef.userId = endUser.userId where userDevices.userDeviceId ='" +
            this.$user.getId() +
            "'";

          console.log("DEVICE ID -> " + queryToCheckUserIdExistOrNot);
          var rows = await functionCalls.callAPI(queryToCheckUserIdExistOrNot);
          console.log(rows.length);

          if (rows.length > 0) {
            this.setSessionAttribute("DeviceFound", "YES");
            console.log(rows[0]);
            this.setSessionAttribute("userName", rows[0].firstName);
            this.setSessionAttribute("UserEmail", rows[0].userName);
            this.setSessionAttribute("UserId", rows[0].userId);
            this.setSessionAttribute("roleName", rows[0].roleName);
            this.setSessionAttribute("courseId", rows[0].courseId);
            this.setSessionAttribute("section", rows[0].section);
            this.setSessionAttribute("organizationId", rows[0].organizationId);

            var boom = "<audio src='https://s3.amazonaws.com/sounds226/boom.mp3'/>";
            if (!this.getSessionAttribute("organizationId") ||
              this.getSessionAttribute("organizationId") === null ||
              this.getSessionAttribute("organizationId") === undefined) {
              speechOut =
                "<prosody> Hey " +
                this.getSessionAttribute("userName") +
                "! Welcome back. Start Quiz, by saying <s>Open and topic Name</s> or say list topics, " +
                " or, If you want to Switch, say switch user. You can also play with friends, " +
                " to enable multiplayer quiz, say multiuser.!</prosody> " +
                boom;
            }
            else {
              speechOut =
                "<prosody> Hey " +
                this.getSessionAttribute("userName") +
                "! Welcome back. Start Quiz, by saying <s>Open and topic Name</s> or say list topics, " +
                " or, If you want to Switch, say switch user.</prosody> " +
                boom;
            }
            repromptText = speechOut;
          } else {
            this.setSessionAttribute("DeviceFound", "NO");
            speechOut = "Say your PIN number by saying my pin is and the PIN number";
            repromptText = speechOut;
          }
        }
        console.log(speechOut, repromptText);
        this.ask(speechOut, repromptText, repromptText, goodByeMsg);  //Prompt changes meenu
      }
    },

    async UnhandledIdentifyUser() {
      this.ask(
        "Looks like that was an Invalid pin!, Please say, my pin is, and your pin"
      );
    },

    IdentifyUserByPin: async function () {
      console.log('  IntentState  ---> ' + this.getSessionAttribute("IntentState"));
      console.log('  state  ---> ' + this.getSessionAttribute("state"));
      console.log('  UserState  ---> ' + this.getSessionAttribute("UserState"));
      var msg, speechOutput, repromptText;
      if (
        help.isValidContext(
          this.getSessionAttribute("IntentState"),
          this.getSessionAttribute("state"),
          this
        )
      ) {
        msg = help.isValidContext(
          this.getSessionAttribute("IntentState"),
          this.getSessionAttribute("state"),
          this
        );
        console.log("TopicIntent isValidContext msg -> " + msg);
        this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
      } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
        this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
        this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
        var txt = help.GetContexBasedHelp(
          this.getSessionAttribute("state"),
          this.getSessionAttribute("UserState"),
          this
        );
        console.log("Help me text -> " + txt);
        console.log("User State-> " + this.getSessionAttribute("UserState"));
        console.log("Quiz State-> " + this.getSessionAttribute("state"));
        this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
        this.getSessionAttribute("IntentState") === "RepeatOptions") &&
        this.getSessionAttribute("state") === "_QUESTIONANSWER") {
        var opt = this.getSessionAttribute("optionsChoice");
        console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
        speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
        repromptText = speechOutput;
        this.ask(speechOutput, repromptText, repromptText, goodByeMsg);
      }
      else {
        console.log("start IdentifyUserByPin");
        var pin = this.$inputs.pin.value;
        const variablePinToTest = pin;
        this.setSessionAttribute("variablePinToTest", variablePinToTest);
        var queryString =
          "select endUser.userId, firstName, endUser.userName, uniqueCode, roleName, courseId, section, organizationId from endUser left join userDevices on endUser.userId = userDevices.endUserId left join endUser_xRef on endUser_xRef.userId = endUser.userId where userDevices.uniqueCode =" +
          pin;
        console.log("Select user Query -> " + queryString);

        let rows = await functionCalls.callAPI(queryString);
        if (rows.length > 0 && rows) {
          console.log("Rows data -> " + rows);
          // uniqueCode = rows;

          //If User Device Id is not registered with any PIN then update the device ID with associated PIN
          if (this.getSessionAttribute("IntentState") !== "SwitchUser") {
            if (this.getSessionAttribute("DeviceFound") === "YES")
              var insertDeviceIDQuery =
                "UPDATE `userDevices` SET `userDeviceId` = '" +
                this.$user.getId() +
                "' WHERE (`uniqueCode` = " +
                pin +
                " and `userDeviceId` is null)";
            else
              var insertDeviceIDQuery =
                "UPDATE `userDevices` SET `userDeviceId` = '" +
                this.$user.getId() +
                "' WHERE (`uniqueCode` = " +
                pin +
                ")";
            console.log("Insert device Id " + insertDeviceIDQuery);
            let resp = await functionCalls.callAPI(insertDeviceIDQuery);
            console.log(resp, resp[0]);
            console.log("Affected rows -> " + resp.affectedRows);
          }

          this.setSessionAttribute("IntentState", "IdentifyUserByPin");
          this.setSessionAttribute("state", constants.states.MAIN);
          this.setSessionAttribute("UserState", constants.UserState.UserSelected);
          this.setSessionAttribute("ChallengeLevelIntent", "");
          this.setSessionAttribute("Topic", "");

          for (var i = 0; i < rows.length; i++) {
            console.log("Rows Id : " + rows[i].userId);
            if (parseInt(pin) === parseInt(rows[i].uniqueCode)) {
              this.setSessionAttribute("userName", rows[i].firstName);
              this.setSessionAttribute("UserEmail", rows[i].userName);
              this.setSessionAttribute("UserId", rows[i].userId);
              this.setSessionAttribute("roleName", rows[0].roleName);
              this.setSessionAttribute("courseId", rows[0].courseId);
              this.setSessionAttribute("section", rows[0].section);
              this.setSessionAttribute("organizationId", rows[0].organizationId);

              //speechOutput = `Thank you ` + this.attributes['userName'] + ` To Start Say, Open, and, the Topic name or you could say, Help me, for help  or ` + constants.HowToUsePrompt;
              if (this.getSessionAttribute("IntentState") === "SwitchUser") {
                if (!this.getSessionAttribute("organizationId") ||
                  this.getSessionAttribute("organizationId") === null ||
                  this.getSessionAttribute("organizationId") === undefined) {
                  speechOutput =
                    `<prosody rate="` +
                    constants.speachSpeed +
                    `">Hey ` +
                    this.getSessionAttribute("userName") +
                    `! , Welcome,  To Start Say, Open, and, the Topic name, or, List Topics, ` +
                    `or, If you want to Switch, say switch user. You can also play with friends, ` +
                    `to enable multiplayer quiz, say multiuser!</prosody> `;
                } else {
                  speechOutput =
                    `<prosody rate="` +
                    constants.speachSpeed +
                    `">Hey ` +
                    this.getSessionAttribute("userName") +
                    `! , Welcome,  To Start Say, Open, and, the Topic name, or, List Topics, ` +
                    `or, If you want to Switch, say switch user.</prosody> `;
                }
                repromptText =
                  `<prosody rate="` +
                  constants.speachSpeed +
                  `">Again!, To Start Say, Open, and, the Topic name, or, List Topics!</prosody>`;
              } else {
                if (!this.getSessionAttribute("organizationId") ||
                  this.getSessionAttribute("organizationId") === null ||
                  this.getSessionAttribute("organizationId") === undefined) {
                  speechOutput =
                    `<prosody rate="` +
                    constants.speachSpeed +
                    `">Hey ` +
                    this.getSessionAttribute("userName") +
                    `! , Welcome,  To Start Say, Open, and, the Topic name, or, List Topics, or, If you want to Switch, ` +
                    `say switch user. You can also play with friends, ` +
                    `to enable multiplayer quiz, say multiuser!</prosody> `;
                } else {
                  speechOutput =
                    `<prosody rate="` +
                    constants.speachSpeed +
                    `">Hey ` +
                    this.getSessionAttribute("userName") +
                    `! , Welcome,  To Start Say, Open, and, the Topic name, or, List Topics, or, If you want to Switch, ` +
                    `say switch user.</prosody> `;
                }
                repromptText =
                  `<prosody rate="` +
                  constants.speachSpeed +
                  `">Again!, To Start Say, Open, and, the Topic name, or, List Topics, or, If you want to Switch, say switch user!</prosody>`;
              }
              this.setSessionAttribute("UserState", "UserSelected");
              // this.handler.state = constants.states.MAIN;
            }
          }
        } else {
          speechOutput =
            "Looks like that was an Invalid pin!, Please say, my pin is, and your pin";
          repromptText = "Looks like that was an Invalid pin!, Please say, my pin is, and your pin";
        }
        this.ask(speechOutput, repromptText, repromptText, 'will meet again');  //Prompt changes meenu
      }
    },

    SwitchUser: function () {
      console.log("Switching user");
      console.log('  IntentState  ---> ' + this.getSessionAttribute("IntentState"));
      console.log('  state  ---> ' + this.getSessionAttribute("state"));
      console.log('  UserState  ---> ' + this.getSessionAttribute("UserState"));
      var msg, speechOut, repromptText;
      if (this.getSessionAttribute("UserState") === "QuestionsRead") {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("switchuser isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOut = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptText = speechOut;
          this.ask(speechOut, repromptText, repromptText, goodByeMsg);
        }
      }
      else {
        this.setSessionAttribute("IntentState", "SwitchUser");
        speechOut = "Please tell me the PIN number";
        repromptText = speechOut;
        this.ask(speechOut, repromptText, repromptText, 'Will meet you again');
      }
    },
    NewSession: async function () {
      // Check for User Data in Session Attributes
      console.log("onboardingStateHandlers : NewSession");

      //this.attributes['UserState'] = "NewSession";
      this.setSessionAttribute("UserState", constants.UserState.NewSession);
      // this.setSessionAttribute('IntentState', 'NewSession');

      console.log("DeviceId -> " + this.getIdToken());

      await this.toIntent("AMAZON.StartOverIntent");
    },

    "AMAZON.StartOverIntent": function () {
      console.log(
        "Current Intent Handler : " + this.getSessionAttribute("IntentState")
      );
      if (
        this.getSessionAttribute("IntentState") &&
        this.getSessionAttribute("IntentState") !== undefined
      )
        return this.toIntent("Unhandled");
      this.setSessionAttribute("IntentState", "AMAZON.StartOverIntent");
      this.setSessionAttribute("state", constants.states.MAIN);
      this.setSessionAttribute("UserState", constants.UserState.UserSelected);
      this.setSessionAttribute("ChallengeLevelIntent", "");
      this.setSessionAttribute("Topic", "");

      // var boom = "<audio src='https://s3.amazonaws.com/sounds226/boom.mp3'/>";
      // this.ask(
      //   "Hey! Start Quiz, by saying <s>Open and topic Name</s>or say list topics" +
      //     boom
      // );
      this.toIntent('IdentifyUser');
    },

    TopicIntent: async function () {
      console.log('intentstate -> ' + this.getSessionAttribute('IntentState'));
      console.log("Starting mainStateHandler:TopicIntent");
      // this.setSessionAttribute("IntentState", "TopicIntent");
      var repromptSpeech = "";
      var speechOutput = "";
      var topic = this.$inputs.topic_name.value;
      console.log("User requested Topic ->" + topic);
      var msg;
      console.log("Userstate -> " + this.getSessionAttribute("UserState"));
      console.log("Cuurrent State : " + this.getSessionAttribute('currentState'));
      console.log("State : " + this.getSessionAttribute('state'));

      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          this.setSessionAttribute("IntentState", "TopicIntent");
          // this.setSessionAttribute('Topic', topic);
          var NewSession = false;
          // var topic = this.getInput();
          if (this.getSessionAttribute("IntentState") == "NewSession") {
            NewSession = true;
          }
          var SelectedTopic = "";
          // var repromptSpeech = "";
          // var speechOutput = "";
          var updatedIntent = this.getIntentName();
          var slotToElicit;
          var ChallengeLevelCount = 0;
          var currentChallengeLevel = "";
          // var userTopicPrompt = this.getInputs(topic);
          // var topic = "";
          var NewSessionFlow = false;
          console.log("mainStateHandlers:TopicIntent start ");
          var userTopicPrompt = "";

          // if (this.getIntentName()) {
          if (topic) {
            userTopicPrompt = "%" + topic + "%";
            //     topic = topic;
            console.log("User topic  -> " + userTopicPrompt);
          }
          // }
          // else {
          //     NewSessionFlow = true
          // }

          var availableChallengeLevel;
          var topicFound = false;
          var topicName = "";
          var similarity = "";
          var maxSimilarity = "";
          var availableChallengeLevel = "";
          // var uID = 1063;
          let rows;
          // console.log('Query String -> ' + "call SP_GET_TOPICS_DIALOGFLOW ('1083','" + userTopicPrompt + "')");

          // console.log("userTopicPrompt -> "+userTopicPrompt)
          let query =
            "call SP_ListOfTopics_alexa ('" +
            this.getSessionAttribute("UserEmail") +
            "', '" +
            userTopicPrompt +
            "')";
          console.log("Query String -> " + query);
          // Database.execute(
          //     database => database.query("call SP_READ_TOPICS_2018_ALEXA ('" + userTopicPrompt + "')")
          //         // database => database.query("call SP_GET_TOPICS_DIALOGFLOW ('1083','" + userTopicPrompt + "')")
          //         .then(r => {
          //             rows = r;
          //         })
          // ).then(() => {

          // }).catch(err => {
          //     console.log('DB Error: ', err);
          //     this.tell(constants.errorPrompt);
          // });
          rows = await functionCalls.callAPI(query);
          this.setSessionAttribute("listTopicsLength", rows.length);
          console.log("Response rows -> " + rows[0].length);   //Bug reported

          if (!rows.length) {
            speechOutput =
              "No topics found in your library, please subscribe topics on myQuiz dot com account ";
            repromptSpeech = speechOutput;
          } else {
            var multipleTopic;
            for (var i = 0; i < rows[0].length; i++) {
              i === 0
                ? (multipleTopic = false)
                : multipleTopic == true
                  ? (multipleTopic = true)
                  : rows[0][i].name === rows[0][i - 1].name
                    ? (multipleTopic = false)
                    : (multipleTopic = true);

              i === 0
                ? (topicName += rows[0][i].name)
                : rows[0][i].name === rows[0][i - 1].name
                  ? ""
                  : (topicName += ` <s>` + rows[0][i].name + " </s> ");
            }
            //If the user has not subscribed any topics
            var queryToCheckSubscribedTopic = "SELECT * FROM EduVoiceDB.userTopic_xRef where isSubscribed = 1 && userId = " +
              this.getSessionAttribute("UserId");
            var resultRows = await functionCalls.callAPI(queryToCheckSubscribedTopic);
            if (resultRows.length) {
              console.log("multipleTopic ->" + multipleTopic);
              console.log("topicName ->" + topicName);
              if (multipleTopic && topic != "") {
                this.getSessionAttribute("TopicId");
                topicCounter = 3;
                speechOutput =
                  topic +
                  " is not part of your subscribed topics. Please go and subscribe the topics, or,  Say <s> Open and the Topic Name</s> or say - list topics to get other topics";
                repromptSpeech =
                  " Say <s> Open and the Topic Name</s>, Options are " +
                  topicName +
                  constants.HelpPrompt;
              }
              // else if (multipleTopic && NewSession == true) {
              //     speechOutput = this.getSessionAttribute('speechOutput') + ' Say <s> Open and the Topic Name</s>, Or Say Find topics ';
              //     repromptSpeech = ' Say <s> Open and the Topic Name</s>, Options are ' + topicName;
              // }
              else if (multipleTopic && NewSession == false) {
                topicCounter = 3;
                speechOutput =
                  "Available Topics are " +
                  topicName +
                  " Say <s> Open and the Topic Name</s> or say - more topics to get other topics";
                repromptSpeech =
                  " Say <s> Open and the Topic Name</s>, Options are " + topicName;
              } else {
                for (var i = 0; i < rows[0].length; i++) {
                  if (i === 0) {
                    this.setSessionAttribute("storeResultOnlyOnce", "Yes")
                  }
                  console.log(topic + "---" + rows[0][i].name);
                  console.log("TEST TYPE ---- " + rows[0][i] + ", " + rows[0]);
                  // console.log(clj_fuzzy.phonetics.soundex(topic) == clj_fuzzy.phonetics.soundex(rows[0][i].name));
                  // console.log(clj_fuzzy.phonetics.soundex(topic) == clj_fuzzy.phonetics.soundex(rows[0][i].name));
                  // if ((rows[0][i].name.toString().toLowerCase() === topic.toString().toLowerCase()) ||
                  //     (clj_fuzzy.phonetics.soundex(topic) == clj_fuzzy.phonetics.soundex(rows[0][i].name))) {
                  maxSimilarity = util.similar(topic, rows[0][i].name);
                  topicFound = true;
                  this.setSessionAttribute("TopicId", rows[0][i].topicId);
                  this.setSessionAttribute("testType", rows[0][i].testTypeCode);

                  console.log(
                    "Topic ID -> " +
                    this.getSessionAttribute("TopicId") +
                    " TEST type : " +
                    this.getSessionAttribute("testType")
                  );
                  ///////Meenu
                  var testResultLength = 0;
                  // var testTypeCode = this.getSessionAttribute("testType");
                  var queryToCheckOneTimeTest = "select * FROM EduVoiceDB.topic where topicId= " +
                    this.getSessionAttribute("TopicId") + " and oneTimeTest = 'yes' ";
                  var queryToCheckOneTimeTestResult = await functionCalls.callAPI(queryToCheckOneTimeTest);
                  console.log(' queryToCheckOneTimeTest => ' + queryToCheckOneTimeTestResult.length);
                  if (queryToCheckOneTimeTestResult.length > 0) {
                    var queryString = "select userId from testResult where userId=" + this.getSessionAttribute("UserId") + " and topicId= " + this.getSessionAttribute("TopicId");
                    var queryStringRows = await functionCalls.callAPI(queryString);
                    testResultLength = queryStringRows.length;
                    this.setSessionAttribute('oneTimeTest', 'Yes');
                  }
                  // else {
                  else {
                    this.setSessionAttribute('oneTimeTest', 'No');
                  }
                  console.log('testResultLength -> ' + testResultLength);
                  if (testResultLength > 0) {
                    this.setSessionAttribute("IntentState", "alreadyTakenTest");
                    await this.toIntent("IdentifyUser");
                    // this.tell("You already have taken this test. You can't access it now.");
                  }
                  else {
                    this.setSessionAttribute("Topic", rows[0][i].name);
                    console.log(
                      "Topic Name -> " +
                      this.getSessionAttribute("Topic") +
                      " Percent Match -> " +
                      maxSimilarity
                    );
                    SelectedTopic = rows[0][i].name;
                    // no challenge level for one time test:-
                    var sum = 0;
                    console.log('Length of the count -> ' + rows[0].length); //to check the length
                    for (var m = 0; m < rows[0].length; m++) {
                      sum = rows[0][m].count + sum;
                    }
                    console.log("ChallengeLevelCount " + ChallengeLevelCount);
                    if (this.getSessionAttribute('oneTimeTest') === "Yes") {
                      console.log('sum -> ' + sum);
                      if (sum >= constants.minNumberOfQuestionsToStartTest) {
                        availableChallengeLevel += `<s>Starter</s>`;
                        ChallengeLevelCount = 1;
                        currentChallengeLevel = "S";
                        console.log(' manually given challenge level -> ');
                      }
                    } else if (this.getSessionAttribute("MultiplayerQuizIntent")) {
                      console.log("ChallengeLevelCount " + ChallengeLevelCount);
                      if (this.getSessionAttribute("numberOfPlayers") === 2) {
                        if (sum >= 10) {
                          availableChallengeLevel += `<s>Starter</s>`;
                          ChallengeLevelCount = 1;
                          currentChallengeLevel = "S";
                        } else {
                          this.ask("You don't have enough questions to start this quiz, please choose another topic or else choose less number of people.");
                        }
                      } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                        console.log("number of questions -> " + sum);
                        console.log("ChallengeLevelCount " + ChallengeLevelCount);
                        if (sum >= 15) {
                          availableChallengeLevel += `<s>Starter</s>`;
                          ChallengeLevelCount = 1;
                          currentChallengeLevel = "S";
                        } else {
                          this.ask("You don't have enough questions to start this quiz, please choose another topic or else choose less number of people.");
                        }
                        console.log("ChallengeLevelCount " + ChallengeLevelCount);
                      } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                        if (sum >= 20) {
                          availableChallengeLevel += `<s>Starter</s>`;
                          ChallengeLevelCount = 1;
                          currentChallengeLevel = "S";
                        } else {
                          this.ask("You don't have enough questions to start this quiz, please choose another topic or else choose less number of people.");
                        }
                      }
                    }
                    else {
                      if (
                        rows[0][i].challenge == "S" &&
                        rows[0][i].count >= constants.minNumberOfQuestionsToStartTest
                      ) {
                        availableChallengeLevel += `<s>Starter</s>`;
                        ChallengeLevelCount += 1;
                        currentChallengeLevel = "S";
                        this.setSessionAttribute("enoughQuesForStarter", "Yes");
                        console.log("currentChallengeLevel = S");
                      }
                      if (
                        rows[0][i].challenge == "I" &&
                        rows[0][i].count >= constants.minNumberOfQuestionsToStartTest
                      ) {
                        availableChallengeLevel += `<s>Intermediate</s>`;
                        ChallengeLevelCount += 1;
                        currentChallengeLevel = "I";
                        this.setSessionAttribute("enoughQuesForIntermediate", "Yes");
                        console.log("currentChallengeLevel = I");
                      }
                      if (
                        rows[0][i].challenge == "A" &&
                        rows[0][i].count >= constants.minNumberOfQuestionsToStartTest
                      ) {
                        availableChallengeLevel += `<s>Advanced</s>`;
                        ChallengeLevelCount += 1;
                        currentChallengeLevel = "A";
                        this.setSessionAttribute("enoughQuesForAdvanced", "Yes");
                        console.log("currentChallengeLevel = A");
                      }
                    }
                  }
                  console.log("currentChallengeLevel -> " + currentChallengeLevel);
                  if (topicName.length > 0) {
                    if (!SelectedTopic) {
                      console.log("Topic not found ");
                      // app.setSessionAttribute('Topic', '');
                      this.setSessionAttribute("AvailableTopics", topicName);
                      console.log(
                        "Topics at the end of topic intent ->" +
                        this.getSessionAttribute("AvailableTopics")
                      );
                      speechOutput =
                        "No matches found for <s> " +
                        topic +
                        "</s> Available Topics are : " +
                        topicName +
                        " say <s>open</s> and name of the  Topic";
                      (repromptSpeech =
                        "Available Topics are <s>" +
                        topicName +
                        "</s>, say <s>open</s> and, topic name! "),
                        "You could also say exit to end";
                    } else {
                      console.log("SelectedTopic -> " + SelectedTopic);
                      this.setSessionAttribute("availableChallengeLevels", availableChallengeLevel);
                      speechOutput =
                        "Available challenge Level " +
                        availableChallengeLevel +
                        " Say <s> Select </s> and the <s>Challenge level</s>";
                      repromptSpeech =
                        " Say <s> Select </s> and the <s>Challenge level</s>, Options are " +
                        availableChallengeLevel;
                      this.setSessionAttribute("UserState", constants.UserState.TopicNameSelected);
                      this.setSessionAttribute("state", constants.states.MAIN);
                      console.log("challengelevelcount " + ChallengeLevelCount);
                      if (ChallengeLevelCount == 1) {
                        // this.setSessionAttribute('ChallengeLevelContains', 'Yes');
                        this.setSessionAttribute(
                          "UserState",
                          constants.UserState.ChallangeLevelSelected
                        );
                        // this.handler.state = constants.states.QUESTIONANSWER;
                        this.setSessionAttribute(
                          "state",
                          constants.states.QUESTIONANSWER
                        );

                        this.setSessionAttribute(
                          "challengeLevelInd",
                          currentChallengeLevel
                        );
                        console.log("challengelevelcount after " + ChallengeLevelCount);
                        await this.toIntent("populateQuestions");
                      }
                    }
                  } else {
                    speechOutput =
                      "No matches found for <s> " +
                      topic +
                      ". please go and subscribe for the topics";
                    repromptSpeech = speechOutput;
                  }
                }
              }
              console.log("Completing Topic Intent");
            }
            else {
              speechOutput =
                "No topics found in your library, please subscribe topics on myQuiz dot com account ";
              repromptSpeech = speechOutput;
            }
            if (!topicFound || ChallengeLevelCount > 1 || multipleTopic == true) {
              this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
            }
          }
        }
      }
    },

    populateQuestions: async function () {
      this.setSessionAttribute("IntentState", "populateQuestions");
      var ansData = [];
      var randomQuestions = [];
      var data;
      console.log(
        "populateQuestions handler : Populate Questions for " +
        this.getSessionAttribute("Topic")
      );
      var topic = this.getSessionAttribute("Topic");
      var testTopic = this.getSessionAttribute("Topic");
      var qId = [], qWeightage = [], qChallengeLevel = [];
      var speechOutput = "", repromptSpeech = "", images = this;
      //If topic selected is not available return a friendly message.

      //Printing the event request
      //Setting null to the session attribute variables
      this.setSessionAttribute("correctAnsweredQuestionId", "");
      this.setSessionAttribute("wrongAnsweredQuestionId", "");
      this.setSessionAttribute("notAttemptedQuestionId", "");
      this.setSessionAttribute("correctAnsweredWeightage", "");
      this.setSessionAttribute("wrongAnsweredWeightage", "");
      this.setSessionAttribute("notAttemptedQuestionWeightage", "");
      //First time load set questionCount = 0
      //Get questions for the sleected Topic
      this.setSessionAttribute("TestIndex", 0);
      this.setSessionAttribute("currentQuestionIndex", 0);
      this.setSessionAttribute("correctAnswerScore", 0);
      this.setSessionAttribute("challengeAdvanceCountQuestions", 0);
      this.setSessionAttribute("challengeAdvanceCountCorrectAns", 0);
      this.setSessionAttribute("challengeAdvanceCountWrongAns", 0);
      this.setSessionAttribute("challengeAdvanceCountNotAttemptedAns", 0);
      this.setSessionAttribute("challengeIntermediateCountQuestions", 0);
      this.setSessionAttribute("challengeIntermediateCountCorrectAns", 0);
      this.setSessionAttribute("challengeIntermediateCountWrongAns", 0);
      this.setSessionAttribute("challengeIntermediateCountNotAttemptedAns", 0);
      this.setSessionAttribute("challengeStarterCountQuestions", 0);
      this.setSessionAttribute("challengeStarterCountCorrectAns", 0);
      this.setSessionAttribute("challengeStarterCountWrongAns", 0);
      this.setSessionAttribute("challengeStarterCountNotAttemptedAns", 0);
      var count = 0;

      //setting initial values for Multiplayer quiz 
      this.setSessionAttribute("firstPlayerTotalQue", 0)
      this.setSessionAttribute("firstPlayerCorrectAnsQue", 0)
      this.setSessionAttribute("firstPlayerWrongAnsQue", 0)
      this.setSessionAttribute("firstPlayerSkipedQue", 0)

      this.setSessionAttribute("secondPlayerTotalQue", 0)
      this.setSessionAttribute("secondPlayerCorrectAnsQue", 0)
      this.setSessionAttribute("secondPlayerWrongAnsQue", 0)
      this.setSessionAttribute("secondPlayerSkipedQue", 0)

      this.setSessionAttribute("thirdPlayerTotalQue", 0);
      this.setSessionAttribute("thirdPlayerCorrectAnsQue", 0);
      this.setSessionAttribute("thirdPlayerWrongAnsQue", 0);
      this.setSessionAttribute("thirdPlayerSkipedQue", 0);

      this.setSessionAttribute("fourthPlayerTotalQue", 0);
      this.setSessionAttribute("fourthPlayerCorrectAnsQue", 0);
      this.setSessionAttribute("fourthPlayerWrongAnsQue", 0);
      this.setSessionAttribute("fourthPlayerSkipedQue", 0);

      this.setSessionAttribute("firstPlayerSet",
        this.getSessionAttribute("firstPlayerName") +
        ", " + this.getSessionAttribute("firstPlayerTotalQue") +
        ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
        ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
        ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      );

      this.setSessionAttribute("secondPlayerSet",
        this.getSessionAttribute("secondPlayerName") +
        ", " + this.getSessionAttribute("secondPlayerTotalQue") +
        ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
        ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
        ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      );

      this.setSessionAttribute("thirdPlayerSet",
        this.getSessionAttribute("thirdPlayerName") +
        ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
        ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
        ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
        ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
      );

      this.setSessionAttribute("fourthPlayerSet",
        this.getSessionAttribute("fourthPlayerName") +
        ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
        ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
        ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
        ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
      );

      // var queryString =
      //   'call SP_accessQuestion_alexa("' +
      //   this.getSessionAttribute("TopicId") +
      //   '", "' +
      //   this.getSessionAttribute("challengeLevelInd") +
      //   '","0","' +
      //   constants.questionsToRead +
      //   '")';

      var numberOfQuestionsToAccess = constants.questionsToRead;
      this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
      if (this.getSessionAttribute("MultiplayerQuizIntent")) {
        if (this.getSessionAttribute("MultiplayerQuizIntent") &&
          this.getSessionAttribute("numberOfPlayers") === 2) {
          numberOfQuestionsToAccess = 10;
          this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
          this.setSessionAttribute("quizType", "twoPlayerQuiz");
          this.setSessionAttribute("thirdPlayerSet", "");
          this.setSessionAttribute("fourthPlayerSet", "");
        } else if (this.getSessionAttribute("MultiplayerQuizIntent") &&
          this.getSessionAttribute("numberOfPlayers") === 3) {
          numberOfQuestionsToAccess = 15;
          this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
          this.setSessionAttribute("quizType", "threePlayerQuiz");
          this.setSessionAttribute("fourthPlayerSet", "");
        } else if (this.getSessionAttribute("MultiplayerQuizIntent") &&
          this.getSessionAttribute("numberOfPlayers") === 4) {
          numberOfQuestionsToAccess = 20;
          this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
          this.setSessionAttribute("quizType", "fourPlayerQuiz");
        }
        var queryString =
          'call SP_accessQuestion_alexa("' +
          this.getSessionAttribute("TopicId") +
          '", "' +
          this.getSessionAttribute("challengeLevelInd") +
          '","' +
          numberOfQuestionsToAccess +
          '")';
      } else {
        this.setSessionAttribute("quizType", "singlePlayerQuiz");
        this.setSessionAttribute("firstPlayerSet", "");
        this.setSessionAttribute("secondPlayerSet", "");
        this.setSessionAttribute("thirdPlayerSet", "");
        this.setSessionAttribute("fourthPlayerSet", "");
        var queryString =
          'call SP_accessQuestion_alexa_Dev_2018("' +
          this.getSessionAttribute("TopicId") +
          '", "' +
          this.getSessionAttribute("challengeLevelInd") +
          '","0","' +
          numberOfQuestionsToAccess +
          '")';
      }

      // Database.execute(
      //     database => database.query(queryString)
      //         .then(r => {
      //             rows = r;
      //         })
      // ).then(() => {

      // }).catch(err => {
      //     console.log('DB Error in NewSession Request: ', err);
      //     this.tell(constants.errorPrompt);
      // });

      var rows = await functionCalls.callAPI(queryString);

      data = rows[0];
      this.setSessionAttribute("questionsTaken", data.length);
      for (var i in data) {
        qId[i] = data[i].questionId;
        qWeightage[i] = data[i].weightage;
        qChallengeLevel[i] = data[i].challenge;
        if (qChallengeLevel[i] === "S") {
          this.setSessionAttribute(
            "challengeStarterCountQuestions",
            this.getSessionAttribute("challengeStarterCountQuestions") + 1
          );
        } else if (qChallengeLevel[i] === "I") {
          this.setSessionAttribute(
            "challengeIntermediateCountQuestions",
            this.getSessionAttribute("challengeIntermediateCountQuestions") + 1
          );
        } else {
          this.setSessionAttribute(
            "challengeAdvanceCountQuestions",
            this.getSessionAttribute("challengeAdvanceCountQuestions") + 1
          );
        }
        console.log("Loading questioNs : " + data[i]);
      }
      console.log("starter -> " + this.getSessionAttribute("challengeStarterCountQuestions"));
      console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountQuestions"));
      console.log("advanced -> " + this.getSessionAttribute("challengeAdvanceCountQuestions"));

      var maxQuestionID = arrayFunctions.maxAaaryValue(qId);
      console.log("Max Question Id-> " + maxQuestionID);
      console.log("Qiestion Id " + qId);
      console.log("Questions weightage " + qWeightage);
      //Add o session attribute
      this.setSessionAttribute("maxQuestionID", maxQuestionID);
      questionData = arrayFunctions.shuffleArray(data);
      this.setSessionAttribute("questions", questionData);
      console.log("Loaded qsTns : " + questionData);
      if ((questionData[0].externalReference) !== null &&
        (questionData[0].externalReference) !== undefined) {
        if ((questionData[0].externalReference).includes('.png') ||
          (questionData[0].externalReference).includes('.jpg')) {
          console.log(' text -> ' + questionData[0].text);
          console.log(' text -> ' + questionData[0].questionId);
          this.setSessionAttribute('ImageContainer', 'Yes');
          console.log('image8');
        } else if ((questionData[0].externalReference).includes('.mp3')) {
          console.log('AudioContainer1');
          this.setSessionAttribute('AudioContainer', 'Yes');
        }
      }
      //Reading the first Question from the multi dimentional array.
      //1 Read the questions
      if (this.getSessionAttribute("MultiplayerQuizIntent")) {
        this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
        speechOutput =
          `Starting Quiz for <s>` +
          testTopic +
          ` </s>  Question for <s> ` +
          this.getSessionAttribute("currentPlayerName") +
          ` </s>  <prosody rate="` +
          constants.speachSpeed +
          `">` +
          questionData[0].text +
          "  <s> Options are </s>";
      } else {
        if (this.getSessionAttribute('ImageContainer') === 'Yes') {
          // let title = '';
          // let content = '';
          console.log('image2');
          console.log(" image url -> " + questionData[0].externalReference);
          imageUrl = questionData[0].externalReference;
          images = this.showImageCard(title, content, imageUrl);
          // this.showImageCard(title, content, imageUrl).ask(questionData[0].text);
          speechOutput =
            `Starting  Quiz for <s>` +
            testTopic +
            ` </s>  Question, <prosody rate="` +
            constants.speachSpeed +
            `">` +
            questionData[0].text +
            ", <s> Options are </s>";
          // this.setSessionAttribute('ImageContainer', 'No');
        } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
          console.log('AudioContainer2');
          speechOutput =
            `Starting  Quiz for <s>` +
            testTopic +
            ` </s>  Question, <prosody rate="` +
            constants.speachSpeed +
            `">` +
            questionData[0].text +
            "<audio src= '" + questionData[0].externalReference + "'/>" +
            // this.showImageCard(title, content, imageUrl)  +
            " <s> Options are </s>";
          // this.setSessionAttribute('AudioContainer', 'No');
        }
        else {
          speechOutput =
            `Starting  Quiz for <s>` +
            testTopic +
            ` </s>  Question, <prosody rate="` +
            constants.speachSpeed +
            `">` +
            questionData[0].text +
            ", <s> Options are </s>";
        }
      }

      //1 Read the options
      //Read the option and output them
      this.getSessionAttribute("currentState", "populateQuestions");
      var rowData = JSON.parse("[" + questionData[0].options + "]");
      var opt = " ";
      for (var key = 0; key < rowData.length; key++) {
        var dt = rowData[key];
        for (var i in dt) {
          if (dt[i] != "") {
            speechOutput =
              speechOutput +
              (`<s>` +
                i +
                `</s>` +
                " - " +
                dt[i] +
                ',<break time="' +
                constants.pauseTime +
                '"/> ');
            opt = opt + i + `,  ` + ',<break time="' +
              constants.pauseTime +
              '"/> ';
            answerText[i] = dt[i];
          }
        }
      }
      speechOutput += `</prosody>`;
      this.setSessionAttribute("optionsChoice", opt);
      console.log("Questoin -> " + speechOutput);

      this.setSessionAttribute("score", 0);
      this.setSessionAttribute("correctAnswerText", questionData[0].answer);
      this.setSessionAttribute("questionId", questionData[0].questionId);
      this.setSessionAttribute("questionWeightage", questionData[0].weightage);
      this.setSessionAttribute("challengeLevelOfEachQue", questionData[0].challenge); //To know the challenge level of 1st question
      console.log("Qid - > " + this.getSessionAttribute("questionId"));
      console.log("Qweightage - > " + this.getSessionAttribute("questionWeightage"));
      console.log("challengeLevelOfEachQue - > " + this.getSessionAttribute("challengeLevelOfEachQue"));
      this.setSessionAttribute("UserState", constants.UserState.QuestionsRead);
      this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
      // this.ask(speechOutput);
      //meenu changes session
      console.log('Userstate -> ' + this.getSessionAttribute("ChallengeLevelContains"));
      console.log('storeResultOnlyOnce -> ' + this.getSessionAttribute("storeResultOnlyOnce"));
      if (this.getSessionAttribute('ChallengeLevelContains') !== "Yes" ||
        this.getSessionAttribute('ChallengeLevelContains') === undefined) {
        if (this.getSessionAttribute("storeResultOnlyOnce") === "Yes" ||
          this.getSessionAttribute('storeResultOnlyOnce') === undefined) {
          this.setSessionAttribute("storeResultOnlyOnce", "No")
          console.log('organisation id -> ' + this.getSessionAttribute("organizationId"));
          console.log('organisation id -> ' + this.getSessionAttribute("courseId"));
          console.log("starter -> " + this.getSessionAttribute("challengeStarterCountQuestions"));
          console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountQuestions"));
          console.log("advanced -> " + this.getSessionAttribute("challengeAdvanceCountQuestions"));
          if (!this.getSessionAttribute("organizationId") && !this.getSessionAttribute("courseId")) {
            this.setSessionAttribute("organizationId", null);
            this.setSessionAttribute("courseId", null);
          }
          var sessionId = "this.getsessionId()";
          var date = new Date();
          var queryStringResult =
            'insert into EduVoiceDB.testResult (userId, topicId, questionsTaken, correctAnswer, totalScore, ' +
            'challengeHighCountQuestions, challengeHighCountCorrectAns, challengeHighCountWrongAns,challengeHighCountNotAttemptedAns, challengeIntermediateCountQuestions, ' +
            'challengeIntermediateCountCorrectAns, challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns, challengeStarterCountQuestions, ' +
            'challengeStarterCountCorrectAns, challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, correctQuestionId, wrongQuestionId, notAttemptedQuestionId, ' +
            'correctQuestion_Weightage, wrongQuestion_Weightage, notAttemptedQuestion_Weightage, createdDate, sessionId, ' +
            'attemptedQuestions, courseId, organizationId, section, testTypeCode, roleName, firstPlayerSet, ' +
            'secondPlayerSet, thirdPlayerSet, fourthplayerSet, quizType) values ("' +
            this.getSessionAttribute("UserId") +
            '",' +
            this.getSessionAttribute("TopicId") +
            "," +
            this.getSessionAttribute("questionsTaken") +
            "," +
            this.getSessionAttribute("score") +
            "," +
            this.getSessionAttribute("correctAnswerScore") +
            "," +
            this.getSessionAttribute("challengeAdvanceCountQuestions") +
            "," +
            this.getSessionAttribute("challengeAdvanceCountCorrectAns") +
            "," +
            this.getSessionAttribute("challengeAdvanceCountWrongAns") +
            "," +
            this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns") +
            "," +
            this.getSessionAttribute("challengeIntermediateCountQuestions") +
            "," +
            this.getSessionAttribute("challengeIntermediateCountCorrectAns") +
            "," +
            this.getSessionAttribute("challengeIntermediateCountWrongAns") +
            "," +
            this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns") +
            "," +
            this.getSessionAttribute("challengeStarterCountQuestions") +
            "," +
            this.getSessionAttribute("challengeStarterCountCorrectAns") +
            "," +
            this.getSessionAttribute("challengeStarterCountWrongAns") +
            "," +
            this.getSessionAttribute("challengeStarterCountNotAttemptedAns") +
            ',"' +
            this.getSessionAttribute("correctAnsweredQuestionId") +
            '","' +
            this.getSessionAttribute("wrongAnsweredQuestionId") +
            '","' +
            this.getSessionAttribute("notAttemptedQuestionId") +
            '","' +
            this.getSessionAttribute("correctAnsweredWeightage") +
            '","' +
            this.getSessionAttribute("wrongAnsweredWeightage") +
            '","' +
            this.getSessionAttribute("notAttemptedQuestionWeightage") +
            '","' +
            date.getFullYear() +
            "/" +
            (date.getMonth() + 1) +
            "/" +
            date.getDate() +
            " " +
            date.getHours() +
            ":" +
            date.getMinutes() +
            ":" +
            date.getSeconds() +
            '","' +
            sessionId +
            '", ' +
            this.getSessionAttribute("TestIndex") +
            "," +
            this.getSessionAttribute("courseId") +
            "," +
            this.getSessionAttribute("organizationId") +
            ', "' +
            this.getSessionAttribute("section") +
            '", "' +
            this.getSessionAttribute("testType") +
            '", "' +
            this.getSessionAttribute("roleName") +
            '", "' +
            this.getSessionAttribute("firstPlayerSet") +
            '", "' +
            this.getSessionAttribute("secondPlayerSet") +
            '", "' +
            this.getSessionAttribute("thirdPlayerSet") +
            '", "' +
            this.getSessionAttribute("fourthPlayerSet") +
            '", "' +
            this.getSessionAttribute("quizType") +
            '")';
          var resultedRows = await functionCalls.callAPI(queryStringResult);
          console.log('resultedRows -> ' + resultedRows);
        }
      }//meenu
      console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
      console.log('options access -> ' + opt);
      repromptSpeech = `<prosody rate="` +
        constants.speachSpeed +
        `">` +
        `To answer use one of these options ` +
        opt +
        `</prosody>` +
        ` You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
      images.ask(
        speechOutput,
        repromptSpeech, repromptSpeech, goodByeMsg  //Meenu
      );
    },

    ChallengeLevelIntent: async function () {
      var challengeLevel = this.$inputs.dareLevel.value;
      // this.setSessionAttribute("IntentState", "ChallengeLevelIntent");
      var speechOutput = "", repromptSpeech = "";
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify" //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //meenu
        );
      } else {
        if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          // this.setSessionAttribute("IntentState", "ChallengeLevelIntent");
          if (this.getSessionAttribute("Topic")) {
            this.setSessionAttribute("IntentState", "ChallengeLevelIntent");
            var msg;
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("ChallengeLevelIntent isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
            } else {
              // var challengeLevel;
              console.log("questionAnswerHandlers:challengeLevelIntent start ");
              //    slotValue = (this.getchallengeLevel() ? this.getchallengeLevel() : "")
              var slotValue = challengeLevel;
              console.log("slotvaluure " + slotValue);
              this.setSessionAttribute(
                "UserState",
                constants.UserState.ChallangeLevelSelected
              );
              this.setSessionAttribute('ChallengeLevelContains', 'Yes');  //Meenu changes
              this.setSessionAttribute("state", constants.states.QUESTIONANSWER);

              if (
                slotValue == "starter" &&
                this.getSessionAttribute("enoughQuesForStarter") === "Yes"
                // slotValue === "starter" ||
                // this.getSessionAttribute("challengeLevelInd") === "S"
              ) {
                challengeLevel = "S";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
                slotValue = "starter";
                await this.toIntent("populateQuestions");
              } else if (
                slotValue == "intermediate" &&
                this.getSessionAttribute("enoughQuesForIntermediate") === "Yes"
                // slotValue === "intermediate" ||
                // this.getSessionAttribute("challengeLevelInd") === "I"
              ) {
                challengeLevel = "I";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
                slotValue = "intermediate";
                await this.toIntent("populateQuestions");
              } else if (
                slotValue == "advanced" &&
                this.getSessionAttribute("enoughQuesForAdvanced") === "Yes"
                // slotValue === "advanced" ||
                // this.getSessionAttribute("challengeLevelInd") === "A"
              ) {
                challengeLevel = "A";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
                slotValue = "advanced";
                await this.toIntent("populateQuestions");
              } else {
                // challengeLevel = "S";
                // slotValue = "starter";
                // this.setSessionAttribute("challengeLevelInd", challengeLevel);
                this.ask(" This option is not available in this topic, please choose from the given challenge levels. " +
                  "Available challenge Level " +
                  this.getSessionAttribute("availableChallengeLevels") +
                  " Say <s> Select </s> and the <s>Challenge level</s>"
                )
              }
              // this.setSessionAttribute(
              //   "UserState",
              //   constants.UserState.ChallangeLevelSelected
              // );
              challengLevelSlotValue = slotValue;
              // this.setSessionAttribute('ChallengeLevelContains', 'Yes');  //Meenu changes
              // this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
              // await this.toIntent("populateQuestions");
            }
          } else {
            speechOutput = "Please select topic before selecting Challenge level. Say <s> Open and the Topic Name</s> or say - list topics to get other topics"; //meenu
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
          }
        }
      }
    },

    AnswerIntent: async function () {
      // var answer = this.$inputs.Answer.value;
      // //TODO : ****************    Ooption needs to be provided to seek th explaination aftere correct/incorrect option before moving tho the next question.
      // this.setSessionAttribute("IntentState", "AnswerIntent");
      // var msg, speechOutput = "", repromptSpeech = "";
      // console.log("Sesion TOPIC called : " + this.getSessionAttribute("TopicId"));
      console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";  //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          var answer = this.$inputs.Answer.value;
          //TODO : ****************    Ooption needs to be provided to seek th explaination aftere correct/incorrect option before moving tho the next question.
          // this.setSessionAttribute("IntentState", "AnswerIntent");
          var msg, speechOutput = "", repromptSpeech = "";
          console.log("Sesion TOPIC called : " + this.getSessionAttribute("TopicId"));
          console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
          if (this.getSessionAttribute("TopicId")) {
            this.setSessionAttribute("IntentState", "AnswerIntent");
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("AnswerIntent isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);  //meenu
            } else {
              console.log("start handleAnswers");
              console.log("User Answer : " + answer);
              // var speechOutput = ""; //meenu
              //Start -> For testing that the option user is telling is exist in options or not
              // this.setSessionAttribute("IntentState", "AnswerIntent");
              var opt = this.getSessionAttribute("optionsChoice");
              console.log('options exist or not -> ' + opt.includes(answer));
              if (answer.toString().toLowerCase() === 'b.') {
                console.log('Correct -> answer -> ' + answer);
                answer = "b";
              }
              if (opt.includes(answer.toString().toUpperCase())) {
                console.log(answer.toString().toUpperCase());
                console.log('yes u r correct');
                answer = answer.toString().toLowerCase();
                //In case mo test in progress return a freindly error message. Transfer to NoTestInProgress function
                if (!questionData) {
                  await this.toIntent("NoTestInProgress");
                } else {
                  if (!answer || answer === undefined) {
                    console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
                    console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));  //options choices
                    var opt = this.getSessionAttribute("optionsChoice");
                    speechOutput = "answer the question by saying - the answer is " + opt;  //meenu
                    repromptSpeech = speechOutput;
                    this.ask(
                      speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
                    );
                  } else {
                    var quslen = this.getSessionAttribute("questions").length;
                    // this.attributes['firstPlayerScore'] = 0;
                    // this.attributes['secondPlayerScore'] = 0;

                    //	if (answer === "a" || answer === "b" || answer === "c" || answer === "d" || answer === "e") {
                    var correctAnswerFlag = false;

                    answer = answer.toString().toLowerCase();

                    //Sometime alexa returns a . in addition to the option. below is to address it
                    answer = answer.substr(0, 1);

                    this.setSessionAttribute(
                      "currentQuestionIndex",
                      this.getSessionAttribute("currentQuestionIndex") + 1
                    );
                    this.setSessionAttribute(
                      "TestIndex",
                      this.getSessionAttribute("TestIndex") + 1
                    );

                    if (
                      this.getSessionAttribute("correctAnswerText")
                        .toString()
                        .toLowerCase() === answer.toString().toLowerCase()
                    ) {
                      if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
                        speechOutput = " correct answer ";
                      }
                      else {
                        speechOutput = "";
                      }
                      correctAnswerFlag = true;
                      //storing this score value in to database so, keeping as it is. later can be removed.
                      this.setSessionAttribute(
                        "score",
                        this.getSessionAttribute("score") + 1
                      );
                      if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
                        this.setSessionAttribute(
                          "challengeStarterCountCorrectAns",
                          this.getSessionAttribute("challengeStarterCountCorrectAns") + 1
                        );
                      } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
                        this.setSessionAttribute(
                          "challengeIntermediateCountCorrectAns",
                          this.getSessionAttribute("challengeIntermediateCountCorrectAns") + 1
                        );
                      } else {
                        this.setSessionAttribute(
                          "challengeAdvanceCountCorrectAns",
                          this.getSessionAttribute("challengeAdvanceCountCorrectAns") + 1
                        );
                      }

                      console.log("Correct Answer ");

                      //Meenu changes session
                      var UserId = this.getSessionAttribute("UserId");
                      var TopicId = this.getSessionAttribute("TopicId");
                      var questionsTaken = this.getSessionAttribute("questionsTaken");
                      var score = this.getSessionAttribute("score");
                      var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
                      var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
                      var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
                      var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
                      var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
                      var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
                      var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
                      var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
                      var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
                      var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
                      var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
                      var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
                      var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
                      var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
                      var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
                      var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
                      var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
                      var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
                      var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
                      var TestIndex = this.getSessionAttribute("TestIndex");
                      var courseId = this.getSessionAttribute("courseId");
                      var organizationId = this.getSessionAttribute("organizationId");
                      var section = this.getSessionAttribute("section");
                      var testType = this.getSessionAttribute("testType");
                      var roleName = this.getSessionAttribute("roleName");
                      var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
                      var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
                      var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
                      var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
                      var quizType = this.getSessionAttribute("quizType");
                      await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
                        correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
                        correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
                        challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
                        challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
                        challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
                        challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
                        challengeStarterCountQuestions, challengeStarterCountCorrectAns,
                        challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
                        courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
                        thirdPlayerSet, fourthPlayerSet, quizType);
                      //QuestionId which are answered correct
                      this.setSessionAttribute(
                        "correctAnsweredQuestionId",
                        this.getSessionAttribute("correctAnsweredQuestionId") +
                        this.getSessionAttribute("questionId") +
                        " "
                      );
                      this.setSessionAttribute(
                        "correctAnsweredWeightage",
                        this.getSessionAttribute("correctAnsweredWeightage") +
                        this.getSessionAttribute("questionWeightage") +
                        " "
                      );  //Weightage
                      this.setSessionAttribute(
                        'correctAnswerScore',
                        this.getSessionAttribute('correctAnswerScore') +
                        this.getSessionAttribute('questionWeightage')
                      );

                      console.log(
                        "Correct Answered QuestionId --> " +
                        this.getSessionAttribute("correctAnsweredQuestionId")
                      );
                      console.log(
                        "Correct Answered Wightage --> " +
                        this.getSessionAttribute("correctAnsweredWeightage")
                      );
                      console.log("Correct Answered Total Score --> " +
                        this.getSessionAttribute('correctAnswerScore'));

                      //TODO Read questions for the Selected Values
                      console.log(
                        "Total Question length : " +
                        quslen +
                        " Test index -> " +
                        this.getSessionAttribute("TestIndex")
                      );
                      this.setSessionAttribute("speechOutput", speechOutput);
                    } else {
                      if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
                        speechOutput =
                          " - ,Incorrect answer, the correct answer is, " +
                          this.getSessionAttribute("correctAnswerText") +
                          " - <s>" +
                          answerText[this.getSessionAttribute("correctAnswerText")] +
                          "</s>,  ";
                      }
                      else {
                        speechOutput = "";
                      }

                      //QuestionId which are answered wrong
                      this.setSessionAttribute(
                        "wrongAnsweredQuestionId",
                        this.getSessionAttribute("wrongAnsweredQuestionId") +
                        this.getSessionAttribute("questionId") +
                        " "
                      );
                      this.setSessionAttribute(
                        "wrongAnsweredWeightage",
                        this.getSessionAttribute("wrongAnsweredWeightage") +
                        this.getSessionAttribute("questionWeightage") +
                        " "
                      );
                      if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
                        this.setSessionAttribute(
                          "challengeStarterCountWrongAns",
                          this.getSessionAttribute("challengeStarterCountWrongAns") + 1
                        );
                      } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
                        this.setSessionAttribute(
                          "challengeIntermediateCountWrongAns",
                          this.getSessionAttribute("challengeIntermediateCountWrongAns") + 1
                        );
                      } else {
                        this.setSessionAttribute(
                          "challengeAdvanceCountWrongAns",
                          this.getSessionAttribute("challengeAdvanceCountWrongAns") + 1
                        );
                      }
                      console.log(
                        "Wrong Answered QuestionId --> " +
                        this.getSessionAttribute("wrongAnsweredQuestionId")
                      );
                      console.log(
                        "Wrong Answered Wightage --> " +
                        this.getSessionAttribute("wrongAnsweredWeightage")
                      );
                      //Ask for explaination
                      this.setSessionAttribute("speechOutput", speechOutput);

                      //VoiceLabs.track(this.event.session, null, null, null, (error, response) => {
                      // this.ask( speechOutput, ', To Explain, try saying, - Explain the Answer or say next question - to continue quiz ');
                      //});
                    }
                    console.log(
                      "Current question index -> " +
                      this.getSessionAttribute("currentQuestionIndex")
                    );


                    // for multiplayer quizes :- 
                    //This block of code is for all players 2,3,4, only for wrong and correct answered
                    // questions. For skiped questions search for "call next question"
                    if (this.getSessionAttribute("MultiplayerQuizIntent") &&
                      this.getSessionAttribute("MultiplayerQuizIntent") !== null) {
                      MqQuestioncount = 0;
                      this.setSessionAttribute("IntentStateForMQ", "");
                      //If the user answers correct, then here we're checking the question index to check the question 
                      //belongs to which player, and based on that we're throwing next question to next player.
                      if (correctAnswerFlag) {
                        this.setSessionAttribute("AnsIntentStateForMQ", "AnswerFlag");
                        console.log("correct answer " + this.getSessionAttribute("currentQuestionIndex"));
                        if (this.getSessionAttribute("numberOfPlayers") === 2) {
                          // if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("secondPlayerName")) {
                          if ((this.getSessionAttribute("currentQuestionIndex")) % 2 === 0) {
                            // secondPlayerScore += 1;
                            console.log("correct score of player2");
                            this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                            this.setSessionAttribute("secondPlayerCorrectAnsQue",
                              this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("secondPlayerSet",
                              this.getSessionAttribute("secondPlayerName") +
                              ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                              ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                            )
                            await this.toIntent("firstPlayerIntent");
                          } else {
                            // firstPlayerScore += 1;
                            console.log("correct score of player1");
                            this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                            this.setSessionAttribute("firstPlayerCorrectAnsQue",
                              this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("firstPlayerSet",
                              this.getSessionAttribute("firstPlayerName") +
                              ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                              ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                            )
                            await this.toIntent("secondPlayerIntent");
                          }
                        } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                          console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                          if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 0) {
                            // thirdPlayerScore += 1;
                            // console.log("correct score of player3", thirdPlayerScore);
                            this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                            this.setSessionAttribute("thirdPlayerCorrectAnsQue",
                              this.getSessionAttribute("thirdPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("thirdPlayerSet",
                              this.getSessionAttribute("thirdPlayerName") +
                              ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                              ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                            );
                            await this.toIntent("firstPlayerIntent");
                          }
                          else if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 1) {
                            // firstPlayerScore += 1;
                            // console.log("correct score of player1", firstPlayerScore);
                            this.setSessionAttribute("firstPlayerTotalQue",
                              this.getSessionAttribute("firstPlayerTotalQue") + 1);
                            this.setSessionAttribute("firstPlayerCorrectAnsQue",
                              this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("firstPlayerSet",
                              this.getSessionAttribute("firstPlayerName") +
                              ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                              ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                            )
                            await this.toIntent("secondPlayerIntent");
                          } else {
                            // secondPlayerScore += 1;
                            // console.log("correct score of player2", secondPlayerScore);
                            this.setSessionAttribute("secondPlayerTotalQue",
                              this.getSessionAttribute("secondPlayerTotalQue") + 1);
                            this.setSessionAttribute("secondPlayerCorrectAnsQue",
                              this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("secondPlayerSet",
                              this.getSessionAttribute("secondPlayerName") +
                              ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                              ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                            )
                            await this.toIntent("thirdPlayerIntent");
                          }
                        } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                          console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                          if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 0) {
                            // thirdPlayerScore += 1;
                            // console.log("correct score of player1", thirdPlayerScore);
                            this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                            this.setSessionAttribute("thirdPlayerCorrectAnsQue",
                              this.getSessionAttribute("thirdPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("thirdPlayerSet",
                              this.getSessionAttribute("thirdPlayerName") +
                              ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                              ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                            );
                            await this.toIntent("firstPlayerIntent");
                          }
                          else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 1) {
                            // firstPlayerScore += 1;
                            // console.log("correct score of player2", firstPlayerScore);
                            this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                            this.setSessionAttribute("firstPlayerCorrectAnsQue",
                              this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("firstPlayerSet",
                              this.getSessionAttribute("firstPlayerName") +
                              ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                              ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                            )
                            await this.toIntent("secondPlayerIntent");
                          } else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 2) {
                            // fourthPlayerScore += 1;
                            // console.log("correct score of player3", fourthPlayerScore);
                            this.setSessionAttribute("fourthPlayerTotalQue", this.getSessionAttribute("fourthPlayerTotalQue") + 1);
                            this.setSessionAttribute("fourthPlayerCorrectAnsQue",
                              this.getSessionAttribute("fourthPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("fourthPlayerSet",
                              this.getSessionAttribute("fourthPlayerName") +
                              ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
                              ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
                            );
                            await this.toIntent("thirdPlayerIntent");
                          } else {
                            // secondPlayerScore += 1;
                            // console.log("correct score of player4", secondPlayerScore);
                            this.setSessionAttribute("secondPlayerCorrectAnsQue",
                              this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
                            this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                            this.setSessionAttribute("secondPlayerSet",
                              this.getSessionAttribute("secondPlayerName") +
                              ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                              ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                            )
                            await this.toIntent("fourthPlayerIntent");
                          }
                        }
                      } else {  ///wrong answer starting
                        //If the user answers wrong, then here we're checking the question index to check the question belongs to which player, 
                        //and based on that we're throwing next question to next player.
                        console.log("wrong answer " + this.getSessionAttribute("currentQuestionIndex"));
                        console.log("speech output -> " + this.getSessionAttribute("speechOutput"));
                        this.setSessionAttribute("AnsIntentStateForMQ", "AnswerFlag");
                        if (this.getSessionAttribute("numberOfPlayers") === 2) {
                          if (this.getSessionAttribute("currentQuestionIndex") % 2 === 0) {
                            // secondPlayerWScore += 1;
                            // secondPlayerScore = secondPlayerWScore;
                            console.log("wrong score of player2");
                            this.setSessionAttribute("secondPlayerTotalQue",
                              this.getSessionAttribute("secondPlayerTotalQue") + 1);
                            this.setSessionAttribute("secondPlayerWrongAnsQue",
                              this.getSessionAttribute("secondPlayerWrongAnsQue") + 1
                            );
                            this.setSessionAttribute("secondPlayerSet",
                              this.getSessionAttribute("secondPlayerName") +
                              ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                              ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                            )
                            await this.toIntent("firstPlayerIntent");
                          } else {
                            // firstPlayerWScore += 1;
                            // firstPlayerScore = firstPlayerWScore;
                            console.log("wrong score of player1");
                            this.setSessionAttribute("firstPlayerTotalQue",
                              this.getSessionAttribute("firstPlayerTotalQue") + 1);
                            this.setSessionAttribute("firstPlayerWrongAnsQue",
                              this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("firstPlayerSet",
                              this.getSessionAttribute("firstPlayerName") +
                              ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                              ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                            )
                            await this.toIntent("secondPlayerIntent");
                          }
                        } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                          console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                          if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 0) {
                            // thirdPlayerWScore += 1;
                            // thirdPlayerScore = thirdPlayerWScore;
                            // console.log("wrong score of player3", thirdPlayerScore);
                            this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                            this.setSessionAttribute("thirdPlayerWrongAnsQue",
                              this.getSessionAttribute("thirdPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("thirdPlayerSet",
                              this.getSessionAttribute("thirdPlayerName") +
                              ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                              ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                            );
                            await this.toIntent("firstPlayerIntent");
                          } else if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 1) {
                            // firstPlayerWScore += 1;
                            // firstPlayerScore = firstPlayerWScore;
                            // console.log("wrong score of player1", firstPlayerScore);
                            this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                            this.setSessionAttribute("firstPlayerWrongAnsQue",
                              this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("firstPlayerSet",
                              this.getSessionAttribute("firstPlayerName") +
                              ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                              ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                            )
                            await this.toIntent("secondPlayerIntent");
                          } else {
                            // secondPlayerWScore += 1;
                            // secondPlayerScore = secondPlayerWScore;
                            // console.log("wrong score of player2", secondPlayerScore);
                            this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                            this.setSessionAttribute("secondPlayerWrongAnsQue",
                              this.getSessionAttribute("secondPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("secondPlayerSet",
                              this.getSessionAttribute("secondPlayerName") +
                              ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                              ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                            )
                            await this.toIntent("thirdPlayerIntent");
                          }
                        } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                          console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                          if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 0) {
                            // thirdPlayerWScore += 1;
                            // thirdPlayerScore = thirdPlayerWScore
                            // console.log("wrong score of player1", thirdPlayerScore);
                            this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                            this.setSessionAttribute("thirdPlayerWrongAnsQue",
                              this.getSessionAttribute("thirdPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("thirdPlayerSet",
                              this.getSessionAttribute("thirdPlayerName") +
                              ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                              ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                            );
                            await this.toIntent("firstPlayerIntent");
                          }
                          else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 1) {
                            // firstPlayerWScore += 1;
                            // firstPlayerScore = firstPlayerWScore;
                            // console.log("wrong score of player2", firstPlayerScore);
                            this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                            this.setSessionAttribute("firstPlayerWrongAnsQue",
                              this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("firstPlayerSet",
                              this.getSessionAttribute("firstPlayerName") +
                              ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                              ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                            )
                            await this.toIntent("secondPlayerIntent");
                          } else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 2) {
                            // fourthPlayerWScore += 1;
                            // fourthPlayerScore = fourthPlayerWScore;
                            // console.log("wrong score of player3", fourthPlayerScore);
                            this.setSessionAttribute("fourthPlayerTotalQue", this.getSessionAttribute("fourthPlayerTotalQue") + 1);
                            this.setSessionAttribute("fourthPlayerWrongAnsQue",
                              this.getSessionAttribute("fourthPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("fourthPlayerSet",
                              this.getSessionAttribute("fourthPlayerName") +
                              ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
                              ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
                            );
                            await this.toIntent("thirdPlayerIntent");
                          } else {
                            // secondPlayerWScore += 1;
                            // secondPlayerScore = secondPlayerWScore;
                            // console.log("wrong score of player4", secondPlayerScore);
                            this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                            this.setSessionAttribute("secondPlayerWrongAnsQue",
                              this.getSessionAttribute("secondPlayerWrongAnsQue") + 1);
                            this.setSessionAttribute("secondPlayerSet",
                              this.getSessionAttribute("secondPlayerName") +
                              ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                              ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                              ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                            )
                            await this.toIntent("fourthPlayerIntent");
                          }
                        }
                      }  //ending wrong answers
                    }
                    //end of multiplayer quiz

                    // if (this.getSessionAttribute("twoPlayersQuiz")) {
                    //   if (
                    //     this.getSessionAttribute("currentQuestionIndex") % 2 ===
                    //     0
                    //   ) {
                    //     if (correctAnswerFlag) {
                    //       firstPlayerScore += 1;
                    //     }
                    //     console.log(
                    //       "Player 2's questionIndex ->" +
                    //       this.getSessionAttribute("currentQuestionIndex")
                    //     );
                    //     console.log(
                    //       "Plater 2's score ->" +
                    //       this.getSessionAttribute("secondPlayerScore")
                    //     );
                    //     await this.toIntent("SecondPlayer");
                    //   } else {
                    //     if (correctAnswerFlag) {
                    //       secondPlayerScore += 1;
                    //     }
                    //     console.log(
                    //       "Player 1's questionIndex ->" +
                    //       this.getSessionAttribute("currentQuestionIndex")
                    //     );
                    //     console.log(
                    //       "Plater 1's score ->" +
                    //       this.getSessionAttribute("firstPlayerScore")
                    //     );
                    //     await this.toIntent("FirstPlayer");
                    //   }
                    // }
                    else {
                      if (
                        this.getSessionAttribute("currentQuestionIndex") <= quslen
                      ) {
                        await this.toIntent("getNextQuestion");
                        //		}
                        //		 else {
                        // this.attributes['speechOutput'] = " - ";
                        // app.toIntent('endOfTest');
                        //		}
                      } else {
                        this.setSessionAttribute(
                          "speechOutput",
                          this.getSessionAttribute("speechOutput") +
                          " End of this quiz "
                        );
                        console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
                        await this.toIntent("endOfTest");
                      }
                    }
                    console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
                  }
                }
              }
              else {
                console.log('Unfortunately, I have to say that, u r wrong');
                speechOutput = "To answer this question, please say one of these options, " + opt +
                  " , or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
                repromptSpeech = speechOutput;
                this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
              }
              //End -> For testing that the option user is telling is exist in options or not
            }
          } else {
            console.log("Intents state -> " + this.getSessionAttribute("IntentState"));
            console.log(" state -> " + this.getSessionAttribute("state"));
            console.log("user state -> " + this.getSessionAttribute("UserState"));
            speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //meenu
            );
          }
        }
      }
    },

    getNextQuestion: async function () {
      console.log("Starting getNextQuestion");
      this.setSessionAttribute("IntentState", "getNextQuestion");
      var data = [];
      var qId = [];
      var completedCount = this.getSessionAttribute("TestIndex");
      //if the compleated question count is equal to configured count, exit test
      var speechoutput = "", repromptSpeech = "";
      if (!this.getSessionAttribute("UserId")) {
        speechoutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
        repromptSpeech = speechoutput;
        this.ask(
          speechoutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (this.getSessionAttribute("TopicId")) {
          if (completedCount >= this.getSessionAttribute("numberOfQuestionsToAccess")) {
            console.log("end");
            await this.toIntent("endOfTest");
          }
          // if (completedCount >= constants.TestSize) {
          //       console.log("end");
          //       await this.toIntent("endOfTest");
          //     } 
          else {
            console.log("inside getnextquestion block");
            // var rem = completedCount % constants.questionsToRead;  //for multiplayer quiz commented
            var rem = completedCount % this.getSessionAttribute("numberOfQuestionsToAccess");
            if (
              rem === 0 &&
              completedCount != 0 &&
              // completedCount <= constants.TestSize   //for multiplayer quiz commented
              completedCount <= this.getSessionAttribute("numberOfQuestionsToAccess")
            ) {
              // this.attributes['currentQuestionIndex'] = 0;
              this.setSessionAttribute("currentQuestionIndex", 0);
              console.log("db connection for nxt qus set");
              console.log("Inside if,  Reminder  : " + rem);
              // var testTopic = this.attributes['Topic'];
              var testTopic = this.getSessionAttribute("Topic");
              //reading next set of questions from DB
              //Read max id from Sesson atribute
              // var maxId = this.attributes['maxQuestionID'];
              var maxId = this.getSessionAttribute("maxQuestionID");
              if (maxId === null) {
                maxId = 0;
              }
              // var queryString = 'call SP_accessQuestion_alexa_Dev_2018("' + this.attributes['TopicId'] + '", "' + this.attributes['challengeLevelInd'] + '","' + maxId + '","' + constants.questionsToRead + '")';
              var queryString =
                'call SP_accessQuestion_alexa_Dev_2018("' +
                this.getSessionAttribute("TopicId") +
                '", "' +
                this.getSessionAttribute("challengeLevelInd") +
                '","' +
                maxId +
                '","' +
                // constants.questionsToRead +    //for multiplayer quiz commented
                this.getSessionAttribute("numberOfQuestionsToAccess") +
                '")';
              console.log("Query String -> " + queryString);
              console.log(
                "completed Count -> " +
                completedCount +
                " Reading next set of questions"
              );
              let rows;
              // Database.execute(
              //     database => database.query(queryString)
              //         .then(r => {
              //             rows = r;
              //         })
              // ).then(() => {

              // }).catch(err => {
              //     console.log('DB Error: ', err);
              //     pointerToThis.tell(constants.errorPrompt);
              // });
              rows = await functionCalls.callAPI(queryString);
              data = rows[0];
              for (var i in data) {
                qId[i] = data[i].questionId;
              }
              var maxQuestionID = arrayFunctions.maxAaaryValue(qId);
              console.log("Maximum Question Id-> " + maxQuestionID);
              console.log("Qiestion Id " + qId);
              //Add o session attribute
              // this.attributes['maxQuestionID'] = maxQuestionID;
              this.setSessionAttribute("maxQuestionID", maxQuestionID);

              // this.attributes['questions'] = "";
              this.setSessionAttribute("questions", "");

              //nextQuesIndex = this.attributes['currentQuestionIndex'] = 0;
              // console.log('Question data erased --->> ' + this.attributes['questions'])
              console.log(
                "Question data erased --->> " +
                this.getSessionAttribute("questions")
              );

              //assign the new list of questions retrived
              questionData = arrayFunctions.shuffleArray(data);
              // this.attributes['questions'] = questionData;
              this.setSessionAttribute("questions", questionData);

              // if (this.attributes['questions'].length === 0) {
              if (this.getSessionAttribute("questions").length === 0) {
                console.log("loadQuestions  did not return anything!!!");
                await this.toIntent("endOfTest");
              } else {
                await this.toIntent("ContinueGetNextQuestion");
              }
            } else {
              // this.emitWithState('ContinueGetNextQuestion');
              await this.toIntent("ContinueGetNextQuestion");
            }
          }
        } else {
          speechoutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
          repromptSpeech = speechoutput;
          this.ask(
            speechoutput, repromptSpeech, repromptSpeech, goodByeMsg // meenu
          );
        }
      }
    },

    ContinueGetNextQuestion: async function () {
      this.setSessionAttribute('IntentState', 'ContinueGetNextQuestion');  //Meenu
      this.setSessionAttribute('ImageContainer', 'No');
      this.setSessionAttribute('AudioContainer', 'No');

      nextQuesIndex = this.getSessionAttribute("currentQuestionIndex");
      questionData = this.getSessionAttribute("questions");

      var completedCount = this.getSessionAttribute("TestIndex");
      var speechOutput = "", repromptSpeech = "", images = this;
      //if the compleated question count is equal to configured count, exit test
      console.log("completedCount -> " + completedCount);
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (this.getSessionAttribute("TopicId")) {
          // if (completedCount >= constants.TestSize) {  //For multiplayer quiz commented
          if (completedCount >= this.getSessionAttribute("numberOfQuestionsToAccess")) {
            console.log("end");
            await this.toIntent("endOfTest");
          } else {
            console.log(questionData + " and index : " + nextQuesIndex);
            console.log("QsTN : " + questionData[nextQuesIndex]);
            console.log("QsTN text : " + questionData[nextQuesIndex].text);

            if ((questionData[nextQuesIndex].externalReference) !== null &&
              (questionData[nextQuesIndex].externalReference) !== undefined) {
              if ((questionData[nextQuesIndex].externalReference).includes('.png') ||
                (questionData[nextQuesIndex].externalReference).includes('.jpg')) {
                console.log(' text -> ' + questionData[nextQuesIndex].text);
                console.log(' text -> ' + questionData[nextQuesIndex].questionId);
                this.setSessionAttribute('ImageContainer', 'Yes');
                console.log('image3');
              } else {
                this.setSessionAttribute('ImageContainer', 'No');
                console.log('image4');
              }
              if ((questionData[nextQuesIndex].externalReference).includes('.mp3')) {
                console.log('AudioContainer3');
                this.setSessionAttribute('AudioContainer', 'Yes');
              } else {
                console.log('AudioContainer4');
                this.setSessionAttribute('AudioContainer', 'No');
              }
            }

            if (this.getSessionAttribute('ImageContainer') === 'Yes') {
              // let title = '';
              // let content = '';
              console.log('image6');
              console.log(" image url -> " + questionData[nextQuesIndex].externalReference);
              imageUrl = questionData[nextQuesIndex].externalReference;
              speechOutput =
                ` -  <prosody rate="` +
                constants.speachSpeed +
                `">` +
                questionData[nextQuesIndex].text +
                ". <s> Options are </s> ";
              images = this.showImageCard(title, content, imageUrl);
            } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
              console.log('AudioContainer5');
              speechOutput =
                ` -  <prosody rate="` +
                constants.speachSpeed +
                `">` +
                questionData[nextQuesIndex].text +
                "<audio src= '" + questionData[nextQuesIndex].externalReference + "'/>" +
                " <s> Options are </s> ";
            } else {
              speechOutput =
                ` -  <prosody rate="` +
                constants.speachSpeed +
                `">` +
                questionData[nextQuesIndex].text +
                " <s> Options are </s> ";
            }
            //Assigning the next question Options

            var rowData = JSON.parse(
              "[" + questionData[nextQuesIndex].options + "]"
            );
            var opt = " ";
            for (var key = 0; key < rowData.length; key++) {
              var dt = rowData[key];
              for (var i in dt) {
                if (dt[i] != "") {
                  speechOutput =
                    speechOutput +
                    (`<s>` +
                      i +
                      `</s>` +
                      " - " +
                      dt[i] +
                      ',<break time="' +
                      constants.pauseTime +
                      '"/> ');
                  opt = opt + i + `,  ` + ',<break time="' +
                    constants.pauseTime +
                    '"/> ';   //options choices
                  answerText[i] = dt[i];
                }
              }
            }
            speechOutput += `</prosody>`;
            console.log('options choices -> ' + opt);
            this.setSessionAttribute("optionsChoice", opt);
            console.log("Next Question is : " + speechOutput);

            /////reset he current index to zer
            this.setSessionAttribute("currentQuestionIndex", nextQuesIndex);
            //populate the answer for the first row in the questions array
            this.setSessionAttribute(
              "correctAnswerText",
              questionData[nextQuesIndex].answer
            );
            this.setSessionAttribute(
              "questionId",
              questionData[nextQuesIndex].questionId
            );
            this.setSessionAttribute(
              "questionWeightage",
              questionData[nextQuesIndex].weightage
            );   //Weightage
            this.setSessionAttribute(
              "challengeLevelOfEachQue",
              questionData[nextQuesIndex].challenge
            );   //To know the challenge level of each question after 1st question
            console.log('questionWeightage -> ' + this.getSessionAttribute('questionWeightage'));
            //incrementing the testIndex by one
            this.setSessionAttribute(
              "UserState",
              constants.UserState.QuestionsRead
            );
            this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
            // VoiceLabs.track(this.event.session, 'ContinueGetNextQuestion', null, null, (error, response) => {

            console.log(
              "After " +
              this.getSessionAttribute("speechOutput") +
              ` Next Question ` +
              speechOutput,
              " Repeating the same question - " +
              speechOutput +
              " Say - the answer is " + opt + " or say Next question to skip this question."
            );

            //Meenu changes session
            var UserId = this.getSessionAttribute("UserId");
            var TopicId = this.getSessionAttribute("TopicId");
            var questionsTaken = this.getSessionAttribute("questionsTaken");
            var score = this.getSessionAttribute("score");
            var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
            var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
            var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
            var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
            var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
            var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
            var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
            var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
            var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
            var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
            var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
            var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
            var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
            var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
            var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
            var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
            var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
            var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
            var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
            var TestIndex = this.getSessionAttribute("TestIndex");
            var courseId = this.getSessionAttribute("courseId");
            var organizationId = this.getSessionAttribute("organizationId");
            var section = this.getSessionAttribute("section");
            var testType = this.getSessionAttribute("testType");
            var roleName = this.getSessionAttribute("roleName");
            var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
            var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
            var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
            var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
            var quizType = this.getSessionAttribute("quizType");
            await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
              correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
              correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
              challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
              challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
              challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
              challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
              challengeStarterCountQuestions, challengeStarterCountCorrectAns,
              challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
              courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
              thirdPlayerSet, fourthPlayerSet, quizType);

            console.log('speechOutput -> ' + speechOutput + ' Result => ' + this.getSessionAttribute("score"));//Meenu session end change
            repromptSpeech = " Repeating the same question - " +
              speechOutput +
              " Say - the answer is " + opt + " or say Next question to skip this question."; //meenu
            if (this.getSessionAttribute('imageExplanation') !== null &&
              this.getSessionAttribute('imageExplanation') !== undefined) {
              imageUrl = this.getSessionAttribute('imageExplanation');
              images = this.showImageCard(title, content, imageUrl);
              console.log(' image url -> ' + imageUrl);
            }
            else {
              this.setSessionAttribute('imageExplanation', null)
              images = this;
            }


            //Explaination for multiplayer quiz
            if (this.getSessionAttribute("MultiplayerQuizIntent")) {
              console.log(" current p name -> " + this.getSessionAttribute("currentPlayerName"));
              if (this.getSessionAttribute("IntentStateForMQ") === "showingCorrectAnswer") {
                images.ask(
                  this.getSessionAttribute("speechOutput") +
                  ` Next Question for ` +
                  this.getSessionAttribute("currentPlayerName") +
                  speechOutput,
                  repromptSpeech, repromptSpeech, goodByeMsg
                );
              } else {
                images.ask(
                  this.getSessionAttribute("speechOutput") +
                  ` Question for ` +
                  this.getSessionAttribute("currentPlayerName") +
                  speechOutput,
                  repromptSpeech, repromptSpeech, goodByeMsg
                );
              }
            } else {
              images.ask(
                this.getSessionAttribute("speechOutput") +
                ` Next Question ` +
                speechOutput,
                repromptSpeech, repromptSpeech, goodByeMsg
              );
            }
            // });
          }
        } else {
          speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>";
          repromptSpeech = speechOutput;
          this.ask(
            speechOutput, repromptSpeech, repromptSpeech, goodByeMsg // meenu
          );
        }
      }
    },

    endOfTest: async function () {
      console.log('End of test');
      // this.setSessionAttribute("IntentState", "IdentifyUser"); SwitchUser UnhandledIdentifyUser IdentifyUserByPin
      var IntentState = this.getSessionAttribute('IntentState');
      if (IntentState === "populateQuestions" || IntentState === "ExplainIntent" ||
        IntentState === "AnswerIntent" || IntentState === "getNextQuestion" ||
        IntentState === "ContinueGetNextQuestion" || IntentState === "RepeatQuestion" ||
        IntentState === "RepeatOptions" || IntentState === "CallNextQuestion"
      ) {
        console.log('ENDIntent -> ' + this.getSessionAttribute('InentState'));
        this.setSessionAttribute("IntentState", "endOfTest");
        this.setSessionAttribute("UserState", constants.UserState.EndOfTest);
        console.log("Starting endOfTest ");
        this.setSessionAttribute("questions", "");
        var speechOutput = "";
        var attemptedQuestions = this.getSessionAttribute("TestIndex")
          ? this.getSessionAttribute("TestIndex")
          : 0;
        var attemptedQuestionsBy2Players =
          attemptedQuestions > 1
            ? (attemptedQuestionsBy2Players = attemptedQuestions / 2)
            : 0;

        var topicId = this.getSessionAttribute("TopicId")
          ? this.getSessionAttribute("TopicId")
          : 0;
        var correctQuestionId = this.getSessionAttribute(
          "correctAnsweredQuestionId"
        )
          ? this.getSessionAttribute("correctAnsweredQuestionId")
          : 0;
        var wrongQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId")
          ? this.getSessionAttribute("wrongAnsweredQuestionId")
          : 0;
        var userId = this.getSessionAttribute("UserId");
        // var sessionId = this.getsessionId();
        var sessionId = "this.getsessionId()";
        var date = new Date();
        console.log(' current date -> ' + date);
        var noOfCorrectAnswers = this.getSessionAttribute("score")
          ? this.getSessionAttribute("score")
          : 0;

        //Meenu session changees
        var UserId = this.getSessionAttribute("UserId");
        var TopicId = this.getSessionAttribute("TopicId");
        var questionsTaken = this.getSessionAttribute("questionsTaken");
        var score = this.getSessionAttribute("score");
        var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
        var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
        var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
        var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
        var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
        var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
        var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
        var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
        var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
        var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
        var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
        var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
        var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
        var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
        var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
        var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
        var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
        var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
        var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
        var TestIndex = this.getSessionAttribute("TestIndex");
        var courseId = this.getSessionAttribute("courseId");
        var organizationId = this.getSessionAttribute("organizationId");
        var section = this.getSessionAttribute("section");
        var testType = this.getSessionAttribute("testType");
        var roleName = this.getSessionAttribute("roleName");
        var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
        var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
        var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
        var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
        var quizType = this.getSessionAttribute("quizType");
        await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
          correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
          correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
          challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
          challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
          challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
          challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
          challengeStarterCountQuestions, challengeStarterCountCorrectAns,
          challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
          courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
          thirdPlayerSet, fourthPlayerSet, quizType);

        // console.log("Query -> " + rows);
        console.log("correct questions ID -> " + correctQuestionId);

        var boom = "<audio src='https://s3.amazonaws.com/sounds226/boom.mp3'/>";
        var resultSpeech = [
          "Well done!, thank you for using My Kwiz.",
          "Well!, My Kwiz can help you to improve your score, Give it another shot, thank you.",
          "Superb!, you have done very well. thank you for using My Kwiz"
        ];


        // Database.execute(
        //     database => database.query(queryString)
        //         .then(r => {
        //             rows = r;
        //         })
        // ).then(() => {

        // }).catch(err => {
        //     console.log('DB Error: ', err);
        //     this.tell(constants.errorPrompt);
        // });
        // rows = await callAPI(queryStringResult);
        // this.tell('I am at end of test');   //Meenu
        // if (rows) {
        console.log("second function");
        // if (constants.TestSize != attemptedQuestions) {  //for multiplayer quiz commented
        if (this.getSessionAttribute("numberOfQuestionsToAccess") != attemptedQuestions) {
          // speechOutput = this.getSessionAttribute('speechOutput') + " Quiz Ended  ";
          // speechOutput = " Quiz Ended,  <break time='1000ms'/>";
          speechOutput = " Quiz Ended,  <break time='250ms'/>";
          // this.tell("Quiz Ended");
        } else {
          speechOutput = this.getSessionAttribute("speechOutput") + " Quiz Ended,  <break time='250ms'/>";
        }
        console.log("speechOutput ->" + speechOutput);
        //Reset so that we dont increase the data
        //speechOutput = speechOutput + "Your total score is " + noOfCorrectAnswers + " - out of - " + attemptedQuestions + ",  ";
        // if (this.getSessionAttribute("twoPlayersQuiz")) {
        //   speechOutput =
        //     speechOutput +
        //     "Player 1's score is " +
        //     firstPlayerScore +
        //     " - out of - " +
        //     attemptedQuestionsBy2Players +
        //     ", and player 2's score is " +
        //     secondPlayerScore +
        //     " - out of - " +
        //     attemptedQuestionsBy2Players +
        //     " ";
        // }
        console.log("quiz type -> " + this.getSessionAttribute("MultiplayerQuizIntent"));
        console.log("no of players -> " + this.getSessionAttribute("numberOfPlayers"));
        if (this.getSessionAttribute("MultiplayerQuizIntent")) {
          if (this.getSessionAttribute("numberOfPlayers") === 2) {
            speechOutput =
              speechOutput +
              " Player " + this.getSessionAttribute("firstPlayerName") +
              " scored " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
              " and player " + this.getSessionAttribute("secondPlayerName") +
              " scored " + this.getSessionAttribute("secondPlayerCorrectAnsQue")
          } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
            speechOutput =
              speechOutput +
              " Player " + this.getSessionAttribute("firstPlayerName") +
              " scored " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
              ", player " + this.getSessionAttribute("secondPlayerName") +
              " scored " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
              " and player " + this.getSessionAttribute("thirdPlayerName") +
              " scored " + this.getSessionAttribute("thirdPlayerCorrectAnsQue")
          } else {
            speechOutput =
              speechOutput +
              " Player " + this.getSessionAttribute("firstPlayerName") +
              " scored " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
              ", player " + this.getSessionAttribute("secondPlayerName") +
              " scored " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
              ", player " + this.getSessionAttribute("thirdPlayerName") +
              " scored " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
              " and player " + this.getSessionAttribute("fourthPlayerName") +
              " scored " + this.getSessionAttribute("fourthPlayerCorrectAnsQue")
          }

        } else {
          if (attemptedQuestions === 0) {
            // speechOutput = speechOutput + " good bye, <break time='1000ms'/>";
            speechOutput = speechOutput + " good bye, <break time='250ms'/>";
          } else {
            if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
              speechOutput =
                speechOutput +
                "Your total score is " +
                noOfCorrectAnswers +
                " - out of - " +
                this.getSessionAttribute("questionsTaken") +
                ",  ";
            }
            else {
              speechOutput = speechOutput;
            }
          }
        }
        console.log("Your total score is " + this.getSessionAttribute("score"));
        // this.handler.state = constants.states.MAIN;
        this.setSessionAttribute("state", constants.states.MAIN);
        // VoiceLabs.track(this.event.session, 'endOfTest', null, null, (error, response) => {
        this.tell(speechOutput + skillExit);
        // });
      }
      else {
        this.tell("Good BYe");
      }
      // }
      // else {
      //   this.tell("Goooood BYe");
      // }
    },

    RepeatQuestion: function () {
      console.log("start RepeatQuestion");
      var currentQuestionIndex = this.getSessionAttribute("currentQuestionIndex");
      // this.setSessionAttribute("IntentState", "RepeatQuestion");
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      var msg, speechOutput = "", repromptSpeech = "", images = this;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        console.log('topic name -> ' + this.getSessionAttribute("Topic")); //testing purpose
        if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          // this.setSessionAttribute("IntentState", "RepeatQuestion");
          if (this.getSessionAttribute("Topic")) {
            this.setSessionAttribute("IntentState", "RepeatQuestion");
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("RepeatQuestion isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
            } else {
              //Since repeating th question
              //It will repease the question
              // this.setSessionAttribute("IntentState", "RepeatQuestion");
              this.setSessionAttribute(
                "UserState",
                constants.UserState.QuestionsRead
              );
              this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
              if (this.getSessionAttribute('ImageContainer') === 'Yes') {
                console.log('image7');
                imageUrl = questionData[currentQuestionIndex].externalReference;
                images = this.showImageCard(title, content, imageUrl);
                speechOutput =
                  "Repeating the question  - " +
                  questionData[currentQuestionIndex].text;
              } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
                console.log('AudioContainer6');
                speechOutput =
                  "Repeating the question  - " +
                  questionData[currentQuestionIndex].text +
                  "<audio src= '" + questionData[currentQuestionIndex].externalReference + "'/>";
              } else {
                console.log('text');
                speechOutput =
                  "Repeating the question  - " +
                  questionData[currentQuestionIndex].text;
              }
              // VoiceLabs.track(this.event.session, 'RepeatQuestion', null, null, (error, response) => {
              console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
              console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));  //options choices
              var opt = this.getSessionAttribute("optionsChoice");
              repromptSpeech = speechOutput +
                " , - to answer to this question try saying the answer is " + opt + " Or to skip this question, say Next question "; //meenu
              images.ask(
                speechOutput,
                repromptSpeech, repromptSpeech, goodByeMsg
              );
            }
          } else {
            speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
          }
        }
      }
    },

    RepeatOptions: function () {
      console.log("start RepeatOptions");
      var msg, speechOutput = "", repromptSpeech = "";
      var currentQuestionIndex = this.getSessionAttribute("currentQuestionIndex");
      // this.setSessionAttribute("IntentState", "RepeatOptions");
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          // this.setSessionAttribute("IntentState", "RepeatOptions");
          if (this.getSessionAttribute("Topic")) {
            this.setSessionAttribute("IntentState", "RepeatOptions");
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("RepeatOptions isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);  //meenu
            } else {
              //Repeating hte options
              //It will repease the options
              // this.setSessionAttribute("IntentState", "RepeatOptions");
              var options = JSON.parse(
                "[" + questionData[currentQuestionIndex].options + "]"
              );
              speechOutput = "Repeating the options  ";
              var breakTime = ',<break time="' +
                constants.pauseTime +
                '"/> ';
              for (var key = 0; key < options.length; key++) {
                var dt = options[key];
                for (var i in dt) {
                  speechOutput = speechOutput + breakTime +
                    (i + "  " + dt[i] + ", " + breakTime);
                }
              }
              console.log("Repeating oprtions" + speechOutput);
              this.setSessionAttribute(
                "UserState",
                constants.UserState.QuestionsRead
              );
              this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
              // VoiceLabs.track(this.event.session, 'RepeatOptions', null, null, (error, response) => {
              console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
              console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));  //options choices
              var opt = this.getSessionAttribute("optionsChoice");
              repromptSpeech = speechOutput +
                " , - to answer to this question try saying the answer is " + opt + " Or to skip this question, say Next question "; //meenu
              this.ask(
                speechOutput,
                repromptSpeech, repromptSpeech, goodByeMsg
              );
              // });
            }
          } else {
            speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
          }
        }
      }
    },

    ExplainIntent: async function () {
      console.log("Starting ExplainIntent ");
      var msg, speechOutput = "", repromptSpeech = "";
      var explainSpeech = "";
      var currentQuestionIndex = this.getSessionAttribute("currentQuestionIndex");
      // this.setSessionAttribute("IntentState", "ExplainIntent");
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          // this.setSessionAttribute("IntentState", "ExplainIntent");
          if (this.getSessionAttribute("Topic")) {
            this.setSessionAttribute("IntentState", "ExplainIntent");
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("explain isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
            } else {
              if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
                var currentQuestionExplanation;
                if (this.getSessionAttribute("MultiplayerQuizIntent")) {
                  if (this.getSessionAttribute("IntentStateForMQ") === "showingCorrectAnswer" ||
                    this.getSessionAttribute("AnsIntentStateForMQ") === "AnswerFlag") {
                    currentQuestionExplanation = questionData[currentQuestionIndex - 1].explain;
                  } else {
                    currentQuestionExplanation = questionData[currentQuestionIndex].explain;
                  }
                } else {   //End of explanation for MPQ
                  currentQuestionExplanation = questionData[currentQuestionIndex].explain;
                }
                console.log("text explanation -> " + currentQuestionExplanation);
                if (currentQuestionExplanation) {
                  var str = currentQuestionExplanation;
                  console.log(' substring -> ' + str.includes('.mp3'));
                  //If explanation contains an audio file
                  if (str.includes('.mp3')) {
                    speechOutput =
                      "Explaination for this answer is <s>  " +
                      "<audio src= '" + currentQuestionExplanation + "'/>" +
                      " </s> ";
                  }
                  //  If explanation is in image form :-
                  else if (str.includes('.jpg') || str.includes('.png')) {
                    imageUrl = currentQuestionExplanation;
                    this.setSessionAttribute('imageExplanation', imageUrl);
                    // images = this.showImageCard(title, content, imageUrl);
                    speechOutput =
                      "Explaination for this answer is <s>  " +
                      " </s> ";
                  }
                  else {
                    speechOutput =
                      "Explaination for this answer is <s>  " +
                      currentQuestionExplanation +
                      " </s> ";
                    console.log(
                      "Explaination read-> " +
                      currentQuestionExplanation
                    );
                  }
                  this.setSessionAttribute(
                    "score",
                    this.getSessionAttribute("score") + 0
                  );
                  explainSpeech =
                    speechOutput +
                    " The answer for this question is - " +
                    this.getSessionAttribute("correctAnswerText") +
                    " - " +
                    answerText[this.getSessionAttribute("correctAnswerText")] +
                    " - ";
                } else {
                  speechOutput =
                    "<s>Explaination is not available for this answer </s> ";
                  console.log("Explaination is not available for this answer");
                  this.setSessionAttribute(
                    "score",
                    this.getSessionAttribute("score") + 0
                  );
                  explainSpeech =
                    speechOutput +
                    " The answer for this question is - " +
                    this.getSessionAttribute("correctAnswerText") +
                    " - " +
                    answerText[this.getSessionAttribute("correctAnswerText")] +
                    " - ";
                }

                if (this.getSessionAttribute("MultiplayerQuizIntent")) {
                  console.log("explanation - > " + this.getSessionAttribute("IntentStateForMQ") + "  " +
                    this.getSessionAttribute("AnsIntentStateForMQ"));
                  if (this.getSessionAttribute("IntentStateForMQ") === "showingCorrectAnswer" ||
                    this.getSessionAttribute("AnsIntentStateForMQ") === "AnswerFlag") {
                    explainSpeech = speechOutput;
                    this.setSessionAttribute("AnsIntentStateForMQ", "");
                  } else {
                    explainSpeech = " Explanation will not be available now." +
                      " You can ask for explanation once you answer the question or the question has been" +
                      " answered by the system when no one will able to answer ";
                  }
                } else {
                  this.setSessionAttribute(
                    "currentQuestionIndex",
                    this.getSessionAttribute("currentQuestionIndex") + 1
                  );
                  this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
                  this.setSessionAttribute(
                    "TestIndex",
                    this.getSessionAttribute("TestIndex") + 1
                  );
                }
                console.log("q i -. " + this.getSessionAttribute("currentQuestionIndex"))
                console.log("explainSpeech -> " + explainSpeech);
                this.setSessionAttribute("speechOutput", explainSpeech);
                //TODO verify the logic
                if (
                  this.getSessionAttribute("currentQuestionIndex") <=
                  this.getSessionAttribute("questions").length
                ) {
                  await this.toIntent("ContinueGetNextQuestion");
                  // } else {
                  // 	this.attributes['speechOutput'] = " - ";
                  // 	await this.toIntent('endOfTest');
                  // }
                } else {
                  this.setSessionAttribute(
                    "speechOutput",
                    this.getSessionAttribute("speechOutput") +
                    " No more questions on theis topic. - "
                  );
                  await this.toIntent("endOfTest");
                }
              }
              else {
                var msg;
                this.setSessionAttribute("IntentState", "oneTimeTestExplaination");
                if (
                  help.isValidContext(
                    this.getSessionAttribute("IntentState"),
                    this.getSessionAttribute("state"),
                    this
                  )
                ) {

                  msg = help.isValidContext(
                    this.getSessionAttribute("IntentState"),
                    this.getSessionAttribute("state"),
                    this
                  );
                  console.log("TopicIntent isValidContext msg -> " + msg);
                  this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
                }
                // explainSpeech = "Since, it's a evolution test, hence you will not have the explanation for this."
                // console.log("explainSpeech -> " + explainSpeech);
                // this.ask(` To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`);
              }
            }
          } else {
            speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
            // }
          }
        }
      }
    },

    CallNextQuestion: async function () {
      this.getSessionAttribute("currentQuestionIndex");
      console.log("Starting CallNextQuestion");
      // this.setSessionAttribute("IntentState", "CallNextQuestion");
      var msg, speechOutput = "", repromptSpeech = "";
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          // this.setSessionAttribute("IntentState", "CallNextQuestion");
          if (this.getSessionAttribute("Topic")) {
            this.setSessionAttribute("IntentState", "CallNextQuestion");
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("CallNextQuestion isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
            } else {
              // //skipping the questions for the next question or requested after the explaination
              // this.setSessionAttribute("IntentState", "CallNextQuestion");
              if (
                this.getSessionAttribute("currentQuestionIndex") >=
                // constants.questionsToRead   //for multiplayer quiz commented
                this.getSessionAttribute("numberOfQuestionsToAccess")
              ) {
                this.setSessionAttribute("currentQuestionIndex", 0);
              } else {
                this.setSessionAttribute(
                  "score",
                  this.getSessionAttribute("score")
                    ? this.getSessionAttribute("score")
                    : 0 + 0
                );
                // this.attributes['score'] = this.attributes['score'] ? this.attributes['score'] : 0 + 0;
                console.log("CALL NEXT QUESTION");
                console.log(
                  "skipping the question" +
                  this.getSessionAttribute("currentQuestionIndex")
                );
                //This block of code is for all players 2,3,4, only for skiped questions
                if (this.getSessionAttribute("MultiplayerQuizIntent")) {
                  console.log(" currentPlayerName -> " + this.getSessionAttribute("currentPlayerName"));
                  console.log(" 1-> " + this.getSessionAttribute("currentQuestionIndex"));
                  if (this.getSessionAttribute("numberOfPlayers") === 2) {
                    if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("firstPlayerName")) {
                      this.setSessionAttribute("firstPlayerTotalQue",
                        this.getSessionAttribute("firstPlayerTotalQue") + 1);
                      this.setSessionAttribute("firstPlayerSkipedQue",
                        this.getSessionAttribute("firstPlayerSkipedQue") + 1);

                      this.setSessionAttribute("firstPlayerSet",
                        this.getSessionAttribute("firstPlayerName") +
                        ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                        ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                      );

                      //Here the initial value of MqQuestionCount will be 0, if the first user skips the question 
                      //then that count will be 1 and the same question will pass to the next player and so on and
                      // at lastwhen none of them will answer it will display the correct answer and set count to 
                      //again 0 and pass the next question to next player.
                      if (MqQuestioncount === 0) {
                        console.log(" 2-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("secondPlayerIntent");
                      } else {
                        console.log(" 3-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 2 === 0) {
                          console.log(" 4-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        } else {
                          console.log(" 5-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }
                    } else if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("secondPlayerName")) {
                      this.setSessionAttribute("secondPlayerTotalQue",
                        this.getSessionAttribute("secondPlayerTotalQue") + 1);
                      this.setSessionAttribute("secondPlayerSkipedQue",
                        this.getSessionAttribute("secondPlayerSkipedQue") + 1);

                      this.setSessionAttribute("secondPlayerSet",
                        this.getSessionAttribute("secondPlayerName") +
                        ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                        ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                      );

                      if (MqQuestioncount === 0) {
                        console.log(" 9-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("firstPlayerIntent");
                      } else {
                        console.log("correct answer is");
                        console.log(" 6-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 2 === 0) {
                          console.log(" 7-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        } else {
                          console.log(" 8-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }

                    }
                  } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                    if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("firstPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("firstPlayerTotalQue",
                        this.getSessionAttribute("firstPlayerTotalQue") + 1);
                      this.setSessionAttribute("firstPlayerSkipedQue",
                        this.getSessionAttribute("firstPlayerSkipedQue") + 1);

                      this.setSessionAttribute("firstPlayerSet",
                        this.getSessionAttribute("firstPlayerName") +
                        ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                        ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1) {
                        console.log(" 10-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("secondPlayerIntent");
                      }
                      else {
                        console.log(" 12-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 3 === 0) {
                          console.log(" 13-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 3 === 1) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        }
                        else {
                          console.log(" 15-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }
                    } else if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("secondPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("secondPlayerTotalQue",
                        this.getSessionAttribute("secondPlayerTotalQue") + 1);
                      this.setSessionAttribute("secondPlayerSkipedQue",
                        this.getSessionAttribute("secondPlayerSkipedQue") + 1);

                      this.setSessionAttribute("secondPlayerSet",
                        this.getSessionAttribute("secondPlayerName") +
                        ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                        ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1) {
                        console.log(" 17-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("thirdPlayerIntent");
                      }
                      else {
                        console.log(" 18-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 3 === 0) {
                          console.log(" 19-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 3 === 1) {
                          console.log(" 20-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        }
                        else {
                          console.log(" 21-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }

                    } else if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("thirdPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("thirdPlayerTotalQue",
                        this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                      this.setSessionAttribute("thirdPlayerSkipedQue",
                        this.getSessionAttribute("thirdPlayerSkipedQue") + 1);

                      this.setSessionAttribute("thirdPlayerSet",
                        this.getSessionAttribute("thirdPlayerName") +
                        ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                        ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1) {
                        console.log(" 22-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("firstPlayerIntent");
                      } else {
                        console.log(" 24-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 3 === 0) {
                          console.log(" 25-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 3 === 1) {
                          console.log(" 26-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        }
                        else {
                          console.log(" 27-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }
                    }
                  } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                    if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("firstPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("firstPlayerTotalQue",
                        this.getSessionAttribute("firstPlayerTotalQue") + 1);
                      this.setSessionAttribute("firstPlayerSkipedQue",
                        this.getSessionAttribute("firstPlayerSkipedQue") + 1);

                      this.setSessionAttribute("firstPlayerSet",
                        this.getSessionAttribute("firstPlayerName") +
                        ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                        ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1 || MqQuestioncount === 2) {
                        console.log(" 10-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("secondPlayerIntent");
                      } else {
                        console.log(" 12-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 0) {
                          console.log(" 13-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 1) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("fourthPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("fourthPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 2) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        } else {
                          console.log(" 15-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }
                    } else if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("secondPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("secondPlayerTotalQue",
                        this.getSessionAttribute("secondPlayerTotalQue") + 1);
                      this.setSessionAttribute("secondPlayerSkipedQue",
                        this.getSessionAttribute("secondPlayerSkipedQue") + 1);

                      this.setSessionAttribute("secondPlayerSet",
                        this.getSessionAttribute("secondPlayerName") +
                        ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                        ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1 || MqQuestioncount === 2) {
                        console.log(" 17-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("thirdPlayerIntent");
                      } else {
                        console.log(" 18-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 0) {
                          console.log(" 13-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 1) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("fourthPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("fourthPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 2) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        } else {
                          console.log(" 15-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }

                    } else if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("thirdPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("thirdPlayerTotalQue",
                        this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                      this.setSessionAttribute("thirdPlayerSkipedQue",
                        this.getSessionAttribute("thirdPlayerSkipedQue") + 1);

                      this.setSessionAttribute("thirdPlayerSet",
                        this.getSessionAttribute("thirdPlayerName") +
                        ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                        ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1 || MqQuestioncount === 2) {
                        console.log(" 22-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("fourthPlayerIntent");
                      } else {
                        console.log(" 24-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 0) {
                          console.log(" 13-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 1) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("fourthPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("fourthPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 2) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("firstPlayerName");
                        } else {
                          console.log(" 15-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }
                    } else if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("fourthPlayerName")) {
                      console.log("MqQuestioncount " + MqQuestioncount);
                      this.setSessionAttribute("fourthPlayerTotalQue",
                        this.getSessionAttribute("fourthPlayerTotalQue") + 1);
                      this.setSessionAttribute("fourthPlayerSkipedQue",
                        this.getSessionAttribute("fourthPlayerSkipedQue") + 1);

                      this.setSessionAttribute("fourthPlayerSet",
                        this.getSessionAttribute("fourthPlayerName") +
                        ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
                        ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
                        ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
                        ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
                      );
                      if (MqQuestioncount === 0 || MqQuestioncount === 1 || MqQuestioncount === 2) {
                        console.log(" 22-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount += 1;
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex")
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("IntentStateForMQ", "");
                        this.setSessionAttribute("speechOutput", "");
                        await this.toIntent("firstPlayerIntent");
                      } else {
                        console.log(" 24-> " + this.getSessionAttribute("currentQuestionIndex"));
                        MqQuestioncount = 0;
                        console.log("correct answer is");
                        if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 0) {
                          console.log(" 13-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // + " Next question for " + this.getSessionAttribute("thirdPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 1) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("fourthPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // + " Next question for " + this.getSessionAttribute("fourthPlayerName");
                        } else if ((this.getSessionAttribute("currentQuestionIndex") - 1) % 4 === 2) {
                          console.log(" 14-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          // + " Next question for " + this.getSessionAttribute("firstPlayerName");
                        } else {
                          console.log(" 15-> " + this.getSessionAttribute("currentQuestionIndex"));
                          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
                          speechOutput = " The answer for this question is - " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - " +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            ". "
                          //  + " Next question for " + this.getSessionAttribute("secondPlayerName");
                        }
                        this.setSessionAttribute("speechOutput", speechOutput);
                        this.setSessionAttribute(
                          "currentQuestionIndex",
                          this.getSessionAttribute("currentQuestionIndex") + 1
                        );
                        this.setSessionAttribute(
                          "TestIndex",
                          this.getSessionAttribute("TestIndex") + 1
                        );
                        this.setSessionAttribute("IntentStateForMQ", "showingCorrectAnswer");
                        await this.toIntent("ContinueGetNextQuestion");
                      }
                    }
                  }
                } else {
                  //To store the notattempted qId and weightage in db  
                  this.setSessionAttribute(
                    "notAttemptedQuestionId",
                    this.getSessionAttribute("notAttemptedQuestionId") +
                    this.getSessionAttribute("questionId") +
                    " ");
                  this.setSessionAttribute(
                    "notAttemptedQuestionWeightage",
                    this.getSessionAttribute("notAttemptedQuestionWeightage") +
                    this.getSessionAttribute("questionWeightage") +
                    " ");
                  // To store not attempted questions in db based on challenge level
                  if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
                    this.setSessionAttribute(
                      "challengeStarterCountNotAttemptedAns",
                      this.getSessionAttribute("challengeStarterCountNotAttemptedAns") + 1
                    );
                  } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
                    this.setSessionAttribute(
                      "challengeIntermediateCountNotAttemptedAns",
                      this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns") + 1
                    );
                  } else {
                    this.setSessionAttribute(
                      "challengeAdvanceCountNotAttemptedAns",
                      this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns") + 1
                    );
                  }
                  console.log("strater -> " + this.getSessionAttribute("challengeStarterCountNotAttemptedAns"));
                  console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns"));
                  console.log("advance -> " + this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns"));

                  this.setSessionAttribute(
                    "currentQuestionIndex",
                    this.getSessionAttribute("currentQuestionIndex") + 1
                  );
                  this.setSessionAttribute(
                    "TestIndex",
                    this.getSessionAttribute("TestIndex") + 1
                  );
                  this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
                  this.setSessionAttribute("speechOutput", "");


                  // if (this.getSessionAttribute("twoPlayersQuiz")) {
                  //   if (this.getSessionAttribute("currentQuestionIndex") % 2 === 0) {
                  //     await this.toIntent("SecondPlayer");
                  //   } else {
                  //     await this.toIntent("FirstPlayer");
                  //   }
                  // } 
                  this.setSessionAttribute(
                    "currentQuestionIndex",
                    this.getSessionAttribute("currentQuestionIndex") + 1
                  );
                  this.setSessionAttribute(
                    "TestIndex",
                    this.getSessionAttribute("TestIndex") + 1
                  );
                  await this.toIntent("ContinueGetNextQuestion");
                }
              }
            }
          } else {
            speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>";
            repromptSpeech = speechOutput; //meenu
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
          }
        }
      }
    },

    listTopicByTag: async function () {
      console.log('list topics listTopicByTag -> ')
      if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
        this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
        this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
        var txt = help.GetContexBasedHelp(
          this.getSessionAttribute("state"),
          this.getSessionAttribute("UserState"),
          this
        );
        console.log("Help me text -> " + txt);
        console.log("User State-> " + this.getSessionAttribute("UserState"));
        console.log("Quiz State-> " + this.getSessionAttribute("state"));
        this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      } else {
        console.log('list topics -> ' + this.$inputs.topicTag.value);
        this.setSessionAttribute("IntentStateOfTopic", "listTopicByTag");
        await this.toIntent('AvailableTopics');
      }
    },

    AvailableTopics: async function () {
      console.log("Starting mainStateHandler:AvailableTopics");
      console.log("Starting:AvailableTopics " + this.getSessionAttribute("UserId"));
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      // this.attributes['IntentState'] = "TopicsAvailable";
      // this.setSessionAttribute("IntentState", "AvailableTopics");
      // this.setSessionAttribute("ListTopics", this.$inputs.topicName.value);
      var msg, speechOutput = "", repromptSpeech = "";
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
        } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        } else {
          var topicList = [];
          var i;
          let rows, queryTopic;

          this.setSessionAttribute("IntentState", "AvailableTopics");
          console.log("intentstate -> " + this.getSessionAttribute("IntentStateOfTopic"));
          if (this.getSessionAttribute("IntentStateOfTopic") === "listTopicByTag" &&
            this.getSessionAttribute("IntentStateOfTopic") !== null) {
            queryTopic = "SELECT * from topic where (tags LIKE '%" + this.$inputs.topicTag.value +
              "%' || name LIKE '%" + this.$inputs.topicTag.value +
              "%' || description LIKE '%" + this.$inputs.topicTag.value +
              "%' || shortDesc LIKE '%" + this.$inputs.topicTag.value +
              "%') and organizationId IS NULL";
            console.log(' rows2 -> ' + queryTopic);
            this.setSessionAttribute("IntentStateOfTopic", null);
          }
          else {
            queryTopic =
              "select * from topic left join userTopic_xRef on topic.topicId = userTopic_xRef.topicId where userTopic_xRef.userId=" +
              this.getSessionAttribute("UserId") +
              " and userTopic_xRef.isSubscribed=1";
            console.log(' rows -> ' + queryTopic);
          }
          rows = await functionCalls.callAPI(queryTopic);
          this.setSessionAttribute("listTopicsLength", rows.length);
          console.log(
            "THE LENFTH OF THE LIST TOPICS : " + rows.length + " -- " + rows
          );
          if (rows.length > 0 && rows) {
            console.log("Row data in topics intent -> " + JSON.stringify(rows));
            topicName = [];
            for (i = 0; i < rows.length; i++) {
              topicName[i] = rows[i].name;
            }
            topicCounter = topicName.length;

            if (topicName.length > 3) {
              for (var j = 0; j < 3; j++) {
                // topicList[j] = "<s>" + topicName[j] + "</s>";
                topicList[j] = topicName[j] + '<break time="350ms"/>';
                topicCounter--;
                lastIndex = j;
              }
              speechOutput =
                "Okay! ," +
                topicList +
                ", Say - Open and the topic name, or say - more topics to get other topics";
              repromptSpeech =
                "  To start quiz, Say - Open and the topic name, or say - more topics to get other topics";
              console.log(' ............... ' + speechOutput);
            } else {
              for (var j = 0; j < topicName.length; j++) {
                // topicList[j] = "<s>" + topicName[j] + "</s>"; 
                topicList[j] = topicName[j] + '<break time="350ms"/>';
              }
              speechOutput =
                "Available topics are " +
                topicList +
                ", To start quiz, Say - Open and the topic name.";
              console.log(speechOutput);
            }
            repromptSpeech = " Say, open and the topic name to start the quiz "; //meenu
            console.log(' ............... ' + speechOutput);
            this.ask(
              speechOutput,
              repromptSpeech, repromptSpeech, goodByeMsg
            );
          } else {
            speechOutput = "No topics found in your subscribed topics, please go and subscribe the topics";
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
          }
        }
      }
    },

    MoreTopics: async function () {
      console.log("More topics Intent handler");
      var msg, speechoutput = "", repromptSpeech = "";
      var moreTopicList = [];
      console.log("MORE LISTINGS" + topicName);
      console.log("LAST INDEX " + lastIndex);
      if (!this.getSessionAttribute("UserId")) {
        speechoutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechoutput;
        this.ask(
          speechoutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
      } else {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
        } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else {
          var startIndex = lastIndex + 1;
          console.log(
            "StartingIndex " +
            startIndex +
            " Array Counter Index -> " +
            topicCounter
          );

          this.setSessionAttribute("IntentState", "MoreTopics");
          if (this.getSessionAttribute("listTopicsLength") > 0) {
            if (
              topicName.length > 3 &&
              this.getSessionAttribute("listTopicsLength") > 0
            ) {
              if (topicCounter > 3) {
                for (var j = startIndex; j < startIndex + 3; j++) {
                  if (j > topicName.length) {
                    speechoutput = "No more results";
                  } else {
                    // moreTopicList[j] = "<s>" + topicName[j] + "</s>";
                    moreTopicList[j] = topicName[j] + '<break time="350ms"/>';
                    moreTopicList[j] = topicName[j] + '<break time="350ms"/>';
                    topicCounter--;
                    lastIndex = j;
                    console.log("TopicCOunter : " + topicCounter + "<" + 3);
                    if (topicCounter < 1)
                      // speechoutput =
                      //   "<emphasis level='moderate'>Okay! here is the next list of topics, are  " +
                      //   moreTopicList +
                      //   "</emphasis>";
                      speechoutput =
                        "Okay! here is the next list of topics, are  " +
                        moreTopicList;
                    else
                      // speechoutput =
                      //   '<emphasis level="moderate"> Okay! Here is the next 3 list of topics,  are ' +
                      //   moreTopicList +
                      //   ' or, <break time="300ms"/>  say, more topics for more topic listings </emphasis>';
                      speechoutput =
                        'Okay! Here is the next 3 list of topics,  are ' +
                        moreTopicList +
                        ' or, <break time="300ms"/>  say, more topics for more topic listings';
                  }
                }
              } else {
                for (var j = startIndex; j < topicName.length; j++) {
                  // moreTopicList[j] = "<s>" + topicName[j] + "</s>";
                  moreTopicList[j] = topicName[j] + '<break time="350ms"/>';
                }
                speechoutput =
                  "Okay, you have,  " +
                  moreTopicList +
                  ", To start quiz, Say - Open and the topic name.";
                console.log(speechoutput);
              }
            } else {  // If the subscribed are less than 3, then in more topics it should list all the topics
              if (topicName.length <= 3) {
                startIndex = 0;
              } else {
                startIndex = startIndex;
              }
              for (var j = startIndex; j < topicName.length; j++) {
                // moreTopicList[j] = "<s>" + topicName[j] + "</s>";
                moreTopicList[j] = topicName[j] + '<break time="350ms"/>';
              }
              speechoutput =
                "Okay, you have,  only " +
                topicName.length +
                " topics, " +
                moreTopicList +
                ", To start quiz, Say - Open and the topic name.";
              console.log(speechoutput);
            }
            repromptSpeech = "To start quiz, Say - Open and the topic name.";
            this.ask(speechoutput, repromptSpeech, repromptSpeech, goodByeMsg);
          } else {
            await this.toIntent("AvailableTopics");
            //     "No topics found in your subscribed topic list. please go and subscribe for the topics";
          }
          // repromptSpeech = "To start quiz, Say - Open and the topic name.";
          // this.ask(speechoutput, repromptSpeech, repromptSpeech, goodByeMsg);
        }
      }
    },


    CatchAllIntent: async function () {
      var msg;
      console.log("CatchAllSlot Intent");
      this.setSessionAttribute("IntentState", "CatchAllSlot");

      if (
        help.isValidContext(
          this.getSessionAttribute("IntentState"),
          this.getSessionAttribute("state"),
          this
        )
      ) {
        msg = help.isValidContext(
          this.getSessionAttribute("IntentState"),
          this.getSessionAttribute("state"),
          this
        );
        console.log("CatchAllSlot isValidContext msg -> " + msg);
        this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
      } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
        this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
        this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
        var txt = help.GetContexBasedHelp(
          this.getSessionAttribute("state"),
          this.getSessionAttribute("UserState"),
          this
        );
        console.log("Help me text -> " + txt);
        console.log("User State-> " + this.getSessionAttribute("UserState"));
        console.log("Quiz State-> " + this.getSessionAttribute("state"));
        this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      } else {
        //things to do
        this.ask(
          RepeatText +
          "if you dont know what to say, get help by saying help me.",
          " Say Exit to end the session", goodByeMsg
        );
      }
    },

    //Dialogflow testing purpose //Meenu
    END() {
      console.log("AMAZON STOP INTENT");
      console.log('Intent -> ' + this.getSessionAttribute('IntentState'))
      return this.toIntent("endOfTest");
    },


    'AMAZON.StopIntent': async function () {
      console.log("Starting mainStateHandler:AMAZON.StopIntent");
      this.setSessionAttribute('IntentState', 'StopIntent');
      // State Automatically Saved with :tell
      // VoiceLabs.track(this.event.session, "AMAZON.StopIntent", null, null, (error, response) => {
      // app.tell(constants.exitMessage);

      await this.toIntent("endOfTest");
      // });
    },

    "AMAZON.CancelIntent": function () {
      console.log("Starting mainStateHandler:CancelIntent");
      // State Automatically Saved with :tell
      // VoiceLabs.track(this.event.session, "AMAZON.CancelIntent", null, null, (error, response) => {
      this.tell(constants.exitMessage);
      // });
    },

    SessionEndedRequest: function () {
      // Force State Save When User Times Out
      console.log("Starting mainStateHandler:SessionEndedRequest");
      // VoiceLabs.track(this.event.session, "SessionEndedRequest", null, null, (error, response) => {
      //this.emit(':saveState', true);
      this.tell(constants.exitMessage);
      // });
    },

    "AMAZON.HelpIntent": function () {
      console.log("AMAZON.HelpIntent");
      var txt = help.GetContexBasedHelp(
        this.getSessionAttribute("state"),
        this.getSessionAttribute("UserState"),
        this
      );
      console.log("Help me text -> " + txt);
      console.log("User State-> " + this.getSessionAttribute("UserState"));
      console.log("Quiz State-> " + this.getSessionAttribute("state"));
      // VoiceLabs.track(this.event.session, "mainStateHandlers:AMAZON.HelpIntent", null, null, (error, response) => {
      this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      // });
    },

    HowToUse: function () {
      console.log('  IntentState  ---> ' + this.getSessionAttribute("IntentState"));
      console.log('  state  ---> ' + this.getSessionAttribute("state"));
      console.log('  UserState  ---> ' + this.getSessionAttribute("UserState"));
      // VoiceLabs.track(this.event.session, "mainStateHandlers:HowToUse", null, null, (error, response) => {
      var repromptSpeech = "Say <s>Help me!</s> to repeat";
      if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
        this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
        this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
        this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
        var txt = help.GetContexBasedHelp(
          this.getSessionAttribute("state"),
          this.getSessionAttribute("UserState"),
          this
        );
        console.log("Help me text -> " + txt);
        console.log("User State-> " + this.getSessionAttribute("UserState"));
        console.log("Quiz State-> " + this.getSessionAttribute("state"));
        this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      } else
        // this.ask(help.GetHowToUse("_MAIN", null, this), repromptSpeech, repromptSpeech, goodByeMsg);
        this.ask(help.GetHowToUse(this.getSessionAttribute("state"), this.getSessionAttribute("UserState"), this), repromptSpeech, repromptSpeech, goodByeMsg);
      // });
    },

    Unhandled: function () {
      console.log(
        "CURRENT UNHANDLED Intent " + this.getSessionAttribute("IntentState")
      );
      var speechOutput = "", repromptSpeech = "";
      if (this.getSessionAttribute("IntentState") == "IdentifyUser" && !this.getSessionAttribute("UserId"))
        this.UnhandledIdentifyUser();
      else if (this.getSessionAttribute("IntentState") == "SwitchUser") {
        speechOutput = "Please tell me the PIN number"; //meenu
        repromptSpeech = speechOutput;
        this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
      }
      else if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Please tell me the PIN number"; //meenu
        repromptSpeech = speechOutput;
        this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
      }
      else if (this.getSessionAttribute("listTopicsLength") <= 0)
      // this.ask("Please say list topics to list the subscribed topics");
      {
        speechOutput = "Please say list topics to list the subscribed topics"; //meenu
        repromptSpeech = speechOutput;
        this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
      }
      //After identifying user, if the user says more topics directly instead of saying list topics 
      else if (this.getSessionAttribute("IntentState") == "IdentifyUser" || this.getSessionAttribute("IntentState") == "IdentifyUserByPin")
      // this.ask("Please say list topics to list the subscribed topics");
      {
        if (this.getSessionAttribute("organizationId"))
          speechOutput = "Please say list topics to list the subscribed topics"; //meenu
        else
          speechOutput = "Please say list topics to list the subscribed topics or to enable multiplayer quiz, say multiplayer or multiuser";
        repromptSpeech = speechOutput;
        this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
      }
      else {
        console.log("Starting Unhandled");
        var txt = help.ProcessUnHandledRequest(
          this.getSessionAttribute("UserState"), this
        );
        console.log("Unhandled txt -> " + txt);
        this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      }
    },

    waitIntent: async function () {
      console.log("Starting waitIntent");
      await this.toIntent("AMAZON.PauseIntent");
    },

    "AMAZON.PauseIntent": function () {
      console.log("Starting AMAZON.PauseIntent");
      // VoiceLabs.track(this.event.session, "questionAnswerHandlers:PauseIntent", null, null, (error, response) => {
      var repromptSpeech = "Say <s> Wait!</s> for more time";
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      this.ask("Ok!", repromptSpeech, repromptSpeech, goodByeMsg);
      // });
    },

    DontKnowIntent: function () {
      this.setSessionAttribute("IntentState", "DontKnowIntent");
      console.log("Starting DontKnowIntent");
      var speechOutput = "", repromptSpeech = "";
      if (
        this.getSessionAttribute("IntentState") === "populateQuestions" ||
        this.getSessionAttribute("IntentState") === "getNextQuestion"
      ) {
        var opt = this.getSessionAttribute("optionsChoice");
        // VoiceLabs.track(this.event.session, "questionAnswerHandlers:DontKnowIntent", null, null, (error, response) => {
        speechOutput = "Sorry! did not get you, answer the question by saying - the answer is " + opt; //meenu
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
        );
        // });
      } else {
        // VoiceLabs.track(this.event.session, "DontKnowIntent", null, null, (error, response) => {
        this.ask(
          RepeatText +
          "if you dont know what to say, get help by saying help me.",
          " Say Exit to end the session", goodByeMsg
        );
        // });
      }
    },

    //Code for multiplayer quiz :- 
    //This intent to ask the number of players wants to play.It will be called when the user say utterances like multiuser or multiplayer
    MultiplayerQuizIntent: async function () {
      console.log("MultiplayerQuizIntent");
      console.log("organisation id -> " + this.getSessionAttribute("organizationId"));
      var speechOutput = "", repromptSpeech = "", msg;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        // if (!this.getSessionAttribute("organizationId") ||
        //   this.getSessionAttribute("organizationId") === null ||
        //   this.getSessionAttribute("organizationId") === undefined) {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("MultiplayerQuizIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
          this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
          this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
          this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
          var txt = help.GetContexBasedHelp(
            this.getSessionAttribute("state"),
            this.getSessionAttribute("UserState"),
            this
          );
          console.log("Help me text -> " + txt);
          console.log("User State-> " + this.getSessionAttribute("UserState"));
          console.log("Quiz State-> " + this.getSessionAttribute("state"));
          this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        } else {
          if (!this.getSessionAttribute("organizationId") ||
            this.getSessionAttribute("organizationId") === null ||
            this.getSessionAttribute("organizationId") === undefined) {
            this.setSessionAttribute("IntentState", "MultiplayerQuizIntent");
            this.setSessionAttribute("MultiplayerQuizIntent", "Yes");
            this.setSessionAttribute("state", constants.states.ONBOARDING);
            this.setSessionAttribute("UserState", constants.UserState.QuizType);
            this.ask("How many people wants to play ? Minimum two and maximum four people can play.");
          }
          // }
          else {
            this.ask("Multiplayer quiz is not applicable for organisation related persons, it's only for general users. " +
              " Say list topics to list all the subscribed topics or to open a quiz say open and the topic name, " +
              " for example open science");
          }
        }
      }
    },

    //To check wheather the given number of players are valid or not
    NumberOfPlayersIntent: async function () {
      console.log("NumberOfPlayersIntent");
      console.log("..... " + this.getSessionAttribute("IntentState"));
      console.log("UserState " + this.getSessionAttribute("UserState"))
      console.log("..... " + this.getSessionAttribute("state"));
      var speechOutput = "", repromptSpeech = "", msgObject, msg;
      // const numberOfPlayers = this.$inputs.numberOfPlayers.value;
      console.log("..... " + this.getSessionAttribute("numberOfPlayers"));
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        // if (!this.getSessionAttribute("organizationId") ||
        //   this.getSessionAttribute("organizationId") === null ||
        //   this.getSessionAttribute("organizationId") === undefined) {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("MultiplayerQuizIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        }
        else {
          if (!this.getSessionAttribute("organizationId") ||
            this.getSessionAttribute("organizationId") === null ||
            this.getSessionAttribute("organizationId") === undefined) {
            if (this.getSessionAttribute("MultiplayerQuizIntent") === "Yes") {
              const numberOfPlayers = parseInt(this.$inputs.countOfPlayers.value);
              if (!numberOfPlayers) {
                msg = help.GetContexBasedHelp(
                  this.getSessionAttribute("state"),
                  this.getSessionAttribute("UserState"),
                  this
                );
                console.log("FirstPlayerNameIntent isValidContext msg -> " + msg);
                this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
              } else if (this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
                this.ask("<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s> or, say <s>list topics  </s>");
              } else {
                this.setSessionAttribute("IntentState", "NumberOfPlayersIntent");
                this.setSessionAttribute("state", constants.states.ONBOARDING);
                this.setSessionAttribute("UserState", constants.UserState.NoOfPlayersSelected);
                this.setSessionAttribute("numberOfPlayers", numberOfPlayers);
                if (numberOfPlayers < 2 || numberOfPlayers > 4) {
                  this.ask("Maximaum 4 and minimum 2 people can play, please choose from 2 to 4.");
                }
                else {
                  if (this.getSessionAttribute("firstPlayerName")) {
                    msgObject = await functionCalls.toCheckPlayersName(this, this.getSessionAttribute("numberOfPlayers"),   //This function is used to check all the players name have been given by the user or not
                      this.getSessionAttribute("firstPlayerName"), this.getSessionAttribute("secondPlayerName"),
                      this.getSessionAttribute("thirdPlayerName"), this.getSessionAttribute("fourthPlayerName"));
                    msg = msgObject.msg;
                    console.log("intentstate1 -> " + msgObject.intentState);
                    if (msgObject.intentState === "toCheckPlayersName") {
                      this.setSessionAttribute("IntentState", "toCheckPlayersName");
                      this.setSessionAttribute("state", constants.states.MAIN);
                      this.setSessionAttribute("UserState", constants.UserState.UserSelected);
                    }
                    this.ask(msg);
                  } else
                    this.ask("Okay! Please tell me the first player name by saying, first player name is and the player name.");
                }
              }
            } else {
              this.ask("To enable multiplayer quiz mode, please say multiuser or multiplayer. You can also say list topics " +
                "to list the subscribed topics or to open the quiz say open and the topic name, example open vocabulary.");
            }
          }
          // }
          else {
            this.ask("Multiplayer quiz is not applicable for organisation related persons, it's only for general users. " +
              " Say list topics to list all the subscribed topics or to open a quiz say open and the topic name, " +
              " for example open science");
          }
        }
      }
    },

    //To take the first player name
    FirstPlayerNameIntent: async function () {
      console.log("FirstPlayerNameIntent");
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      console.log("UserState " + this.getSessionAttribute("UserState"))
      console.log("state " + this.getSessionAttribute("state"))
      var speechOutput = "", repromptSpeech = "", msgObject, msg;
      var firstPlayerName = this.$inputs.firstPlayer.value;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        // if (!this.getSessionAttribute("organizationId") ||
        //   this.getSessionAttribute("organizationId") === null ||
        //   this.getSessionAttribute("organizationId") === undefined) {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        } else {
          if (!this.getSessionAttribute("organizationId") ||
            this.getSessionAttribute("organizationId") === null ||
            this.getSessionAttribute("organizationId") === undefined) {
            if (this.getSessionAttribute("MultiplayerQuizIntent") === "Yes") {
              if (!firstPlayerName && this.getSessionAttribute("numberOfPlayers")) {
                msg = help.GetContexBasedHelp(
                  this.getSessionAttribute("state"),
                  this.getSessionAttribute("UserState"),
                  this
                );
                console.log("FirstPlayerNameIntent isValidContext msg -> " + msg);
                this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
              } else if (this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
                this.ask("<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s> or, say <s>list topics  </s>");
              } else {
                this.setSessionAttribute("IntentState", "FirstPlayerNameIntent");
                this.setSessionAttribute("firstPlayerName", this.$inputs.firstPlayer.value)
                this.setSessionAttribute("state", constants.states.ONBOARDING);
                this.setSessionAttribute("UserState", constants.UserState.FirstPlayerNameGiven);
                console.log("number of players " + this.getSessionAttribute("numberOfPlayers"));
                msgObject = await functionCalls.toCheckPlayersName(this, this.getSessionAttribute("numberOfPlayers"),   //This function is used to check all the players name have been given by the user or not
                  this.getSessionAttribute("firstPlayerName"), this.getSessionAttribute("secondPlayerName"),
                  this.getSessionAttribute("thirdPlayerName"), this.getSessionAttribute("fourthPlayerName"));
                msg = msgObject.msg;
                console.log("intentstate1 -> " + msgObject.intentState);
                if (msgObject.intentState === "toCheckPlayersName") {
                  this.setSessionAttribute("IntentState", "toCheckPlayersName");
                  this.setSessionAttribute("state", constants.states.MAIN);
                  this.setSessionAttribute("UserState", constants.UserState.UserSelected);
                }
                this.ask(msg);
              }
            } else {
              this.ask("To enable multiplayer quiz mode, please say multiuser or multiplayer. You can also say list topics " +
                "to list the subscribed topics or to open the quiz say open and the topic name, example open vocabulary.");
            }
          }
          // }
          else {
            this.ask("Multiplayer quiz is not applicable for organisation related persons, it's only for general users. " +
              " Say list topics to list all the subscribed topics or to open a quiz say open and the topic name, " +
              " for example open science");
          }
        }
      }
    },

    //To take the second player name
    SecondPlayerNameIntent: async function () {
      console.log("SecondPlayerNameIntent");
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      console.log("UserState " + this.getSessionAttribute("UserState"))
      console.log("state " + this.getSessionAttribute("state"))
      var speechOutput = "", repromptSpeech = "", msgObject, msg;
      var secondPlayerName = this.$inputs.second_player.value;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        // if (!this.getSessionAttribute("organizationId") ||
        //   this.getSessionAttribute("organizationId") === null ||
        //   this.getSessionAttribute("organizationId") === undefined) {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        } else {
          if (!this.getSessionAttribute("organizationId") ||
            this.getSessionAttribute("organizationId") === null ||
            this.getSessionAttribute("organizationId") === undefined) {
            if (this.getSessionAttribute("MultiplayerQuizIntent") === "Yes") {
              if (!secondPlayerName && this.getSessionAttribute("numberOfPlayers")) {
                msg = help.GetContexBasedHelp(
                  this.getSessionAttribute("state"),
                  this.getSessionAttribute("UserState"),
                  this
                );
                console.log("SecondPlayerNameIntent isValidContext msg -> " + msg);
                this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
              } else if (this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
                this.ask("<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s> or, say <s>list topics  </s>");
              } else {
                this.setSessionAttribute("IntentState", "SecondPlayerNameIntent");
                this.setSessionAttribute("secondPlayerName", this.$inputs.second_player.value);
                this.setSessionAttribute("state", constants.states.ONBOARDING);
                this.setSessionAttribute("UserState", constants.UserState.SecondPlayerNameGiven);
                msgObject = await functionCalls.toCheckPlayersName(this, this.getSessionAttribute("numberOfPlayers"),
                  this.getSessionAttribute("firstPlayerName"), this.getSessionAttribute("secondPlayerName"),
                  this.getSessionAttribute("thirdPlayerName"), this.getSessionAttribute("fourthPlayerName"));
                msg = msgObject.msg;
                console.log("intentstate -> " + msgObject.intentState);
                if (msgObject.intentState === "toCheckPlayersName") {
                  this.setSessionAttribute("IntentState", "toCheckPlayersName");
                  this.setSessionAttribute("state", constants.states.MAIN);
                  this.setSessionAttribute("UserState", constants.UserState.UserSelected);
                }
                this.ask(msg);
              }
            }
            else {
              this.ask("To enable multiplayer quiz mode, please say multiuser or multiplayer. You can also say list topics " +
                "to list the subscribed topics or to open the quiz say open and the topic name, example open vocabulary.");
            }
          }
          // }
          else {
            this.ask("Multiplayer quiz is not applicable for organisation related persons, it's only for general users. " +
              " Say list topics to list all the subscribed topics or to open a quiz say open and the topic name, " +
              " for example open science");
          }
        }
      }
    },

    //To take the third player name
    ThirdPlayerNameIntent: async function () {
      console.log("ThirdPlayerNameIntent");
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      console.log("UserState " + this.getSessionAttribute("UserState"))
      console.log("state " + this.getSessionAttribute("state"))
      var speechOutput = "", repromptSpeech = "", msgObject, msg;
      var thirdPlayerName = this.$inputs.third_player.value;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        // if (!this.getSessionAttribute("organizationId") ||
        //   this.getSessionAttribute("organizationId") === null ||
        //   this.getSessionAttribute("organizationId") === undefined) {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        } else {
          if (!this.getSessionAttribute("organizationId") ||
            this.getSessionAttribute("organizationId") === null ||
            this.getSessionAttribute("organizationId") === undefined) {
            if (this.getSessionAttribute("MultiplayerQuizIntent") === "Yes") {
              if (!thirdPlayerName && this.getSessionAttribute("numberOfPlayers")) {
                msg = help.GetContexBasedHelp(
                  this.getSessionAttribute("state"),
                  this.getSessionAttribute("UserState"),
                  this
                );
                console.log("SecondPlayerNameIntent isValidContext msg -> " + msg);
                this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
              } else if (this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
                this.ask("<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s> or, say <s>list topics  </s>");
              } else {
                this.setSessionAttribute("IntentState", "ThirdPlayerNameIntent");
                this.setSessionAttribute("thirdPlayerName", this.$inputs.third_player.value);
                this.setSessionAttribute("state", constants.states.ONBOARDING);
                this.setSessionAttribute("UserState", constants.UserState.ThirdPlayerNameGiven);
                msgObject = await functionCalls.toCheckPlayersName(this, this.getSessionAttribute("numberOfPlayers"),
                  this.getSessionAttribute("firstPlayerName"), this.getSessionAttribute("secondPlayerName"),
                  this.getSessionAttribute("thirdPlayerName"), this.getSessionAttribute("fourthPlayerName"));
                msg = msgObject.msg;
                if (msgObject.intentState === "toCheckPlayersName") {
                  this.setSessionAttribute("IntentState", "toCheckPlayersName");
                  this.setSessionAttribute("state", constants.states.MAIN);
                  this.setSessionAttribute("UserState", constants.UserState.UserSelected);
                }
                this.ask(msg);
              }
            } else {
              this.ask("To enable multiplayer quiz mode, please say multiuser or multiplayer. You can also say list topics " +
                "to list the subscribed topics or to open the quiz say open and the topic name, example open vocabulary.");
            }
          }
          // }
          else {
            this.ask("Multiplayer quiz is not applicable for organisation related persons, it's only for general users. " +
              " Say list topics to list all the subscribed topics or to open a quiz say open and the topic name, " +
              " for example open science");
          }
        }
      }
    },

    //To take the fourth player name
    FourthPlayerNameIntent: async function () {
      console.log("FourthPlayerNameIntent");
      console.log("Intentstate " + this.getSessionAttribute("IntentState"));
      console.log("UserState " + this.getSessionAttribute("UserState"))
      console.log("state " + this.getSessionAttribute("state"))
      var speechOutput = "", repromptSpeech = "", msgObject, msg;
      var fourthPlayerName = this.$inputs.fourth_player.value;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        // if (!this.getSessionAttribute("organizationId") ||
        //   this.getSessionAttribute("organizationId") === null ||
        //   this.getSessionAttribute("organizationId") === undefined) {
        if (
          help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          )
        ) {
          msg = help.isValidContext(
            this.getSessionAttribute("IntentState"),
            this.getSessionAttribute("state"),
            this
          );
          console.log("TopicIntent isValidContext msg -> " + msg);
          this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
        } else if ((this.getSessionAttribute("IntentState") === "RepeatQuestion" ||
          this.getSessionAttribute("IntentState") === "RepeatOptions") &&
          this.getSessionAttribute("state") === "_QUESTIONANSWER") {
          var opt = this.getSessionAttribute("optionsChoice");
          console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));
          speechOutput = "To Answer, Say " + opt + " or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
          repromptSpeech = speechOutput;
          this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
        } else {
          if (!this.getSessionAttribute("organizationId") ||
            this.getSessionAttribute("organizationId") === null ||
            this.getSessionAttribute("organizationId") === undefined) {
            if (this.getSessionAttribute("MultiplayerQuizIntent") === "Yes") {
              if (!fourthPlayerName && this.getSessionAttribute("numberOfPlayers")) {
                msg = help.GetContexBasedHelp(
                  this.getSessionAttribute("state"),
                  this.getSessionAttribute("UserState"),
                  this
                );
              } else if (this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
                this.ask("<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s> or, say <s>list topics  </s>");
              } else {
                this.setSessionAttribute("IntentState", "FourthPlayerNameIntent");
                this.setSessionAttribute("fourthPlayerName", this.$inputs.fourth_player.value);
                this.setSessionAttribute("state", constants.states.ONBOARDING);
                this.setSessionAttribute("UserState", constants.UserState.FourthPlayerNameGiven);
                msgObject = await functionCalls.toCheckPlayersName(this, this.getSessionAttribute("numberOfPlayers"),
                  this.getSessionAttribute("firstPlayerName"), this.getSessionAttribute("secondPlayerName"),
                  this.getSessionAttribute("thirdPlayerName"), this.getSessionAttribute("fourthPlayerName"));
                msg = msgObject.msg;
                if (msgObject.intentState === "toCheckPlayersName") {
                  this.setSessionAttribute("IntentState", "toCheckPlayersName");
                  this.setSessionAttribute("state", constants.states.MAIN);
                  this.setSessionAttribute("UserState", constants.UserState.UserSelected);
                }
                this.ask(msg);
              }
            } else {
              this.ask("To enable multiplayer quiz mode, please say multiuser or multiplayer. You can also say list topics " +
                "to list the subscribed topics or to open the quiz say open and the topic name, example open vocabulary.");
            }
          }
          // }
          else {
            this.ask("Multiplayer quiz is not applicable for organisation related persons, it's only for general users. " +
              " Say list topics to list all the subscribed topics or to open a quiz say open and the topic name, " +
              " for example open science");
          }
        }
      }
    },

    //To set the session variables for first player and for passing the question to the next player
    firstPlayerIntent: async function () {
      this.setSessionAttribute("firstPlayer", "Yes");
      console.log(" current fp name -> " + this.getSessionAttribute("currentPlayerName"));
      var speechOutput = "", repromptSpeech = "", msg;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        this.setSessionAttribute("IntentState", "firstPlayerIntent");
        this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
        await this.toIntent("ContinueGetNextQuestion");
        // }
      }
    },

    //To set the session variables for second player and for passing the question to the next player
    secondPlayerIntent: async function () {
      console.log(" current sp name -> " + this.getSessionAttribute("currentPlayerName"));
      var speechOutput = "", repromptSpeech = "", msg;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        this.setSessionAttribute("IntentState", "secondPlayerIntent");
        this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("secondPlayerName"));
        await this.toIntent("ContinueGetNextQuestion");
        // }
      }
    },

    //To set the session variables for third player and for passing the question to the next player
    thirdPlayerIntent: async function () {
      console.log(" current tp name -> " + this.getSessionAttribute("currentPlayerName"));
      var speechOutput = "", repromptSpeech = "", msg;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        this.setSessionAttribute("IntentState", "thirdPlayerIntent");
        this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("thirdPlayerName"));
        await this.toIntent("ContinueGetNextQuestion");
        // }
      }
    },

    //To set the session variables for fourth player and for passing the question to the next player
    fourthPlayerIntent: async function () {
      console.log(" current 4p name -> " + this.getSessionAttribute("currentPlayerName"));
      var speechOutput = "", repromptSpeech = "", msg;
      if (!this.getSessionAttribute("UserId")) {
        speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
        repromptSpeech = speechOutput;
        this.ask(
          speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
        );
      } else {
        this.setSessionAttribute("IntentState", "fourthPlayerIntent");
        this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("fourthPlayerName"));
        await this.toIntent("ContinueGetNextQuestion");
        // }
      }
    },
  }
);

module.exports.app = app;
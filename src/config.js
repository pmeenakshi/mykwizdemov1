// ------------------------------------------------------------------
// APP CONFIGURATION
// ------------------------------------------------------------------

module.exports = {
    logging: true,
 
    intentMap: {
       'AMAZON.StopIntent': 'END',
       'AMAZON.CancelIntent': 'END',
       'AMAZON.PauseIntent': 'waitIntent'
    },
 
    db: {
         FileDb: {
             pathToFile: '../db/db.json',
         }
     },

     intentsToSkipUnhandled: [
        'END'
    ],
     
 };
 
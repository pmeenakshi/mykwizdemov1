var constants = require('../constants/constants');
const app = require('jovo-framework').Jovo;
module.exports = {
    GetHowToUse: function (state, currentConext, app) {
        var HowToUse = `<prosody rate="` + constants.speachSpeed + `">`;
        if (!app.getSessionAttribute("UserId")) {
            HowToUse += ` Please tell me the pin number by saying, my pin is and your 6 digit pin `;
        } else {
            if (state == constants.states.ONBOARDING) {
                HowToUse += ` You have to create an account on <s> QuizMe dot Guru first </s> using  browser on your mobile or computer. Once registered, You will have to subscribe to the quizes. And then, Register this device using the Pin on QuizMe dot Guru. say, my pin is and your pin`;
            }
            if ((state == constants.states.MAIN || state == constants.states.ONBOARDING)) {
                // HowToUse += ` <s>Select topic name</s> to open a Quiz, Say Open and topic Name, for example, <s>Open Vocabulary</s>`;
                // HowToUse += ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, available Challenge levels are Starter, Intermediate, Advanced.`;
                if (currentConext === constants.UserState.TopicNameSelected && app.getSessionAttribute('Topic'))
                    HowToUse += ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, available Challenge levels are ` +  app.getSessionAttribute("availableChallengeLevels");
                else
                    HowToUse += ` <s>Select topic name</s> to open a Quiz, Say Open and topic Name, for example, <s>Open Vocabulary</s>`;
            }
            if (state == constants.states.QUESTIONANSWER || state == constants.states.ONBOARDING) {
                if (app.getSessionAttribute("optionsChoice") && app.getSessionAttribute("optionsChoice") !== undefined) {
                    console.log(' options choice -> ' + app.getSessionAttribute("optionsChoice"));  //options choices
                    var opt = app.getSessionAttribute("optionsChoice");
                    // HowToUse += ` Afer starting the test, To Answer, ` + opt + `, or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                    HowToUse += ` To Answer, say ` + opt + `, or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                } else {
                    HowToUse += ` Afer starting the test, To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                }
            }
        }

        HowToUse += `</prosody>`
        return HowToUse;
    },

    GetContexBasedHelp: function (state, currentConext, app) {
        var txt = `<prosody rate="` + constants.speachSpeed + `">`;
        if (state == constants.states.ONBOARDING) {
            if (currentConext == constants.UserState.NewSession) {
                txt += ` You have to create an account on <s> QuizMe dot Guru first </s> using  browser on your mobile or computer. Once registered, You will have to subscribe to the quizes. And then, Register this device using the Pin on QuizMe dot Guru. say, my pin is and your pin`;
            }
            if (currentConext == constants.UserState.QuizType) {
                txt += ` Please provide the number of players wants to play, by saying 2, 3, 4 people or players or users, for example 2 users or 3 users like this. `;
            }
            if ((currentConext == constants.UserState.NoOfPlayersSelected) ||
                (currentConext == constants.UserState.FirstPlayerNameGiven) ||
                (currentConext == constants.UserState.SecondPlayerNameGiven) ||
                (currentConext == constants.UserState.ThirdPlayerNameGiven) ||
                currentConext == constants.UserState.FourthPlayerNameGiven) {
                txt += ` Please provide the players name by saying, first / second / third / fourth player name is and the player name. For example first player name is shaanu`;
            }
        }
        if (state == constants.states.MAIN) {
            if (currentConext == constants.UserState.UserSelected) {
                txt += ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary.</s> You can also say list topics to list all the subscribed topics`;
            }
            if (currentConext == constants.UserState.TopicNameSelected) {
                txt += ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, Options are <s> ` + app.getSessionAttribute("availableChallengeLevels") + ` </s>`;
            }
        }
        if (state == constants.states.QUESTIONANSWER) {
            if (currentConext == constants.UserState.QuestionsRead) {
                //options choices
                if (app.getSessionAttribute("optionsChoice") && app.getSessionAttribute("optionsChoice") !== undefined) {
                    console.log(' options choice -> ' + app.getSessionAttribute("optionsChoice"));
                    var opt = app.getSessionAttribute("optionsChoice");
                    txt += ` To Answer, Say ` + opt + `, or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                } else {
                    txt += ` To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                }
            }
        }
        txt += `</prosody>`
        return txt;
    },

    ProcessUnHandledRequest: function (userState, app) {

        if (userState == constants.UserState.NewSession) {
            return this.GetContexBasedHelp(constants.states.ONBOARDING, userState, app)
        }
        if (userState == constants.UserState.UserSelected || userState == constants.UserState.TopicNameSelected) {
            return this.GetContexBasedHelp(constants.states.MAIN, userState, app)
        }
        if (userState == constants.UserState.ChallangeLevelSelected || userState == constants.UserState.QuestionsRead) {
            return this.GetContexBasedHelp(constants.states.QUESTIONANSWER, userState, app)
        }
        // if (userState == constants.UserState.QuizType) {
        //     return this.GetContexBasedHelp(constants.states.ONBOARDING, userState, app)
        // }
        if ((userState == constants.UserState.QuizType) ||
            (userState == constants.UserState.NoOfPlayersSelected) ||
            (userState == constants.UserState.FirstPlayerNameGiven) ||
            (userState == constants.UserState.SecondPlayerNameGiven) ||
            (userState == constants.UserState.ThirdPlayerNameGiven) ||
            userState == constants.UserState.FourthPlayerNameGiven) {
            return this.GetContexBasedHelp(constants.states.ONBOARDING, userState, app)
        }
    },

    isValidContext: function (IntentState, state, app) {
        var msg = '';

        //For ChallengeLevel invalid contect
        if (state == constants.states.MAIN) {
            if (IntentState === "ContinueGetNextQuestion" && app.getSessionAttribute("UserState") === "QuestionsRead") {
                console.log(' options choice -> ' + app.getSessionAttribute("optionsChoice"));  //options choices
                var opt = app.getSessionAttribute("optionsChoice");
                msg = `To Answer, Say ` + opt + `, You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s> or to start new quiz you can say, start new quiz`;
            }

            if (IntentState === 'ChallengeLevelIntent' && !app.getSessionAttribute('Topic')) {
                msg = ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s>`;
                console.log("isValidContext 1 msg -> " + msg);
            }
            // else{
            // if (IntentState === 'ChallengeLevelIntent' && state === constants.states.QUESTIONANSWER) {
            //     msg = `To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s> or to start new quiz you can say, start new quiz`;
            // }  
            // }
            console.log(IntentState + '=== AnswerIntent && ' + !app.getSessionAttribute('Topic'));
            if (IntentState === 'AnswerIntent' && app.getSessionAttribute('Topic')) {
                msg = ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s>`;
            }
            else if ((IntentState === 'AnswerIntent' || IntentState === 'RepeatQuestion' || IntentState === 'RepeatOptions' || IntentState === 'ExplainIntent' || IntentState === 'CallNextQuestion') && !app.getSessionAttribute('questions')) {
                // if(app.getSessionAttribute('oneTimeTest') === "Yes"){
                //     msg = ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s>`;
                // } else{
                    msg = ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, Options are <s> ` + app.getSessionAttribute("availableChallengeLevels") + ` </s>`;
                    console.log("isValidContext 2 msg -> " + msg);
                // }
            }
            else if (IntentState === 'TopicIntent' && app.getSessionAttribute('UserState') === constants.UserState.TopicNameSelected) {
                msg = ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, Options are <s> ` + app.getSessionAttribute("availableChallengeLevels") + ` </s>`;
                console.log("isValidContext 3 msg -> " + msg);
            }

        }


        if ((IntentState === 'TopicIntent' || IntentState === 'ChallengeLevelIntent' || IntentState === "ContinueGetNextQuestion" ||
            IntentState === 'populateQuestions' || IntentState === 'oneTimeTestExplaination' || IntentState === 'AvailableTopics' ||
            IntentState === 'MoreTopics') && state == constants.states.QUESTIONANSWER) {
            if (app.getSessionAttribute('UserState') == constants.UserState.QuestionsRead) {
                console.log(' options choice -> ' + app.getSessionAttribute("optionsChoice"));  //options choices
                var opt = app.getSessionAttribute("optionsChoice");
                msg += ` To Answer, Say ` + opt + `, You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                console.log("isValidContext 4 msg -> " + msg);
            }
        }

        // if (state == constants.states.QUESTIONANSWER) {
        //     if (IntentState === 'TopicIntent' && (app.getSessionAttribute('Topic') && app.getSessionAttribute('questions'))) {
        //         msg = ` To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
        //         console.log("isValidContext 3 msg -> " + msg);
        //     }
        // }
        return msg;
    },

};
module.exports = {
    //similar: function (first, second) {
    similar: function (a, b) {
        var lengthA = a.length;
        var lengthB = b.length;
        var equivalency = 0;
        var minLength = (a.length > b.length) ? b.length : a.length;
        var maxLength = (a.length < b.length) ? b.length : a.length;
        for (var i = 0; i < minLength; i++) {
            if (a[i] == b[i]) {
                equivalency++;
            }
        }
        var weight = equivalency / maxLength;
        return (weight * 100);
    }
}
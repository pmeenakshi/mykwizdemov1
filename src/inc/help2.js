var constants = require('../constants/constants');
const app = require('jovo-framework').Jovo;
module.exports = {
    GetHowToUse: function (state, currentConext) {
        var HowToUse = `<prosody rate="` + constants.speachSpeed + `">`;
        if (state == constants.states.ONBOARDING) {
            HowToUse += ` You have to create an account on <s> QuizMe dot Guru first </s> using  browser on your mobile or computer. Once registered, You will have to subscribe to the quizes. And then, Register this device using the Pin on QuizMe dot Guru. say, my pin is and your pin`;
        }
        if ((state == constants.states.MAIN || state == constants.states.ONBOARDING)) {
            HowToUse += ` <s>Select topic name</s> to open a Quiz, Say Open and topic Name, for example, <s>Open Vocabulary</s>`;
            HowToUse += ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, available Challenge levels are Starter, Intermediate, Advanced.`;
        }
        if (state == constants.states.QUESTIONANSWER || state == constants.states.ONBOARDING) {
            HowToUse += ` Afer starting the test, To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
        }
        HowToUse += `</prosody>`
        return HowToUse;
    },

    GetContexBasedHelp: function (state, currentConext) {
        var txt = `<prosody rate="` + constants.speachSpeed + `">`;
        if (state == constants.states.ONBOARDING) {
            if (currentConext == constants.UserState.NewSession) {
                txt += ` You have to create an account on <s> QuizMe dot Guru first </s> using  browser on your mobile or computer. Once registered, You will have to subscribe to the quizes. And then, Register this device using the Pin on QuizMe dot Guru. say, my pin is and your pin`;
            }
        }
        if (state == constants.states.MAIN) {
            if (currentConext == constants.UserState.UserSelected) {
                txt += ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s>`;
            }
            if (currentConext == constants.UserState.TopicNameSelected) {
                txt += ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challnge level </s>, Options are <s>Starter, Intermediate, Advanced</s>`;
            }
        }
        if (state == constants.states.QUESTIONANSWER) {
            if (currentConext == constants.UserState.QuestionsRead) {
                txt += ` To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
            }
        }
        txt += `</prosody>`
        return txt;
    },

    ProcessUnHandledRequest: function (userState) {

        if (userState == constants.UserState.NewSession) {
            return this.GetContexBasedHelp(constants.states.ONBOARDING, userState)
        }
        if (userState == constants.UserState.UserSelected || userState == constants.UserState.TopicNameSelected) {
            return this.GetContexBasedHelp(constants.states.MAIN, userState)
        }
        if (userState == constants.UserState.ChallangeLevelSelected || userState == constants.UserState.QuestionsRead) {
            return this.GetContexBasedHelp(constants.states.QUESTIONANSWER, userState)
        }
    },

    isValidContext: function (IntentState, state, app) {
        var msg = '';

        //For ChallengeLevel invalid contect
        if (state == constants.states.MAIN) {
            if (IntentState === "ContinueGetNextQuestion" && app.getSessionAttribute("UserState") === "QuestionsRead") {
                console.log(' options choice -> ' + app.getSessionAttribute("optionsChoice"));  //options choices
                var opt = app.getSessionAttribute("optionsChoice");
                msg = `To Answer, Say ` + opt + `, You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s> or to start new quiz you can say, start new quiz`;
            }

            if (IntentState === 'ChallengeLevelIntent' && !app.getSessionAttribute('Topic')) {
                msg = ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s>`;
                console.log("isValidContext 1 msg -> " + msg);
            }
            // else{
            // if (IntentState === 'ChallengeLevelIntent' && state === constants.states.QUESTIONANSWER) {
            //     msg = `To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s> or to start new quiz you can say, start new quiz`;
            // }  
            // }
            console.log(IntentState + '=== AnswerIntent && ' + !app.getSessionAttribute('Topic'));
            if (IntentState === 'AnswerIntent' && app.getSessionAttribute('Topic'))
                msg = ` <s>Select topic name</s> to open a Quiz, Say <s>Open and topic Name</s>, for example, <s>Open Vocabulary</s>`;

            else if ((IntentState === 'AnswerIntent' || IntentState === 'RepeatQuestion' || IntentState === 'RepeatOptions' || IntentState === 'ExplainIntent' || IntentState === 'CallNextQuestion') && !app.getSessionAttribute('questions')) {
                msg = ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, Options are <s>Starter, Intermediate, Advanced</s>`;
                console.log("isValidContext 2 msg -> " + msg);
            }
            else if (IntentState === 'TopicIntent' && app.getSessionAttribute('UserState') === constants.UserState.TopicNameSelected) {
                msg = ` After you have selected the Topic Select a Challenge level by saying <s> Select and the Challenge level </s>, Options are <s>Starter, Intermediate, Advanced</s>`;
                console.log("isValidContext 3 msg -> " + msg);
            }
        }
        if ((IntentState === 'TopicIntent' || IntentState === 'ChallengeLevelIntent' || IntentState === "ContinueGetNextQuestion" ||
            IntentState === 'populateQuestions' || IntentState === 'oneTimeTestExplaination' || IntentState === 'AvailableTopics' ||
            IntentState === 'MoreTopics') && state == constants.states.QUESTIONANSWER) {
            if (app.getSessionAttribute('UserState') == constants.UserState.QuestionsRead) {
                console.log(' options choice -> ' + app.getSessionAttribute("optionsChoice"));  //options choices
                var opt = app.getSessionAttribute("optionsChoice");
                msg += ` To Answer, Say ` + opt + `, You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
                console.log("isValidContext 4 msg -> " + msg);
            }
        }

        // if (state == constants.states.QUESTIONANSWER) {
        //     if (IntentState === 'TopicIntent' && (app.getSessionAttribute('Topic') && app.getSessionAttribute('questions'))) {
        //         msg = ` To Answer, Say <s> A </s><s> B</s><s> C</s><s> D</s><s> E </s> You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
        //         console.log("isValidContext 3 msg -> " + msg);
        //     }
        // }
        return msg;
    }
};
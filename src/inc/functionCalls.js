var Database = require("../db/dbwrapper");
var constants = require("../constants/constants");
const app = require('jovo-framework').Jovo;
module.exports = {

    callSessionFunc: async function (app, UserId, TopicId, questionsTaken, score, correctAnswerScore,
        correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId, correctAnsweredWeightage,
        wrongAnsweredWeightage, notAttemptedQuestionWeightage, challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
        challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
        challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
        challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
        challengeStarterCountQuestions, challengeStarterCountCorrectAns, challengeStarterCountWrongAns,
        challengeStarterCountNotAttemptedAns, TestIndex, courseId,
        organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
        thirdPlayerSet, fourthPlayerSet, quizType) {
        var sessionId = "this.getsessionId()";
        var date = new Date();
        if (UserId !== undefined && TopicId !== undefined) {
            var queryToFetchTestId = "SELECT testId FROM EduVoiceDB.testResult where " +
                "userId = " + UserId +
                " and topicId = " + TopicId +
                " ORDER BY `testId` DESC LIMIT 1";
            var queryToFetchTestIdResult = await this.callAPI(queryToFetchTestId);
            var testId = queryToFetchTestIdResult[0].testId;
        }
        console.log(' testId result ' + testId + '  => ' + TopicId);
        if (testId === undefined || !testId) {
            console.log("starter -> " + app.getSessionAttribute("challengeStarterCountQuestions"));
            console.log("intermediate -> " + app.getSessionAttribute("challengeIntermediateCountQuestions"));
            console.log("advanced -> " + app.getSessionAttribute("challengeAdvanceCountQuestions"));
            var queryStringResult =
                'insert into EduVoiceDB.testResult (userId, topicId, questionsTaken, correctAnswer, totalScore, ' +
                'challengeHighCountQuestions, challengeHighCountCorrectAns,challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns, challengeIntermediateCountQuestions, ' +
                'challengeIntermediateCountCorrectAns, challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns, challengeStarterCountQuestions, ' +
                'challengeStarterCountCorrectAns, challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, correctQuestionId, wrongQuestionId, notAttemptedQuestionId, ' +
                'correctQuestion_Weightage, wrongQuestion_Weightage, notAttemptedQuestion_Weightage, createdDate, sessionId, attemptedQuestions, ' +
                'courseId, organizationId, section, testTypeCode, roleName, firstPlayerSet, secondPlayerSet, thirdPlayerSet, fourthPlayerSet, quizType) values ("' +
                UserId +
                '",' +
                TopicId +
                "," +
                questionsTaken +
                "," +
                score +
                correctAnswerScore +
                "," +
                challengeAdvanceCountQuestions +
                "," +
                challengeAdvanceCountCorrectAns +
                "," +
                challengeAdvanceCountWrongAns +
                "," +
                challengeAdvanceCountNotAttemptedAns +
                "," +
                challengeIntermediateCountQuestions +
                "," +
                challengeIntermediateCountCorrectAns +
                "," +
                challengeIntermediateCountWrongAns +
                "," +
                challengeIntermediateCountNotAttemptedAns +
                "," +
                challengeStarterCountQuestions +
                "," +
                challengeStarterCountCorrectAns +
                "," +
                challengeStarterCountWrongAns +
                "," +
                challengeStarterCountNotAttemptedAns +
                ',"' +
                // ',2,3,4,5,4,2,"' +
                correctAnsweredQuestionId +
                '","' +
                wrongAnsweredQuestionId +
                '","' +
                notAttemptedQuestionId +
                '","' +
                correctAnsweredWeightage +
                '","' +
                wrongAnsweredWeightage +
                '","' +
                notAttemptedQuestionWeightage +
                '","' +
                date.getFullYear() +
                "/" +
                (date.getMonth() + 1) +
                "/" +
                date.getDate() +
                " " +
                date.getHours() +
                ":" +
                date.getMinutes() +
                ":" +
                date.getSeconds() +
                '","' +
                sessionId +
                '", ' +
                TestIndex +
                "," +
                courseId +
                "," +
                organizationId +
                ', "' +
                section +
                '", "' +
                testType +
                '", "' +
                roleName +
                '", "' +
                firstPlayerSet +
                '", "' +
                secondPlayerSet +
                '", "' +
                thirdPlayerSet +
                '", "' +
                fourthPlayerSet +
                '", "' +
                quizType +
                '")';
            var resultedRows = await this.callAPI(queryStringResult);
            console.log('resultedRows -> ' + resultedRows);  //meenu
        } else {
            var queryStringResult = "UPDATE `testResult` SET `correctAnswer` = '" +
                score +
                "', `questionsTaken` = '" +
                questionsTaken +
                "', `totalScore` = '" +
                correctAnswerScore +
                "', `challengeHighCountQuestions` = '" + challengeAdvanceCountQuestions +
                "', `challengeHighCountCorrectAns` = '" + challengeAdvanceCountCorrectAns +
                "', `challengeHighCountWrongAns` = '" + challengeAdvanceCountWrongAns +
                "', `challengeHighCountNotAttemptedAns` = '" + challengeAdvanceCountNotAttemptedAns +
                "', `challengeIntermediateCountQuestions` = '" + challengeIntermediateCountQuestions +
                "', `challengeIntermediateCountCorrectAns` = '" + challengeIntermediateCountCorrectAns +
                "', `challengeIntermediateCountWrongAns` = '" + challengeIntermediateCountWrongAns +
                "', `challengeIntermediateCountNotAttemptedAns` = '" + challengeIntermediateCountNotAttemptedAns +
                "', `challengeStarterCountQuestions` = '" + challengeStarterCountQuestions +
                "', `challengeStarterCountCorrectAns` = '" + challengeStarterCountCorrectAns +
                "', `challengeStarterCountWrongAns` = '" + challengeStarterCountWrongAns +
                "', `challengeStarterCountNotAttemptedAns` = '" + challengeStarterCountNotAttemptedAns +
                "', `correctQuestionId` = '" +
                correctAnsweredQuestionId +
                "', `wrongQuestionId` = '" +
                wrongAnsweredQuestionId +
                "', `notAttemptedQuestionId` = '" +
                notAttemptedQuestionId +
                "', `correctQuestion_Weightage` = '" +
                correctAnsweredWeightage +
                "', `wrongQuestion_Weightage` = '" +
                wrongAnsweredWeightage +
                "', `notAttemptedQuestion_Weightage` = '" +
                notAttemptedQuestionWeightage +
                "', `sessionId` = '" + sessionId +
                "', `attemptedQuestions` = '" +
                TestIndex +
                "', `courseId` = '" +
                courseId +
                "', `organizationId` = '" +
                organizationId +
                "', `section` = '" +
                section +
                "', `testTypeCode` = '" +
                testType +
                "', `roleName` = '" +
                roleName +
                "', `firstPlayerSet` = '" +
                firstPlayerSet +
                "', `secondPlayerSet` = '" +
                secondPlayerSet +
                "', `thirdPlayerSet` = '" +
                thirdPlayerSet +
                "', `fourthPlayerSet` = '" +
                fourthPlayerSet +
                "', `quizType` = '" +
                quizType +
                "', `createdDate` = '" +
                date.getFullYear() +
                "/" +
                (date.getMonth() + 1) +
                "/" +
                date.getDate() +
                " " +
                date.getHours() +
                ":" +
                date.getMinutes() +
                ":" +
                date.getSeconds() +

                "' WHERE (`userId` = " +
                UserId +
                " and `topicId` = " + TopicId +
                " and `testId` = " + testId + ")";
            var resultedRows = await this.callAPI(queryStringResult);
            console.log('resultedRows -> ' + resultedRows);
        }
    },

    toCheckPlayersName: async function (app, numberOfPlayers, firstPlayerName, secondPlayerName,
        thirdPlayerName, fourthPlayerName) {
        var numberOfPlayers = parseInt(numberOfPlayers);
        var msg, intentState = "";
        console.log("number of players "+numberOfPlayers);
        if (numberOfPlayers === 2) {
            if (!firstPlayerName ||
                firstPlayerName === null ||
                firstPlayerName === undefined) {
                msg = "Please tell me the first player name by saying, first player name is and the player name.";
            } else if (!secondPlayerName ||
                secondPlayerName === null ||
                secondPlayerName === undefined) {
                msg = "Please tell me the second player name by saying, second player name is and the player name."
            } else {
                msg = "Okay! just to confirm the users name - First player name is " +
                    firstPlayerName +
                    " and the second player name is " + secondPlayerName +
                    ". To start the quiz please say list topics, to list all the subscribed topics";
                intentState = "toCheckPlayersName";
            }
        } else if (numberOfPlayers === 3) {
            if (!firstPlayerName ||
                firstPlayerName === null ||
                firstPlayerName === undefined) {
                msg = "Please tell me the first player name by saying, first player name is and the player name.";
            } else if (!secondPlayerName ||
                secondPlayerName === null ||
                secondPlayerName === undefined) {
                msg = "Please tell me the second player name by saying, second player name is and the player name.";
            } else if (!thirdPlayerName ||
                thirdPlayerName === null ||
                thirdPlayerName === undefined) {
                msg = "Please tell me the third player name by saying, third player name is and the player name.";
            } else {
                msg = "Okay! just to confirm the users name - First player name is " +
                    firstPlayerName +
                    " , the second player name is " + secondPlayerName +
                    " and the third player name is " + thirdPlayerName +
                    ". To start the quiz please say list topics, to list all the subscribed topics";
                    intentState = "toCheckPlayersName";
            }
        } else if (numberOfPlayers === 4) {
            if (!firstPlayerName ||
                firstPlayerName === null ||
                firstPlayerName === undefined) {
                msg = "Please tell me the first player name by saying, first player name is and the player name.";
            } else if (!secondPlayerName ||
                secondPlayerName === null ||
                secondPlayerName === undefined) {
                msg = "Please tell me the second player name by saying, second player name is and the player name.";
            } else if (!thirdPlayerName ||
                thirdPlayerName === null ||
                thirdPlayerName === undefined) {
                msg = "Please tell me the third player name by saying, third player name is and the player name.";
            } else if (!fourthPlayerName ||
                fourthPlayerName === null ||
                fourthPlayerName === undefined) {
                msg = "Please tell me the fourth player name by saying, fourth player name is and the player name.";
            } else {
                msg = "Okay! just to confirm the users name - First player name is " +
                    firstPlayerName +
                    " , the second player name is " + secondPlayerName +
                    " , the third player name is " + thirdPlayerName +
                    " and the fourth player name is " + fourthPlayerName +
                    ". To start the quiz please say list topics, to list all the subscribed topics";
                    intentState = "toCheckPlayersName";
            }
        } else {
            msg = " Please provide the number of players first, by saying 2 people or 3 people like this. ";
        }
        return {
            msg: msg,
            intentState: intentState
        };
    },

    callAPI: async function (queryString) {
        var rows;
        return new Promise((resolve, reject) => {
            // request(options, (error, response, body) => {
            //   if (error) {
            //     return reject(error);
            //   }
            //   resolve(body.starWarsQuote);
            // });
            Database.execute(database =>
                database.query(queryString).then(r => {
                    rows = r;
                })
            )
                .then(() => {
                    resolve(rows);
                })
                .catch(err => {
                    console.log("DB Error: ", err);
                    return reject(constants.errorPrompt);
                });
        });
    }
}
module.exports = {
  shuffleArray: function(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;

    }
    return array;
  },


  //  shuffleArray: function(array) {

  //   var arr = array;
  //   for (var i = array.length - 1; i > 0; i--) {
  //     var j = Math.floor(Math.random() * (i + 1));
  //     var max = Math.max.apply(null, arr);
 
  //   }
  //   return array;
  // },

  ArrayToString : function(array) {
    var str = '';
    var first = true;
    for (var i = 0; i < array.length; i++) {
      if (i === (array.length - 1)) {
        if (first == true) {
          str += array[i];
          first = false;
        }
        else str += ', - ' + array[i]

      } else {
        str += ', - ' + array[i];
      }
    }
    return str;
  },

  printObject : function (o) {
    var out = '';
    for (var p in o) {
      out += p + ': ' + o[p] + '\n';
    }
    console.log("from printObject : " + out);
    return out;
  },

  maxAaaryValue : function(arr) {
  var len = arr.length, max = -Infinity;
  while (len--) {
    if (arr[len] > max) {
      max = arr[len];
    }
  }
  return max;
}
};
const mysql = require('mysql');
var dbconfig = require('./dbconfig.js');

var db_access = {
    host: dbconfig.db.host,
    user: dbconfig.db.username,
    password: dbconfig.db.password,
    database: dbconfig.db.dbname,
    port: dbconfig.db.port
};
class Database {
    constructor(config) {
        this.connection = mysql.createConnection(db_access);
    }
    query(sql, args) {
        console.log("SQL query : "+sql);
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err)
                    return reject(err);
                resolve(rows);
            });
        });
    }
    close() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err)
                    return reject(err);
                resolve();
            });
        });
    }
};



module.exports = Database;

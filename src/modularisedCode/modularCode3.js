const app = require('jovo-framework').Jovo;

var request = require("request");
var Database = require("../db/dbwrapper");
var constants = require("../constants/constants");
var util = require("../inc/util");
var arrayFunctions = require("../inc/ArrayFunctions");
var help = require("../inc/help");
var functionCalls = require("../inc/functionCalls");
var RepeatText = "<s>Please Repeat!</s> ";
var skillExit = ". Thank you For Using Pop Quiz.";
var goodByeMsg = 'Good Bye, see you soon';
var topicName = [];
var questionData = [];
var topicCounter = 0;
var lastIndex = 0;
var nextQuesIndex;
var answerText = [];
var challengLevelSlotValue = "";
let title = '';
let content = '';
let imageUrl = '';
var MqQuestioncount = 0;

module.exports = {
    populateQuestions: async function () {
        this.setSessionAttribute("IntentState", "populateQuestions");
        var ansData = [];
        var randomQuestions = [];
        var data;
        console.log(
          "populateQuestions handler : Populate Questions for " +
          this.getSessionAttribute("Topic")
        );
        var topic = this.getSessionAttribute("Topic");
        var testTopic = this.getSessionAttribute("Topic");
        var qId = [], qWeightage = [], qChallengeLevel = [];
        var speechOutput = "", repromptSpeech = "", images = this;
        //If topic selected is not available return a friendly message.
    
        //Printing the event request
        //Setting null to the session attribute variables
        this.setSessionAttribute("correctAnsweredQuestionId", "");
        this.setSessionAttribute("wrongAnsweredQuestionId", "");
        this.setSessionAttribute("notAttemptedQuestionId", "");
        this.setSessionAttribute("correctAnsweredWeightage", "");
        this.setSessionAttribute("wrongAnsweredWeightage", "");
        this.setSessionAttribute("notAttemptedQuestionWeightage", "");
        //First time load set questionCount = 0
        //Get questions for the sleected Topic
        this.setSessionAttribute("TestIndex", 0);
        this.setSessionAttribute("currentQuestionIndex", 0);
        this.setSessionAttribute("correctAnswerScore", 0);
        this.setSessionAttribute("challengeAdvanceCountQuestions", 0);
        this.setSessionAttribute("challengeAdvanceCountCorrectAns", 0);
        this.setSessionAttribute("challengeAdvanceCountWrongAns", 0);
        this.setSessionAttribute("challengeAdvanceCountNotAttemptedAns", 0);
        this.setSessionAttribute("challengeIntermediateCountQuestions", 0);
        this.setSessionAttribute("challengeIntermediateCountCorrectAns", 0);
        this.setSessionAttribute("challengeIntermediateCountWrongAns", 0);
        this.setSessionAttribute("challengeIntermediateCountNotAttemptedAns", 0);
        this.setSessionAttribute("challengeStarterCountQuestions", 0);
        this.setSessionAttribute("challengeStarterCountCorrectAns", 0);
        this.setSessionAttribute("challengeStarterCountWrongAns", 0);
        this.setSessionAttribute("challengeStarterCountNotAttemptedAns", 0);
        var count = 0;
    
        //setting initial values for Multiplayer quiz 
        this.setSessionAttribute("firstPlayerTotalQue", 0)
        this.setSessionAttribute("firstPlayerCorrectAnsQue", 0)
        this.setSessionAttribute("firstPlayerWrongAnsQue", 0)
        this.setSessionAttribute("firstPlayerSkipedQue", 0)
    
        this.setSessionAttribute("secondPlayerTotalQue", 0)
        this.setSessionAttribute("secondPlayerCorrectAnsQue", 0)
        this.setSessionAttribute("secondPlayerWrongAnsQue", 0)
        this.setSessionAttribute("secondPlayerSkipedQue", 0)
    
        this.setSessionAttribute("thirdPlayerTotalQue", 0);
        this.setSessionAttribute("thirdPlayerCorrectAnsQue", 0);
        this.setSessionAttribute("thirdPlayerWrongAnsQue", 0);
        this.setSessionAttribute("thirdPlayerSkipedQue", 0);
    
        this.setSessionAttribute("fourthPlayerTotalQue", 0);
        this.setSessionAttribute("fourthPlayerCorrectAnsQue", 0);
        this.setSessionAttribute("fourthPlayerWrongAnsQue", 0);
        this.setSessionAttribute("fourthPlayerSkipedQue", 0);
    
        this.setSessionAttribute("firstPlayerSet",
          this.getSessionAttribute("firstPlayerName") +
          ", " + this.getSessionAttribute("firstPlayerTotalQue") +
          ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
          ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
          ", " + this.getSessionAttribute("firstPlayerSkipedQue")
        );
    
        this.setSessionAttribute("secondPlayerSet",
          this.getSessionAttribute("secondPlayerName") +
          ", " + this.getSessionAttribute("secondPlayerTotalQue") +
          ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
          ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
          ", " + this.getSessionAttribute("secondPlayerSkipedQue")
        );
    
        this.setSessionAttribute("thirdPlayerSet",
          this.getSessionAttribute("thirdPlayerName") +
          ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
          ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
          ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
          ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
        );
    
        this.setSessionAttribute("fourthPlayerSet",
          this.getSessionAttribute("fourthPlayerName") +
          ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
          ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
          ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
          ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
        );
    
        // var queryString =
        //   'call SP_accessQuestion_alexa("' +
        //   this.getSessionAttribute("TopicId") +
        //   '", "' +
        //   this.getSessionAttribute("challengeLevelInd") +
        //   '","0","' +
        //   constants.questionsToRead +
        //   '")';
    
        var numberOfQuestionsToAccess = constants.questionsToRead;
        this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
        if (this.getSessionAttribute("MultiplayerQuizIntent")) {
          if (this.getSessionAttribute("MultiplayerQuizIntent") &&
            this.getSessionAttribute("numberOfPlayers") === 2) {
            numberOfQuestionsToAccess = 10;
            this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
            this.setSessionAttribute("quizType", "twoPlayerQuiz");
            this.setSessionAttribute("thirdPlayerSet", "");
            this.setSessionAttribute("fourthPlayerSet", "");
          } else if (this.getSessionAttribute("MultiplayerQuizIntent") &&
            this.getSessionAttribute("numberOfPlayers") === 3) {
            numberOfQuestionsToAccess = 15;
            this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
            this.setSessionAttribute("quizType", "threePlayerQuiz");
            this.setSessionAttribute("fourthPlayerSet", "");
          } else if (this.getSessionAttribute("MultiplayerQuizIntent") &&
            this.getSessionAttribute("numberOfPlayers") === 4) {
            numberOfQuestionsToAccess = 20;
            this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
            this.setSessionAttribute("quizType", "fourPlayerQuiz");
          }
          var queryString =
            'call SP_accessQuestion_alexa("' +
            this.getSessionAttribute("TopicId") +
            '", "' +
            this.getSessionAttribute("challengeLevelInd") +
            '","' +
            numberOfQuestionsToAccess +
            '")';
        } else {
          this.setSessionAttribute("quizType", "singlePlayerQuiz");
          this.setSessionAttribute("firstPlayerSet", "");
          this.setSessionAttribute("secondPlayerSet", "");
          this.setSessionAttribute("thirdPlayerSet", "");
          this.setSessionAttribute("fourthPlayerSet", "");
          var queryString =
            'call SP_accessQuestion_alexa_Dev_2018("' +
            this.getSessionAttribute("TopicId") +
            '", "' +
            this.getSessionAttribute("challengeLevelInd") +
            '","0","' +
            numberOfQuestionsToAccess +
            '")';
        }
    
        // Database.execute(
        //     database => database.query(queryString)
        //         .then(r => {
        //             rows = r;
        //         })
        // ).then(() => {
    
        // }).catch(err => {
        //     console.log('DB Error in NewSession Request: ', err);
        //     this.tell(constants.errorPrompt);
        // });
    
        var rows = await functionCalls.callAPI(queryString);
    
        data = rows[0];
        this.setSessionAttribute("questionsTaken", data.length);
        for (var i in data) {
          qId[i] = data[i].questionId;
          qWeightage[i] = data[i].weightage;
          qChallengeLevel[i] = data[i].challenge;
          if (qChallengeLevel[i] === "S") {
            this.setSessionAttribute(
              "challengeStarterCountQuestions",
              this.getSessionAttribute("challengeStarterCountQuestions") + 1
            );
          } else if (qChallengeLevel[i] === "I") {
            this.setSessionAttribute(
              "challengeIntermediateCountQuestions",
              this.getSessionAttribute("challengeIntermediateCountQuestions") + 1
            );
          } else {
            this.setSessionAttribute(
              "challengeAdvanceCountQuestions",
              this.getSessionAttribute("challengeAdvanceCountQuestions") + 1
            );
          }
          console.log("Loading questioNs : " + data[i]);
        }
        console.log("starter -> " + this.getSessionAttribute("challengeStarterCountQuestions"));
        console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountQuestions"));
        console.log("advanced -> " + this.getSessionAttribute("challengeAdvanceCountQuestions"));
    
        var maxQuestionID = arrayFunctions.maxAaaryValue(qId);
        console.log("Max Question Id-> " + maxQuestionID);
        console.log("Qiestion Id " + qId);
        console.log("Questions weightage " + qWeightage);
        //Add o session attribute
        this.setSessionAttribute("maxQuestionID", maxQuestionID);
        questionData = arrayFunctions.shuffleArray(data);
        this.setSessionAttribute("questions", questionData);
        console.log("Loaded qsTns : " + questionData);
        if ((questionData[0].externalReference) !== null &&
          (questionData[0].externalReference) !== undefined) {
          if ((questionData[0].externalReference).includes('.png') ||
            (questionData[0].externalReference).includes('.jpg')) {
            console.log(' text -> ' + questionData[0].text);
            console.log(' text -> ' + questionData[0].questionId);
            this.setSessionAttribute('ImageContainer', 'Yes');
            console.log('image8');
          } else if ((questionData[0].externalReference).includes('.mp3')) {
            console.log('AudioContainer1');
            this.setSessionAttribute('AudioContainer', 'Yes');
          }
        }
        //Reading the first Question from the multi dimentional array.
        //1 Read the questions
        if (this.getSessionAttribute("MultiplayerQuizIntent")) {
          this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
          speechOutput =
            `Starting Quiz for <s>` +
            testTopic +
            ` </s>  Question for <s> ` +
            this.getSessionAttribute("currentPlayerName") +
            ` </s>  <prosody rate="` +
            constants.speachSpeed +
            `">` +
            questionData[0].text +
            "  <s> Options are </s>";
        } else {
          if (this.getSessionAttribute('ImageContainer') === 'Yes') {
            // let title = '';
            // let content = '';
            console.log('image2');
            console.log(" image url -> " + questionData[0].externalReference);
            imageUrl = questionData[0].externalReference;
            images = this.showImageCard(title, content, imageUrl);
            // this.showImageCard(title, content, imageUrl).ask(questionData[0].text);
            speechOutput =
              `Starting  Quiz for <s>` +
              testTopic +
              ` </s>  Question, <prosody rate="` +
              constants.speachSpeed +
              `">` +
              questionData[0].text +
              ", <s> Options are </s>";
            // this.setSessionAttribute('ImageContainer', 'No');
          } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
            console.log('AudioContainer2');
            speechOutput =
              `Starting  Quiz for <s>` +
              testTopic +
              ` </s>  Question, <prosody rate="` +
              constants.speachSpeed +
              `">` +
              questionData[0].text +
              "<audio src= '" + questionData[0].externalReference + "'/>" +
              // this.showImageCard(title, content, imageUrl)  +
              " <s> Options are </s>";
            // this.setSessionAttribute('AudioContainer', 'No');
          }
          else {
            speechOutput =
              `Starting  Quiz for <s>` +
              testTopic +
              ` </s>  Question, <prosody rate="` +
              constants.speachSpeed +
              `">` +
              questionData[0].text +
              ", <s> Options are </s>";
          }
        }
    
        //1 Read the options
        //Read the option and output them
        this.getSessionAttribute("currentState", "populateQuestions");
        var rowData = JSON.parse("[" + questionData[0].options + "]");
        var opt = " ";
        for (var key = 0; key < rowData.length; key++) {
          var dt = rowData[key];
          for (var i in dt) {
            if (dt[i] != "") {
              speechOutput =
                speechOutput +
                (`<s>` +
                  i +
                  `</s>` +
                  " - " +
                  dt[i] +
                  ',<break time="' +
                  constants.pauseTime +
                  '"/> ');
              opt = opt + i + `,  ` + ',<break time="' +
                constants.pauseTime +
                '"/> ';
              answerText[i] = dt[i];
            }
          }
        }
        speechOutput += `</prosody>`;
        this.setSessionAttribute("optionsChoice", opt);
        console.log("Questoin -> " + speechOutput);
    
        this.setSessionAttribute("score", 0);
        this.setSessionAttribute("correctAnswerText", questionData[0].answer);
        this.setSessionAttribute("questionId", questionData[0].questionId);
        this.setSessionAttribute("questionWeightage", questionData[0].weightage);
        this.setSessionAttribute("challengeLevelOfEachQue", questionData[0].challenge); //To know the challenge level of 1st question
        console.log("Qid - > " + this.getSessionAttribute("questionId"));
        console.log("Qweightage - > " + this.getSessionAttribute("questionWeightage"));
        console.log("challengeLevelOfEachQue - > " + this.getSessionAttribute("challengeLevelOfEachQue"));
        this.setSessionAttribute("UserState", constants.UserState.QuestionsRead);
        this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
        // this.ask(speechOutput);
        //meenu changes session
        console.log('Userstate -> ' + this.getSessionAttribute("ChallengeLevelContains"));
        console.log('storeResultOnlyOnce -> ' + this.getSessionAttribute("storeResultOnlyOnce"));
        if (this.getSessionAttribute('ChallengeLevelContains') !== "Yes" ||
          this.getSessionAttribute('ChallengeLevelContains') === undefined) {
          if (this.getSessionAttribute("storeResultOnlyOnce") === "Yes" ||
            this.getSessionAttribute('storeResultOnlyOnce') === undefined) {
            console.log('organisation id -> ' + this.getSessionAttribute("organizationId"));
            console.log('organisation id -> ' + this.getSessionAttribute("courseId"));
            console.log("starter -> " + this.getSessionAttribute("challengeStarterCountQuestions"));
            console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountQuestions"));
            console.log("advanced -> " + this.getSessionAttribute("challengeAdvanceCountQuestions"));
            if (!this.getSessionAttribute("organizationId") && !this.getSessionAttribute("courseId")) {
              this.setSessionAttribute("organizationId", null);
              this.setSessionAttribute("courseId", null);
            }
            var sessionId = "this.getsessionId()";
            var date = new Date();
            var queryStringResult =
              'insert into EduVoiceDB.testResult (userId, topicId, questionsTaken, correctAnswer, totalScore, ' +
              'challengeHighCountQuestions, challengeHighCountCorrectAns, challengeHighCountWrongAns,challengeHighCountNotAttemptedAns, challengeIntermediateCountQuestions, ' +
              'challengeIntermediateCountCorrectAns, challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns, challengeStarterCountQuestions, ' +
              'challengeStarterCountCorrectAns, challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, correctQuestionId, wrongQuestionId, notAttemptedQuestionId, ' +
              'correctQuestion_Weightage,wrongQuestion_Weightage, notAttemptedQuestion_Weightage, createdDate, sessionId, ' +
              'attemptedQuestions, courseId, organizationId, section, testTypeCode, roleName, firstPlayerSet, ' +
              'secondPlayerSet, thirdPlayerSet, fourthplayerSet, quizType) values ("' +
              this.getSessionAttribute("UserId") +
              '",' +
              this.getSessionAttribute("TopicId") +
              "," +
              this.getSessionAttribute("questionsTaken") +
              "," +
              this.getSessionAttribute("score") +
              "," +
              this.getSessionAttribute("correctAnswerScore") +
              "," +
              this.getSessionAttribute("challengeAdvanceCountQuestions") +
              "," +
              this.getSessionAttribute("challengeAdvanceCountCorrectAns") +
              "," +
              this.getSessionAttribute("challengeAdvanceCountWrongAns") +
              "," +
              this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns") +
              "," +
              this.getSessionAttribute("challengeIntermediateCountQuestions") +
              "," +
              this.getSessionAttribute("challengeIntermediateCountCorrectAns") +
              "," +
              this.getSessionAttribute("challengeIntermediateCountWrongAns") +
              "," +
              this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns") +
              "," +
              this.getSessionAttribute("challengeStarterCountQuestions") +
              "," +
              this.getSessionAttribute("challengeStarterCountCorrectAns") +
              "," +
              this.getSessionAttribute("challengeStarterCountWrongAns") +
              "," +
              this.getSessionAttribute("challengeStarterCountNotAttemptedAns") +
              ',"' +
              this.getSessionAttribute("correctAnsweredQuestionId") +
              '","' +
              this.getSessionAttribute("wrongAnsweredQuestionId") +
              '","' +
              this.getSessionAttribute("notAttemptedQuestionId") +
              '","' +
              this.getSessionAttribute("correctAnsweredWeightage") +
              '","' +
              this.getSessionAttribute("wrongAnsweredWeightage") +
              '","' +
              this.getSessionAttribute("notAttemptedQuestionWeightage") +
              '","' +
              date.getFullYear() +
              "/" +
              (date.getMonth() + 1) +
              "/" +
              date.getDate() +
              " " +
              date.getHours() +
              ":" +
              date.getMinutes() +
              ":" +
              date.getSeconds() +
              '","' +
              sessionId +
              '", ' +
              this.getSessionAttribute("TestIndex") +
              "," +
              this.getSessionAttribute("courseId") +
              "," +
              this.getSessionAttribute("organizationId") +
              ', "' +
              this.getSessionAttribute("section") +
              '", "' +
              this.getSessionAttribute("testType") +
              '", "' +
              this.getSessionAttribute("roleName") +
              '", "' +
              this.getSessionAttribute("firstPlayerSet") +
              '", "' +
              this.getSessionAttribute("secondPlayerSet") +
              '", "' +
              this.getSessionAttribute("thirdPlayerSet") +
              '", "' +
              this.getSessionAttribute("fourthPlayerSet") +
              '", "' +
              this.getSessionAttribute("quizType") +
              '")';
            var resultedRows = await functionCalls.callAPI(queryStringResult);
            console.log('resultedRows -> ' + resultedRows);
          }
        }//meenu
        console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
        console.log('options access -> ' + opt);
        repromptSpeech = `<prosody rate="` +
          constants.speachSpeed +
          `">` +
          `To answer use one of these options ` +
          opt +
          `</prosody>` +
          ` You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
        images.ask(
          speechOutput,
          repromptSpeech, repromptSpeech, goodByeMsg  //Meenu
        );
      },
    
}
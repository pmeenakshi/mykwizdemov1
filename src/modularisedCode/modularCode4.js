const app = require('jovo-framework').Jovo;

var request = require("request");
var Database = require("../db/dbwrapper");
var constants = require("../constants/constants");
var util = require("../inc/util");
var arrayFunctions = require("../inc/ArrayFunctions");
var help = require("../inc/help");
var functionCalls = require("../inc/functionCalls");
var RepeatText = "<s>Please Repeat!</s> ";
var skillExit = ". Thank you For Using Pop Quiz.";
var goodByeMsg = 'Good Bye, see you soon';
var topicName = [];
var questionData = [];
var topicCounter = 0;
var lastIndex = 0;
var nextQuesIndex;
var answerText = [];
var challengLevelSlotValue = "";
let title = '';
let content = '';
let imageUrl = '';
var MqQuestioncount = 0;


module.exports = {
    ChallengeLevelIntent: async function () {
        var challengeLevel = this.$inputs.challengeLevel.value;
        this.setSessionAttribute("IntentState", "ChallengeLevelIntent");
        var speechOutput = "", repromptSpeech = "";
        if (!this.getSessionAttribute("UserId")) {
          speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify" //meenu
          repromptSpeech = speechOutput;
          this.ask(
            speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //meenu
          );
        } else {
          if (this.getSessionAttribute("Topic")) {
            var msg;
            if (
              help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              )
            ) {
              msg = help.isValidContext(
                this.getSessionAttribute("IntentState"),
                this.getSessionAttribute("state"),
                this
              );
              console.log("ChallengeLevelIntent isValidContext msg -> " + msg);
              this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
            } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
              this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
              this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
              this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
              this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
              this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
              var txt = help.GetContexBasedHelp(
                this.getSessionAttribute("state"),
                this.getSessionAttribute("UserState"),
                this
              );
              console.log("Help me text -> " + txt);
              console.log("User State-> " + this.getSessionAttribute("UserState"));
              console.log("Quiz State-> " + this.getSessionAttribute("state"));
              this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
            } else {
              // var challengeLevel;
              console.log("questionAnswerHandlers:challengeLevelIntent start ");
              //    slotValue = (this.getchallengeLevel() ? this.getchallengeLevel() : "")
              var slotValue = challengeLevel;
    
              if (
                slotValue === "starter" ||
                this.getSessionAttribute("challengeLevelInd") === "S"
                // this.getSessionAttribute("challengeLevelStarter") === "A"
              ) {
                challengeLevel = "S";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
                slotValue = "starter";
              } else if (
                slotValue === "intermediate" ||
                this.getSessionAttribute("challengeLevelInd") === "I"
                // this.getSessionAttribute("challengeLevelIntermediate") === "A"
              ) {
                challengeLevel = "I";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
                slotValue = "intermediate";
              } else if (
                slotValue === "advanced" ||
                this.getSessionAttribute("challengeLevelInd") === "A"
                // this.getSessionAttribute("challengeLevelAdvanced") === "A"
              ) {
                challengeLevel = "A";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
                slotValue = "advanced";
              } else {
                challengeLevel = "S";
                slotValue = "starter";
                this.setSessionAttribute("challengeLevelInd", challengeLevel);
              }
              this.setSessionAttribute(
                "UserState",
                constants.UserState.ChallangeLevelSelected
              );
              challengLevelSlotValue = slotValue;
              // this.handler.state = constants.states.QUESTIONANSWER;
              this.setSessionAttribute('ChallengeLevelContains', 'Yes');  //Meenu changes
              this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
              await this.toIntent("populateQuestions");
            }
          } else {
            speechOutput = "Please select topic before selecting Challenge level. Say <s> Open and the Topic Name</s> or say - more topics to get other topics"; //meenu
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
            );
          }
        }
      },
    
      AnswerIntent: async function () {
        // var answer = this.$inputs.Answer.value;
        // //TODO : ****************    Ooption needs to be provided to seek th explaination aftere correct/incorrect option before moving tho the next question.
        // this.setSessionAttribute("IntentState", "AnswerIntent");
        // var msg, speechOutput = "", repromptSpeech = "";
        // console.log("Sesion TOPIC called : " + this.getSessionAttribute("TopicId"));
        console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
        if (!this.getSessionAttribute("UserId")) {
          speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";  //meenu
          repromptSpeech = speechOutput;
          this.ask(
            speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
          );
        } else {
          if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
            this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
            this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
            var txt = help.GetContexBasedHelp(
              this.getSessionAttribute("state"),
              this.getSessionAttribute("UserState"),
              this
            );
            console.log("Help me text -> " + txt);
            console.log("User State-> " + this.getSessionAttribute("UserState"));
            console.log("Quiz State-> " + this.getSessionAttribute("state"));
            this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
          } else {
            var answer = this.$inputs.Answer.value;
            //TODO : ****************    Ooption needs to be provided to seek th explaination aftere correct/incorrect option before moving tho the next question.
            this.setSessionAttribute("IntentState", "AnswerIntent");
            var msg, speechOutput = "", repromptSpeech = "";
            console.log("Sesion TOPIC called : " + this.getSessionAttribute("TopicId"));
            console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
            if (this.getSessionAttribute("TopicId")) {
              if (
                help.isValidContext(
                  this.getSessionAttribute("IntentState"),
                  this.getSessionAttribute("state"),
                  this
                )
              ) {
                msg = help.isValidContext(
                  this.getSessionAttribute("IntentState"),
                  this.getSessionAttribute("state"),
                  this
                );
                console.log("AnswerIntent isValidContext msg -> " + msg);
                this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);  //meenu
              } else {
                console.log("start handleAnswers");
                console.log("User Answer : " + answer);
                // var speechOutput = ""; //meenu
                //Start -> For testing that the option user is telling is exist in options or not
                // this.setSessionAttribute("IntentState", "AnswerIntent");
                var opt = this.getSessionAttribute("optionsChoice");
                console.log('options exist or not -> ' + opt.includes(answer));
                if (answer.toString().toLowerCase() === 'b.') {
                  console.log('Correct -> answer -> ' + answer);
                  answer = "b";
                }
                if (opt.includes(answer.toString().toUpperCase())) {
                  console.log(answer.toString().toUpperCase());
                  console.log('yes u r correct');
                  answer = answer.toString().toLowerCase();
                  //In case mo test in progress return a freindly error message. Transfer to NoTestInProgress function
                  if (!questionData) {
                    await this.toIntent("NoTestInProgress");
                  } else {
                    if (!answer || answer === undefined) {
                      console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
                      console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));  //options choices
                      var opt = this.getSessionAttribute("optionsChoice");
                      speechOutput = "answer the question by saying - the answer is " + opt;  //meenu
                      repromptSpeech = speechOutput;
                      this.ask(
                        speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
                      );
                    } else {
                      var quslen = this.getSessionAttribute("questions").length;
                      // this.attributes['firstPlayerScore'] = 0;
                      // this.attributes['secondPlayerScore'] = 0;
    
                      //	if (answer === "a" || answer === "b" || answer === "c" || answer === "d" || answer === "e") {
                      var correctAnswerFlag = false;
    
                      answer = answer.toString().toLowerCase();
    
                      //Sometime alexa returns a . in addition to the option. below is to address it
                      answer = answer.substr(0, 1);
    
                      this.setSessionAttribute(
                        "currentQuestionIndex",
                        this.getSessionAttribute("currentQuestionIndex") + 1
                      );
                      this.setSessionAttribute(
                        "TestIndex",
                        this.getSessionAttribute("TestIndex") + 1
                      );
    
                      if (
                        this.getSessionAttribute("correctAnswerText")
                          .toString()
                          .toLowerCase() === answer.toString().toLowerCase()
                      ) {
                        if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
                          speechOutput = " correct answer ";
                        }
                        else {
                          speechOutput = "";
                        }
                        correctAnswerFlag = true;
                        //storing this score value in to database so, keeping as it is. later can be removed.
                        this.setSessionAttribute(
                          "score",
                          this.getSessionAttribute("score") + 1
                        );
                        if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
                          this.setSessionAttribute(
                            "challengeStarterCountCorrectAns",
                            this.getSessionAttribute("challengeStarterCountCorrectAns") + 1
                          );
                        } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
                          this.setSessionAttribute(
                            "challengeIntermediateCountCorrectAns",
                            this.getSessionAttribute("challengeIntermediateCountCorrectAns") + 1
                          );
                        } else {
                          this.setSessionAttribute(
                            "challengeAdvanceCountCorrectAns",
                            this.getSessionAttribute("challengeAdvanceCountCorrectAns") + 1
                          );
                        }
    
                        console.log("Correct Answer ");
    
                        //Meenu changes session
                        var UserId = this.getSessionAttribute("UserId");
                        var TopicId = this.getSessionAttribute("TopicId");
                        var questionsTaken = this.getSessionAttribute("questionsTaken");
                        var score = this.getSessionAttribute("score");
                        var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
                        var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
                        var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
                        var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
                        var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
                        var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
                        var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
                        var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
                        var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
                        var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
                        var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
                        var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
                        var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
                        var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
                        var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
                        var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
                        var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
                        var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
                        var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
                        var TestIndex = this.getSessionAttribute("TestIndex");
                        var courseId = this.getSessionAttribute("courseId");
                        var organizationId = this.getSessionAttribute("organizationId");
                        var section = this.getSessionAttribute("section");
                        var testType = this.getSessionAttribute("testType");
                        var roleName = this.getSessionAttribute("roleName");
                        var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
                        var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
                        var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
                        var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
                        var quizType = this.getSessionAttribute("quizType");
                        await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
                          correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
                          correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
                          challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
                          challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
                          challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
                          challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
                          challengeStarterCountQuestions, challengeStarterCountCorrectAns,
                          challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
                          courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
                          thirdPlayerSet, fourthPlayerSet, quizType);
                        //QuestionId which are answered correct
                        this.setSessionAttribute(
                          "correctAnsweredQuestionId",
                          this.getSessionAttribute("correctAnsweredQuestionId") +
                          this.getSessionAttribute("questionId") +
                          " "
                        );
                        this.setSessionAttribute(
                          "correctAnsweredWeightage",
                          this.getSessionAttribute("correctAnsweredWeightage") +
                          this.getSessionAttribute("questionWeightage") +
                          " "
                        );  //Weightage
                        this.setSessionAttribute(
                          'correctAnswerScore',
                          this.getSessionAttribute('correctAnswerScore') +
                          this.getSessionAttribute('questionWeightage')
                        );
    
                        console.log(
                          "Correct Answered QuestionId --> " +
                          this.getSessionAttribute("correctAnsweredQuestionId")
                        );
                        console.log(
                          "Correct Answered Wightage --> " +
                          this.getSessionAttribute("correctAnsweredWeightage")
                        );
                        console.log("Correct Answered Total Score --> " +
                          this.getSessionAttribute('correctAnswerScore'));
    
                        //TODO Read questions for the Selected Values
                        console.log(
                          "Total Question length : " +
                          quslen +
                          " Test index -> " +
                          this.getSessionAttribute("TestIndex")
                        );
                        this.setSessionAttribute("speechOutput", speechOutput);
                      } else {
                        if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
                          speechOutput =
                            " - ,Incorrect answer, the correct answer is, " +
                            this.getSessionAttribute("correctAnswerText") +
                            " - <s>" +
                            answerText[this.getSessionAttribute("correctAnswerText")] +
                            "</s>,  ";
                        }
                        else {
                          speechOutput = "";
                        }
    
                        //QuestionId which are answered wrong
                        this.setSessionAttribute(
                          "wrongAnsweredQuestionId",
                          this.getSessionAttribute("wrongAnsweredQuestionId") +
                          this.getSessionAttribute("questionId") +
                          " "
                        );
                        this.setSessionAttribute(
                          "wrongAnsweredWeightage",
                          this.getSessionAttribute("wrongAnsweredWeightage") +
                          this.getSessionAttribute("questionWeightage") +
                          " "
                        );
                        if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
                          this.setSessionAttribute(
                            "challengeStarterCountWrongAns",
                            this.getSessionAttribute("challengeStarterCountWrongAns") + 1
                          );
                        } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
                          this.setSessionAttribute(
                            "challengeIntermediateCountWrongAns",
                            this.getSessionAttribute("challengeIntermediateCountWrongAns") + 1
                          );
                        } else {
                          this.setSessionAttribute(
                            "challengeAdvanceCountWrongAns",
                            this.getSessionAttribute("challengeAdvanceCountWrongAns") + 1
                          );
                        }
                        console.log(
                          "Wrong Answered QuestionId --> " +
                          this.getSessionAttribute("wrongAnsweredQuestionId")
                        );
                        console.log(
                          "Wrong Answered Wightage --> " +
                          this.getSessionAttribute("wrongAnsweredWeightage")
                        );
                        //Ask for explaination
                        this.setSessionAttribute("speechOutput", speechOutput);
    
                        //VoiceLabs.track(this.event.session, null, null, null, (error, response) => {
                        // this.ask( speechOutput, ', To Explain, try saying, - Explain the Answer or say next question - to continue quiz ');
                        //});
                      }
                      console.log(
                        "Current question index -> " +
                        this.getSessionAttribute("currentQuestionIndex")
                      );
    
    
                      // for multiplayer quizes :- 
                      //This block of code is for all players 2,3,4, only for wrong and correct answered
                      // questions. For skiped questions search for "call next question"
                      if (this.getSessionAttribute("MultiplayerQuizIntent") &&
                        this.getSessionAttribute("MultiplayerQuizIntent") !== null) {
                        MqQuestioncount = 0;
                        this.setSessionAttribute("IntentStateForMQ", "");
                        //If the user answers correct, then here we're checking the question index to check the question 
                        //belongs to which player, and based on that we're throwing next question to next player.
                        if (correctAnswerFlag) {
                          this.setSessionAttribute("AnsIntentStateForMQ", "AnswerFlag");
                          console.log("correct answer " + this.getSessionAttribute("currentQuestionIndex"));
                          if (this.getSessionAttribute("numberOfPlayers") === 2) {
                            // if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("secondPlayerName")) {
                            if ((this.getSessionAttribute("currentQuestionIndex")) % 2 === 0) {
                              // secondPlayerScore += 1;
                              console.log("correct score of player2");
                              this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                              this.setSessionAttribute("secondPlayerCorrectAnsQue",
                                this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("secondPlayerSet",
                                this.getSessionAttribute("secondPlayerName") +
                                ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                                ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                              )
                              await this.toIntent("firstPlayerIntent");
                            } else {
                              // firstPlayerScore += 1;
                              console.log("correct score of player1");
                              this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                              this.setSessionAttribute("firstPlayerCorrectAnsQue",
                                this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("firstPlayerSet",
                                this.getSessionAttribute("firstPlayerName") +
                                ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                                ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                              )
                              await this.toIntent("secondPlayerIntent");
                            }
                          } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                            console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                            if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 0) {
                              // thirdPlayerScore += 1;
                              // console.log("correct score of player3", thirdPlayerScore);
                              this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                              this.setSessionAttribute("thirdPlayerCorrectAnsQue",
                                this.getSessionAttribute("thirdPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("thirdPlayerSet",
                                this.getSessionAttribute("thirdPlayerName") +
                                ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                                ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                              );
                              await this.toIntent("firstPlayerIntent");
                            }
                            else if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 1) {
                              // firstPlayerScore += 1;
                              // console.log("correct score of player1", firstPlayerScore);
                              this.setSessionAttribute("firstPlayerTotalQue",
                                this.getSessionAttribute("firstPlayerTotalQue") + 1);
                              this.setSessionAttribute("firstPlayerCorrectAnsQue",
                                this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("firstPlayerSet",
                                this.getSessionAttribute("firstPlayerName") +
                                ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                                ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                              )
                              await this.toIntent("secondPlayerIntent");
                            } else {
                              // secondPlayerScore += 1;
                              // console.log("correct score of player2", secondPlayerScore);
                              this.setSessionAttribute("secondPlayerTotalQue",
                                this.getSessionAttribute("secondPlayerTotalQue") + 1);
                              this.setSessionAttribute("secondPlayerCorrectAnsQue",
                                this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("secondPlayerSet",
                                this.getSessionAttribute("secondPlayerName") +
                                ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                                ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                              )
                              await this.toIntent("thirdPlayerIntent");
                            }
                          } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                            console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                            if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 0) {
                              // thirdPlayerScore += 1;
                              // console.log("correct score of player1", thirdPlayerScore);
                              this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                              this.setSessionAttribute("thirdPlayerCorrectAnsQue",
                                this.getSessionAttribute("thirdPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("thirdPlayerSet",
                                this.getSessionAttribute("thirdPlayerName") +
                                ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                                ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                              );
                              await this.toIntent("firstPlayerIntent");
                            }
                            else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 1) {
                              // firstPlayerScore += 1;
                              // console.log("correct score of player2", firstPlayerScore);
                              this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                              this.setSessionAttribute("firstPlayerCorrectAnsQue",
                                this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("firstPlayerSet",
                                this.getSessionAttribute("firstPlayerName") +
                                ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                                ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                              )
                              await this.toIntent("secondPlayerIntent");
                            } else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 2) {
                              // fourthPlayerScore += 1;
                              // console.log("correct score of player3", fourthPlayerScore);
                              this.setSessionAttribute("fourthPlayerTotalQue", this.getSessionAttribute("fourthPlayerTotalQue") + 1);
                              this.setSessionAttribute("fourthPlayerCorrectAnsQue",
                                this.getSessionAttribute("fourthPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("fourthPlayerSet",
                                this.getSessionAttribute("fourthPlayerName") +
                                ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
                                ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
                              );
                              await this.toIntent("thirdPlayerIntent");
                            } else {
                              // secondPlayerScore += 1;
                              // console.log("correct score of player4", secondPlayerScore);
                              this.setSessionAttribute("secondPlayerCorrectAnsQue",
                                this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
                              this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                              this.setSessionAttribute("secondPlayerSet",
                                this.getSessionAttribute("secondPlayerName") +
                                ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                                ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                              )
                              await this.toIntent("fourthPlayerIntent");
                            }
                          }
                        } else {  ///wrong answer starting
                          //If the user answers wrong, then here we're checking the question index to check the question belongs to which player, 
                          //and based on that we're throwing next question to next player.
                          console.log("wrong answer " + this.getSessionAttribute("currentQuestionIndex"));
                          console.log("speech output -> " + this.getSessionAttribute("speechOutput"));
                          this.setSessionAttribute("AnsIntentStateForMQ", "AnswerFlag");
                          if (this.getSessionAttribute("numberOfPlayers") === 2) {
                            if (this.getSessionAttribute("currentQuestionIndex") % 2 === 0) {
                              // secondPlayerWScore += 1;
                              // secondPlayerScore = secondPlayerWScore;
                              console.log("wrong score of player2");
                              this.setSessionAttribute("secondPlayerTotalQue",
                                this.getSessionAttribute("secondPlayerTotalQue") + 1);
                              this.setSessionAttribute("secondPlayerWrongAnsQue",
                                this.getSessionAttribute("secondPlayerWrongAnsQue") + 1
                              );
                              this.setSessionAttribute("secondPlayerSet",
                                this.getSessionAttribute("secondPlayerName") +
                                ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                                ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                              )
                              await this.toIntent("firstPlayerIntent");
                            } else {
                              // firstPlayerWScore += 1;
                              // firstPlayerScore = firstPlayerWScore;
                              console.log("wrong score of player1");
                              this.setSessionAttribute("firstPlayerTotalQue",
                                this.getSessionAttribute("firstPlayerTotalQue") + 1);
                              this.setSessionAttribute("firstPlayerWrongAnsQue",
                                this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("firstPlayerSet",
                                this.getSessionAttribute("firstPlayerName") +
                                ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                                ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                              )
                              await this.toIntent("secondPlayerIntent");
                            }
                          } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                            console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                            if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 0) {
                              // thirdPlayerWScore += 1;
                              // thirdPlayerScore = thirdPlayerWScore;
                              // console.log("wrong score of player3", thirdPlayerScore);
                              this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                              this.setSessionAttribute("thirdPlayerWrongAnsQue",
                                this.getSessionAttribute("thirdPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("thirdPlayerSet",
                                this.getSessionAttribute("thirdPlayerName") +
                                ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                                ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                              );
                              await this.toIntent("firstPlayerIntent");
                            } else if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 1) {
                              // firstPlayerWScore += 1;
                              // firstPlayerScore = firstPlayerWScore;
                              // console.log("wrong score of player1", firstPlayerScore);
                              this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                              this.setSessionAttribute("firstPlayerWrongAnsQue",
                                this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("firstPlayerSet",
                                this.getSessionAttribute("firstPlayerName") +
                                ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                                ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                              )
                              await this.toIntent("secondPlayerIntent");
                            } else {
                              // secondPlayerWScore += 1;
                              // secondPlayerScore = secondPlayerWScore;
                              // console.log("wrong score of player2", secondPlayerScore);
                              this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                              this.setSessionAttribute("secondPlayerWrongAnsQue",
                                this.getSessionAttribute("secondPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("secondPlayerSet",
                                this.getSessionAttribute("secondPlayerName") +
                                ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                                ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                              )
                              await this.toIntent("thirdPlayerIntent");
                            }
                          } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                            console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
                            if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 0) {
                              // thirdPlayerWScore += 1;
                              // thirdPlayerScore = thirdPlayerWScore
                              // console.log("wrong score of player1", thirdPlayerScore);
                              this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
                              this.setSessionAttribute("thirdPlayerWrongAnsQue",
                                this.getSessionAttribute("thirdPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("thirdPlayerSet",
                                this.getSessionAttribute("thirdPlayerName") +
                                ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
                                ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
                              );
                              await this.toIntent("firstPlayerIntent");
                            }
                            else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 1) {
                              // firstPlayerWScore += 1;
                              // firstPlayerScore = firstPlayerWScore;
                              // console.log("wrong score of player2", firstPlayerScore);
                              this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
                              this.setSessionAttribute("firstPlayerWrongAnsQue",
                                this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("firstPlayerSet",
                                this.getSessionAttribute("firstPlayerName") +
                                ", " + this.getSessionAttribute("firstPlayerTotalQue") +
                                ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("firstPlayerSkipedQue")
                              )
                              await this.toIntent("secondPlayerIntent");
                            } else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 2) {
                              // fourthPlayerWScore += 1;
                              // fourthPlayerScore = fourthPlayerWScore;
                              // console.log("wrong score of player3", fourthPlayerScore);
                              this.setSessionAttribute("fourthPlayerTotalQue", this.getSessionAttribute("fourthPlayerTotalQue") + 1);
                              this.setSessionAttribute("fourthPlayerWrongAnsQue",
                                this.getSessionAttribute("fourthPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("fourthPlayerSet",
                                this.getSessionAttribute("fourthPlayerName") +
                                ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
                                ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
                              );
                              await this.toIntent("thirdPlayerIntent");
                            } else {
                              // secondPlayerWScore += 1;
                              // secondPlayerScore = secondPlayerWScore;
                              // console.log("wrong score of player4", secondPlayerScore);
                              this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
                              this.setSessionAttribute("secondPlayerWrongAnsQue",
                                this.getSessionAttribute("secondPlayerWrongAnsQue") + 1);
                              this.setSessionAttribute("secondPlayerSet",
                                this.getSessionAttribute("secondPlayerName") +
                                ", " + this.getSessionAttribute("secondPlayerTotalQue") +
                                ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
                                ", " + this.getSessionAttribute("secondPlayerSkipedQue")
                              )
                              await this.toIntent("fourthPlayerIntent");
                            }
                          }
                        }  //ending wrong answers
                      }
                      //end of multiplayer quiz
    
                      // if (this.getSessionAttribute("twoPlayersQuiz")) {
                      //   if (
                      //     this.getSessionAttribute("currentQuestionIndex") % 2 ===
                      //     0
                      //   ) {
                      //     if (correctAnswerFlag) {
                      //       firstPlayerScore += 1;
                      //     }
                      //     console.log(
                      //       "Player 2's questionIndex ->" +
                      //       this.getSessionAttribute("currentQuestionIndex")
                      //     );
                      //     console.log(
                      //       "Plater 2's score ->" +
                      //       this.getSessionAttribute("secondPlayerScore")
                      //     );
                      //     await this.toIntent("SecondPlayer");
                      //   } else {
                      //     if (correctAnswerFlag) {
                      //       secondPlayerScore += 1;
                      //     }
                      //     console.log(
                      //       "Player 1's questionIndex ->" +
                      //       this.getSessionAttribute("currentQuestionIndex")
                      //     );
                      //     console.log(
                      //       "Plater 1's score ->" +
                      //       this.getSessionAttribute("firstPlayerScore")
                      //     );
                      //     await this.toIntent("FirstPlayer");
                      //   }
                      // }
                      else {
                        if (
                          this.getSessionAttribute("currentQuestionIndex") <= quslen
                        ) {
                          await this.toIntent("getNextQuestion");
                          //		}
                          //		 else {
                          // this.attributes['speechOutput'] = " - ";
                          // app.toIntent('endOfTest');
                          //		}
                        } else {
                          this.setSessionAttribute(
                            "speechOutput",
                            this.getSessionAttribute("speechOutput") +
                            " End of this quiz "
                          );
                          console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
                          await this.toIntent("endOfTest");
                        }
                      }
                      console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
                    }
                  }
                }
                else {
                  console.log('Unfortunately, I have to say that, u r wrong');
                  speechOutput = "To answer this question, please say one of these options, " + opt +
                    " , or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
                  repromptSpeech = speechOutput;
                  this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
                }
                //End -> For testing that the option user is telling is exist in options or not
              }
            } else {
              console.log("Intents state -> " + this.getSessionAttribute("IntentState"));
              console.log(" state -> " + this.getSessionAttribute("state"));
              console.log("user state -> " + this.getSessionAttribute("UserState"));
              speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
              repromptSpeech = speechOutput;
              this.ask(
                speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //meenu
              );
            }
          }
        }
      },
    
      getNextQuestion: async function () {
        console.log("Starting getNextQuestion");
        this.setSessionAttribute("IntentState", "getNextQuestion");
        var data = [];
        var qId = [];
        var completedCount = this.getSessionAttribute("TestIndex");
        //if the compleated question count is equal to configured count, exit test
        var speechoutput = "", repromptSpeech = "";
        if (!this.getSessionAttribute("UserId")) {
          speechoutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
          repromptSpeech = speechoutput;
          this.ask(
            speechoutput, repromptSpeech, repromptSpeech, goodByeMsg
          );
        } else {
          if (this.getSessionAttribute("TopicId")) {
            if (completedCount >= this.getSessionAttribute("numberOfQuestionsToAccess")) {
              console.log("end");
              await this.toIntent("endOfTest");
            }
            // if (completedCount >= constants.TestSize) {
            //       console.log("end");
            //       await this.toIntent("endOfTest");
            //     } 
            else {
              console.log("inside getnextquestion block");
              // var rem = completedCount % constants.questionsToRead;  //for multiplayer quiz commented
              var rem = completedCount % this.getSessionAttribute("numberOfQuestionsToAccess");
              if (
                rem === 0 &&
                completedCount != 0 &&
                // completedCount <= constants.TestSize   //for multiplayer quiz commented
                completedCount <= this.getSessionAttribute("numberOfQuestionsToAccess")
              ) {
                // this.attributes['currentQuestionIndex'] = 0;
                this.setSessionAttribute("currentQuestionIndex", 0);
                console.log("db connection for nxt qus set");
                console.log("Inside if,  Reminder  : " + rem);
                // var testTopic = this.attributes['Topic'];
                var testTopic = this.getSessionAttribute("Topic");
                //reading next set of questions from DB
                //Read max id from Sesson atribute
                // var maxId = this.attributes['maxQuestionID'];
                var maxId = this.getSessionAttribute("maxQuestionID");
                if (maxId === null) {
                  maxId = 0;
                }
                // var queryString = 'call SP_accessQuestion_alexa_Dev_2018("' + this.attributes['TopicId'] + '", "' + this.attributes['challengeLevelInd'] + '","' + maxId + '","' + constants.questionsToRead + '")';
                var queryString =
                  'call SP_accessQuestion_alexa_Dev_2018("' +
                  this.getSessionAttribute("TopicId") +
                  '", "' +
                  this.getSessionAttribute("challengeLevelInd") +
                  '","' +
                  maxId +
                  '","' +
                  // constants.questionsToRead +    //for multiplayer quiz commented
                  this.getSessionAttribute("numberOfQuestionsToAccess") +
                  '")';
                console.log("Query String -> " + queryString);
                console.log(
                  "completed Count -> " +
                  completedCount +
                  " Reading next set of questions"
                );
                let rows;
                // Database.execute(
                //     database => database.query(queryString)
                //         .then(r => {
                //             rows = r;
                //         })
                // ).then(() => {
    
                // }).catch(err => {
                //     console.log('DB Error: ', err);
                //     pointerToThis.tell(constants.errorPrompt);
                // });
                rows = await functionCalls.callAPI(queryString);
                data = rows[0];
                for (var i in data) {
                  qId[i] = data[i].questionId;
                }
                var maxQuestionID = arrayFunctions.maxAaaryValue(qId);
                console.log("Maximum Question Id-> " + maxQuestionID);
                console.log("Qiestion Id " + qId);
                //Add o session attribute
                // this.attributes['maxQuestionID'] = maxQuestionID;
                this.setSessionAttribute("maxQuestionID", maxQuestionID);
    
                // this.attributes['questions'] = "";
                this.setSessionAttribute("questions", "");
    
                //nextQuesIndex = this.attributes['currentQuestionIndex'] = 0;
                // console.log('Question data erased --->> ' + this.attributes['questions'])
                console.log(
                  "Question data erased --->> " +
                  this.getSessionAttribute("questions")
                );
    
                //assign the new list of questions retrived
                questionData = arrayFunctions.shuffleArray(data);
                // this.attributes['questions'] = questionData;
                this.setSessionAttribute("questions", questionData);
    
                // if (this.attributes['questions'].length === 0) {
                if (this.getSessionAttribute("questions").length === 0) {
                  console.log("loadQuestions  did not return anything!!!");
                  await this.toIntent("endOfTest");
                } else {
                  await this.toIntent("ContinueGetNextQuestion");
                }
              } else {
                // this.emitWithState('ContinueGetNextQuestion');
                await this.toIntent("ContinueGetNextQuestion");
              }
            }
          } else {
            speechoutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
            repromptSpeech = speechoutput;
            this.ask(
              speechoutput, repromptSpeech, repromptSpeech, goodByeMsg // meenu
            );
          }
        }
      },
    
}
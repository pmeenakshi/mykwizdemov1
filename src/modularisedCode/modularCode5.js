const app = require('jovo-framework').Jovo;

var request = require("request");
var Database = require("../db/dbwrapper");
var constants = require("../constants/constants");
var util = require("../inc/util");
var arrayFunctions = require("../inc/ArrayFunctions");
var help = require("../inc/help");
var functionCalls = require("../inc/functionCalls");
var RepeatText = "<s>Please Repeat!</s> ";
var skillExit = ". Thank you For Using Pop Quiz.";
var goodByeMsg = 'Good Bye, see you soon';
var topicName = [];
var questionData = [];
var topicCounter = 0;
var lastIndex = 0;
var nextQuesIndex;
var answerText = [];
var challengLevelSlotValue = "";
let title = '';
let content = '';
let imageUrl = '';
var MqQuestioncount = 0;


module.exports = {
    ContinueGetNextQuestion: async function () {
        this.setSessionAttribute('IntentState', 'ContinueGetNextQuestion');  //Meenu
        this.setSessionAttribute('ImageContainer', 'No');
        this.setSessionAttribute('AudioContainer', 'No');
    
        nextQuesIndex = this.getSessionAttribute("currentQuestionIndex");
        questionData = this.getSessionAttribute("questions");
    
        var completedCount = this.getSessionAttribute("TestIndex");
        var speechOutput = "", repromptSpeech = "", images = this;
        //if the compleated question count is equal to configured count, exit test
        console.log("completedCount -> " + completedCount);
        if (!this.getSessionAttribute("UserId")) {
          speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
          repromptSpeech = speechOutput;
          this.ask(
            speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
          );
        } else {
          if (this.getSessionAttribute("TopicId")) {
            // if (completedCount >= constants.TestSize) {  //For multiplayer quiz commented
            if (completedCount >= this.getSessionAttribute("numberOfQuestionsToAccess")) {
              console.log("end");
              await this.toIntent("endOfTest");
            } else {
              console.log(questionData + " and index : " + nextQuesIndex);
              console.log("QsTN : " + questionData[nextQuesIndex]);
              console.log("QsTN text : " + questionData[nextQuesIndex].text);
    
              if ((questionData[nextQuesIndex].externalReference) !== null &&
                (questionData[nextQuesIndex].externalReference) !== undefined) {
                if ((questionData[nextQuesIndex].externalReference).includes('.png') ||
                  (questionData[nextQuesIndex].externalReference).includes('.jpg')) {
                  console.log(' text -> ' + questionData[nextQuesIndex].text);
                  console.log(' text -> ' + questionData[nextQuesIndex].questionId);
                  this.setSessionAttribute('ImageContainer', 'Yes');
                  console.log('image3');
                } else {
                  this.setSessionAttribute('ImageContainer', 'No');
                  console.log('image4');
                }
                if ((questionData[nextQuesIndex].externalReference).includes('.mp3')) {
                  console.log('AudioContainer3');
                  this.setSessionAttribute('AudioContainer', 'Yes');
                } else {
                  console.log('AudioContainer4');
                  this.setSessionAttribute('AudioContainer', 'No');
                }
              }
    
              if (this.getSessionAttribute('ImageContainer') === 'Yes') {
                // let title = '';
                // let content = '';
                console.log('image6');
                console.log(" image url -> " + questionData[nextQuesIndex].externalReference);
                imageUrl = questionData[nextQuesIndex].externalReference;
                speechOutput =
                  ` -  <prosody rate="` +
                  constants.speachSpeed +
                  `">` +
                  questionData[nextQuesIndex].text +
                  ". <s> Options are </s> ";
                images = this.showImageCard(title, content, imageUrl);
              } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
                console.log('AudioContainer5');
                speechOutput =
                  ` -  <prosody rate="` +
                  constants.speachSpeed +
                  `">` +
                  questionData[nextQuesIndex].text +
                  "<audio src= '" + questionData[nextQuesIndex].externalReference + "'/>" +
                  " <s> Options are </s> ";
              } else {
                speechOutput =
                  ` -  <prosody rate="` +
                  constants.speachSpeed +
                  `">` +
                  questionData[nextQuesIndex].text +
                  " <s> Options are </s> ";
              }
              //Assigning the next question Options
    
              var rowData = JSON.parse(
                "[" + questionData[nextQuesIndex].options + "]"
              );
              var opt = " ";
              for (var key = 0; key < rowData.length; key++) {
                var dt = rowData[key];
                for (var i in dt) {
                  if (dt[i] != "") {
                    speechOutput =
                      speechOutput +
                      (`<s>` +
                        i +
                        `</s>` +
                        " - " +
                        dt[i] +
                        ',<break time="' +
                        constants.pauseTime +
                        '"/> ');
                    opt = opt + i + `,  ` + ',<break time="' +
                      constants.pauseTime +
                      '"/> ';   //options choices
                    answerText[i] = dt[i];
                  }
                }
              }
              speechOutput += `</prosody>`;
              console.log('options choices -> ' + opt);
              this.setSessionAttribute("optionsChoice", opt);
              console.log("Next Question is : " + speechOutput);
    
              /////reset he current index to zer
              this.setSessionAttribute("currentQuestionIndex", nextQuesIndex);
              //populate the answer for the first row in the questions array
              this.setSessionAttribute(
                "correctAnswerText",
                questionData[nextQuesIndex].answer
              );
              this.setSessionAttribute(
                "questionId",
                questionData[nextQuesIndex].questionId
              );
              this.setSessionAttribute(
                "questionWeightage",
                questionData[nextQuesIndex].weightage
              );   //Weightage
              this.setSessionAttribute(
                "challengeLevelOfEachQue",
                questionData[nextQuesIndex].challenge
              );   //To know the challenge level of each question after 1st question
              console.log('questionWeightage -> ' + this.getSessionAttribute('questionWeightage'));
              //incrementing the testIndex by one
              this.setSessionAttribute(
                "UserState",
                constants.UserState.QuestionsRead
              );
              this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
              // VoiceLabs.track(this.event.session, 'ContinueGetNextQuestion', null, null, (error, response) => {
    
              console.log(
                "After " +
                this.getSessionAttribute("speechOutput") +
                ` Next Question ` +
                speechOutput,
                " Repeating the same question - " +
                speechOutput +
                " Say - the answer is " + opt + " or say Next question to skip this question."
              );
    
              //Meenu changes session
              var UserId = this.getSessionAttribute("UserId");
              var TopicId = this.getSessionAttribute("TopicId");
              var questionsTaken = this.getSessionAttribute("questionsTaken");
              var score = this.getSessionAttribute("score");
              var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
              var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
              var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
              var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
              var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
              var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
              var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
              var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
              var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
              var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
              var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
              var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
              var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
              var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
              var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
              var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
              var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
              var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
              var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
              var TestIndex = this.getSessionAttribute("TestIndex");
              var courseId = this.getSessionAttribute("courseId");
              var organizationId = this.getSessionAttribute("organizationId");
              var section = this.getSessionAttribute("section");
              var testType = this.getSessionAttribute("testType");
              var roleName = this.getSessionAttribute("roleName");
              var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
              var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
              var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
              var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
              var quizType = this.getSessionAttribute("quizType");
              await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
                correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
                correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
                challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
                challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
                challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
                challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
                challengeStarterCountQuestions, challengeStarterCountCorrectAns,
                challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
                courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
                thirdPlayerSet, fourthPlayerSet, quizType);
    
              console.log('speechOutput -> ' + speechOutput + ' Result => ' + this.getSessionAttribute("score"));//Meenu session end change
              repromptSpeech = " Repeating the same question - " +
                speechOutput +
                " Say - the answer is " + opt + " or say Next question to skip this question."; //meenu
              if (this.getSessionAttribute('imageExplanation') !== null &&
                this.getSessionAttribute('imageExplanation') !== undefined) {
                imageUrl = this.getSessionAttribute('imageExplanation');
                images = this.showImageCard(title, content, imageUrl);
                console.log(' image url -> ' + imageUrl);
              }
              else {
                this.setSessionAttribute('imageExplanation', null)
                images = this;
              }
    
    
              //Explaination for multiplayer quiz
              if (this.getSessionAttribute("MultiplayerQuizIntent")) {
                console.log(" current p name -> " + this.getSessionAttribute("currentPlayerName"));
                if (this.getSessionAttribute("IntentStateForMQ") === "showingCorrectAnswer") {
                  images.ask(
                    this.getSessionAttribute("speechOutput") +
                    ` Next Question for ` +
                    this.getSessionAttribute("currentPlayerName") +
                    speechOutput,
                    repromptSpeech, repromptSpeech, goodByeMsg
                  );
                } else {
                  images.ask(
                    this.getSessionAttribute("speechOutput") +
                    ` Question for ` +
                    this.getSessionAttribute("currentPlayerName") +
                    speechOutput,
                    repromptSpeech, repromptSpeech, goodByeMsg
                  );
                }
              } else {
                images.ask(
                  this.getSessionAttribute("speechOutput") +
                  ` Next Question ` +
                  speechOutput,
                  repromptSpeech, repromptSpeech, goodByeMsg
                );
              }
              // });
            }
          } else {
            speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>";
            repromptSpeech = speechOutput;
            this.ask(
              speechOutput, repromptSpeech, repromptSpeech, goodByeMsg // meenu
            );
          }
        }
      },
}
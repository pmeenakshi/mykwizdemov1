const app = require('jovo-framework').Jovo;

var request = require("request");
var Database = require("../db/dbwrapper");
var constants = require("../constants/constants");
var util = require("../inc/util");
var arrayFunctions = require("../inc/ArrayFunctions");
var help = require("../inc/help");
var functionCalls = require("../inc/functionCalls");
var RepeatText = "<s>Please Repeat!</s> ";
var skillExit = ". Thank you For Using Pop Quiz.";
var goodByeMsg = 'Good Bye, see you soon';
var topicName = [];
var questionData = [];
var topicCounter = 0;
var lastIndex = 0;
var nextQuesIndex;
var answerText = [];
var challengLevelSlotValue = "";
let title = '';
let content = '';
let imageUrl = '';
var MqQuestioncount = 0;


module.exports = {
    NewSession: async function() {
      // Check for User Data in Session Attributes
      console.log("onboardingStateHandlers : NewSession");
  
      //this.attributes['UserState'] = "NewSession";
      this.setSessionAttribute("UserState", constants.UserState.NewSession);
      // this.setSessionAttribute('IntentState', 'NewSession');
  
      console.log("DeviceId -> " + this.getIdToken());
  
      await this.toIntent("AMAZON.StartOverIntent");
    },
  
    "AMAZON.StartOverIntent": function() {
      console.log(
        "Current Intent Handler : " + this.getSessionAttribute("IntentState")
      );
      if (
        this.getSessionAttribute("IntentState") &&
        this.getSessionAttribute("IntentState") !== undefined
      )
        return this.toIntent("Unhandled");
      this.setSessionAttribute("IntentState", "AMAZON.StartOverIntent");
      this.setSessionAttribute("state", constants.states.MAIN);
      this.setSessionAttribute("UserState", constants.UserState.UserSelected);
      this.setSessionAttribute("ChallengeLevelIntent", "");
      this.setSessionAttribute("Topic", "");
  
      // var boom = "<audio src='https://s3.amazonaws.com/sounds226/boom.mp3'/>";
      // this.ask(
      //   "Hey! Start Quiz, by saying <s>Open and topic Name</s>or say list topics" +
      //     boom
      // );
      this.toIntent('IdentifyUser');
    },
  
    TopicIntent: async function () {
        console.log('intentstate -> ' + this.getSessionAttribute('IntentState'));
        console.log("Starting mainStateHandler:TopicIntent");
        // this.setSessionAttribute("IntentState", "TopicIntent");
        var repromptSpeech = "";
        var speechOutput = "";
        var topic = this.$inputs.topic_name.value;
        console.log("User requested Topic ->" + topic);
        var msg;
        console.log("Userstate -> " + this.getSessionAttribute("UserState"));
        console.log("Cuurrent State : " + this.getSessionAttribute('currentState'));
        console.log("State : " + this.getSessionAttribute('state'));
    
        if (!this.getSessionAttribute("UserId")) {
          speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";
          repromptSpeech = speechOutput;
          this.ask(
            speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //reprompt changes meenu
          );
        } else {
          if (
            help.isValidContext(
              this.getSessionAttribute("IntentState"),
              this.getSessionAttribute("state"),
              this
            )
          ) {
            msg = help.isValidContext(
              this.getSessionAttribute("IntentState"),
              this.getSessionAttribute("state"),
              this
            );
            console.log("TopicIntent isValidContext msg -> " + msg);
            this.ask(msg, msg, constants.HelpPrompt, goodByeMsg); //Meenu reprompt
          } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
            this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
            this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
            this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
            var txt = help.GetContexBasedHelp(
              this.getSessionAttribute("state"),
              this.getSessionAttribute("UserState"),
              this
            );
            console.log("Help me text -> " + txt);
            console.log("User State-> " + this.getSessionAttribute("UserState"));
            console.log("Quiz State-> " + this.getSessionAttribute("state"));
            this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
          } else {
            this.setSessionAttribute("IntentState", "TopicIntent");
            // this.setSessionAttribute('Topic', topic);
            var NewSession = false;
            // var topic = this.getInput();
            if (this.getSessionAttribute("IntentState") == "NewSession") {
              NewSession = true;
            }
            var SelectedTopic = "";
            // var repromptSpeech = "";
            // var speechOutput = "";
            var updatedIntent = this.getIntentName();
            var slotToElicit;
            var ChallengeLevelCount = 0;
            var currentChallengeLevel = "";
            // var userTopicPrompt = this.getInputs(topic);
            // var topic = "";
            var NewSessionFlow = false;
            console.log("mainStateHandlers:TopicIntent start ");
            var userTopicPrompt = "";
    
            // if (this.getIntentName()) {
            if (topic) {
              userTopicPrompt = "%" + topic + "%";
              //     topic = topic;
              console.log("User topic  -> " + userTopicPrompt);
            }
            // }
            // else {
            //     NewSessionFlow = true
            // }
    
            var availableChallengeLevel;
            var topicFound = false;
            var topicName = "";
            var similarity = "";
            var maxSimilarity = "";
            var availableChallengeLevel = "";
            // var uID = 1063;
            let rows;
            // console.log('Query String -> ' + "call SP_GET_TOPICS_DIALOGFLOW ('1083','" + userTopicPrompt + "')");
    
            // console.log("userTopicPrompt -> "+userTopicPrompt)
            let query =
              "call SP_ListOfTopics_alexa ('" +
              this.getSessionAttribute("UserEmail") +
              "', '" +
              userTopicPrompt +
              "')";
            console.log("Query String -> " + query);
            // Database.execute(
            //     database => database.query("call SP_READ_TOPICS_2018_ALEXA ('" + userTopicPrompt + "')")
            //         // database => database.query("call SP_GET_TOPICS_DIALOGFLOW ('1083','" + userTopicPrompt + "')")
            //         .then(r => {
            //             rows = r;
            //         })
            // ).then(() => {
    
            // }).catch(err => {
            //     console.log('DB Error: ', err);
            //     this.tell(constants.errorPrompt);
            // });
            rows = await functionCalls.callAPI(query);
            this.setSessionAttribute("listTopicsLength", rows.length);
            console.log("Response rows -> " + rows[0].length);   //Bug reported
    
            if (!rows.length) {
              speechOutput =
                "No topics found in your library, please subscribe topics on myQuiz dot com account ";
              repromptSpeech = speechOutput;
            } else {
              var multipleTopic;
              for (var i = 0; i < rows[0].length; i++) {
                i === 0
                  ? (multipleTopic = false)
                  : multipleTopic == true
                    ? (multipleTopic = true)
                    : rows[0][i].name === rows[0][i - 1].name
                      ? (multipleTopic = false)
                      : (multipleTopic = true);
    
                i === 0
                  ? (topicName += rows[0][i].name)
                  : rows[0][i].name === rows[0][i - 1].name
                    ? ""
                    : (topicName += ` <s>` + rows[0][i].name + " </s> ");
              }
              //If the user has not subscribed any topics
              var queryToCheckSubscribedTopic = "SELECT * FROM EduVoiceDB.userTopic_xRef where isSubscribed = 1 && userId = " +
                this.getSessionAttribute("UserId");
              var resultRows = await functionCalls.callAPI(queryToCheckSubscribedTopic);
              if (resultRows.length) {
                console.log("multipleTopic ->" + multipleTopic);
                console.log("topicName ->" + topicName);
                if (multipleTopic && topic != "") {
                  this.getSessionAttribute("TopicId");
                  topicCounter = 3;
                  speechOutput =
                    topic +
                    " is not part of your subscribed topics. Please go and subscribe the topics, or,  Say <s> Open and the Topic Name</s> or say - list topics to get other topics";
                  repromptSpeech =
                    " Say <s> Open and the Topic Name</s>, Options are " +
                    topicName +
                    constants.HelpPrompt;
                }
                // else if (multipleTopic && NewSession == true) {
                //     speechOutput = this.getSessionAttribute('speechOutput') + ' Say <s> Open and the Topic Name</s>, Or Say Find topics ';
                //     repromptSpeech = ' Say <s> Open and the Topic Name</s>, Options are ' + topicName;
                // }
                else if (multipleTopic && NewSession == false) {
                  topicCounter = 3;
                  speechOutput =
                    "Available Topics are " +
                    topicName +
                    " Say <s> Open and the Topic Name</s> or say - more topics to get other topics";
                  repromptSpeech =
                    " Say <s> Open and the Topic Name</s>, Options are " + topicName;
                } else {
                  for (var i = 0; i < rows[0].length; i++) {
                    if (i > 0) {
                      this.setSessionAttribute("storeResultOnlyOnce", "No")
                    }
                    else {
                      this.setSessionAttribute("storeResultOnlyOnce", "Yes")
                    }
                    console.log(topic + "---" + rows[0][i].name);
                    console.log("TEST TYPE ---- " + rows[0][i] + ", " + rows[0]);
                    // console.log(clj_fuzzy.phonetics.soundex(topic) == clj_fuzzy.phonetics.soundex(rows[0][i].name));
                    // console.log(clj_fuzzy.phonetics.soundex(topic) == clj_fuzzy.phonetics.soundex(rows[0][i].name));
                    // if ((rows[0][i].name.toString().toLowerCase() === topic.toString().toLowerCase()) ||
                    //     (clj_fuzzy.phonetics.soundex(topic) == clj_fuzzy.phonetics.soundex(rows[0][i].name))) {
                    maxSimilarity = util.similar(topic, rows[0][i].name);
                    topicFound = true;
                    this.setSessionAttribute("TopicId", rows[0][i].topicId);
                    this.setSessionAttribute("testType", rows[0][i].testTypeCode);
    
                    console.log(
                      "Topic ID -> " +
                      this.getSessionAttribute("TopicId") +
                      " TEST type : " +
                      this.getSessionAttribute("testType")
                    );
                    ///////Meenu
                    var testResultLength = 0;
                    // var testTypeCode = this.getSessionAttribute("testType");
                    var queryToCheckOneTimeTest = "select * FROM EduVoiceDB.topic where topicId= " +
                      this.getSessionAttribute("TopicId") + " and oneTimeTest = 'yes' ";
                    var queryToCheckOneTimeTestResult = await functionCalls.callAPI(queryToCheckOneTimeTest);
                    console.log(' queryToCheckOneTimeTest => ' + queryToCheckOneTimeTestResult.length);
                    if (queryToCheckOneTimeTestResult.length > 0) {
                      var queryString = "select userId from testResult where userId=" + this.getSessionAttribute("UserId") + " and topicId= " + this.getSessionAttribute("TopicId");
                      var queryStringRows = await functionCalls.callAPI(queryString);
                      testResultLength = queryStringRows.length;
                      this.setSessionAttribute('oneTimeTest', 'Yes');
                    }
                    // else {
                    else {
                      this.setSessionAttribute('oneTimeTest', 'No');
                    }
                    console.log('testResultLength -> ' + testResultLength);
                    if (testResultLength > 0) {
                      this.setSessionAttribute("IntentState", "alreadyTakenTest");
                      await this.toIntent("IdentifyUser");
                      // this.tell("You already have taken this test. You can't access it now.");
                    }
                    else {
                      this.setSessionAttribute("Topic", rows[0][i].name);
                      console.log(
                        "Topic Name -> " +
                        this.getSessionAttribute("Topic") +
                        " Percent Match -> " +
                        maxSimilarity
                      );
                      SelectedTopic = rows[0][i].name;
                      // no challenge level for one time test:-
                      var sum = 0;
                      console.log('Length of the count -> ' + rows[0].length); //to check the length
                      for (var m = 0; m < rows[0].length; m++) {
                        sum = rows[0][m].count + sum;
                      }
                      console.log("ChallengeLevelCount " + ChallengeLevelCount);
                      if (this.getSessionAttribute('oneTimeTest') === "Yes") {
                        console.log('sum -> ' + sum);
                        if (sum >= constants.minNumberOfQuestionsToStartTest) {
                          availableChallengeLevel += `<s>Starter</s>`;
                          ChallengeLevelCount = 1;
                          currentChallengeLevel = "S";
                          console.log(' manually given challenge level -> ');
                        }
                      } else if (this.getSessionAttribute("MultiplayerQuizIntent")) {
                        console.log("ChallengeLevelCount " + ChallengeLevelCount);
                        if (this.getSessionAttribute("numberOfPlayers") === 2) {
                          if (sum >= 10) {
                            availableChallengeLevel += `<s>Starter</s>`;
                            ChallengeLevelCount = 1;
                            currentChallengeLevel = "S";
                          } else {
                            this.ask("You don't have enough questions to start this quiz, please choose another topic or else choose less number of people.");
                          }
                        } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
                          console.log("number of questions -> " + sum);
                          console.log("ChallengeLevelCount " + ChallengeLevelCount);
                          if (sum >= 15) {
                            availableChallengeLevel += `<s>Starter</s>`;
                            ChallengeLevelCount = 1;
                            currentChallengeLevel = "S";
                          } else {
                            this.ask("You don't have enough questions to start this quiz, please choose another topic or else choose less number of people.");
                          }
                          console.log("ChallengeLevelCount " + ChallengeLevelCount);
                        } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
                          if (sum >= 20) {
                            availableChallengeLevel += `<s>Starter</s>`;
                            ChallengeLevelCount = 1;
                            currentChallengeLevel = "S";
                          } else {
                            this.ask("You don't have enough questions to start this quiz, please choose another topic or else choose less number of people.");
                          }
                        }
                      }
                      else {
                        if (
                          rows[0][i].challenge == "S" &&
                          rows[0][i].count >= constants.minNumberOfQuestionsToStartTest
                        ) {
                          availableChallengeLevel += `<s>Starter</s>`;
                          ChallengeLevelCount += 1;
                          currentChallengeLevel = "S";
                          // this.setSessionAttribute("challengeLevelStarter",
                          //   currentChallengeLevel
                          // );
                          console.log("currentChallengeLevel = S");
                        }
                        if (
                          rows[0][i].challenge == "I" &&
                          rows[0][i].count >= constants.minNumberOfQuestionsToStartTest
                        ) {
                          availableChallengeLevel += `<s>Intermediate</s>`;
                          ChallengeLevelCount += 1;
                          currentChallengeLevel = "I";
                          //   this.setSessionAttribute("challengeLevelIntermediate",
                          //   currentChallengeLevel
                          // );
                          console.log("currentChallengeLevel = I");
                        }
                        if (
                          rows[0][i].challenge == "A" &&
                          rows[0][i].count >= constants.minNumberOfQuestionsToStartTest
                        ) {
                          availableChallengeLevel += `<s>Advanced</s>`;
                          ChallengeLevelCount += 1;
                          currentChallengeLevel = "A";
                          //   this.setSessionAttribute("challengeLevelAdvanced",
                          //   currentChallengeLevel
                          // );
                          console.log("currentChallengeLevel = A");
                        }
                      }
                    }
                    console.log("currentChallengeLevel -> " + currentChallengeLevel);
                    if (topicName.length > 0) {
                      if (!SelectedTopic) {
                        console.log("Topic not found ");
                        // app.setSessionAttribute('Topic', '');
                        this.setSessionAttribute("AvailableTopics", topicName);
                        console.log(
                          "Topics at the end of topic intent ->" +
                          this.getSessionAttribute("AvailableTopics")
                        );
                        speechOutput =
                          "No matches found for <s> " +
                          topic +
                          "</s> Available Topics are : " +
                          topicName +
                          " say <s>open</s> and name of the  Topic";
                        (repromptSpeech =
                          "Available Topics are <s>" +
                          topicName +
                          "</s>, say <s>open</s> and, topic name! "),
                          "You could also say exit to end";
                      } else {
                        console.log("SelectedTopic -> " + SelectedTopic);
                        speechOutput =
                          "Available challenge Level " +
                          availableChallengeLevel +
                          " Say <s> Select </s> and the <s>Challenge level</s>";
                        repromptSpeech =
                          " Say <s> Select </s> and the <s>Challenge level</s>, Options are " +
                          availableChallengeLevel;
                        this.setSessionAttribute("UserState", constants.UserState.TopicNameSelected);
                        this.setSessionAttribute("state", constants.states.MAIN);
                        if (ChallengeLevelCount == 1) {
                          // this.setSessionAttribute('ChallengeLevelContains', 'Yes');
                          this.setSessionAttribute(
                            "UserState",
                            constants.UserState.ChallangeLevelSelected
                          );
                          // this.handler.state = constants.states.QUESTIONANSWER;
                          this.setSessionAttribute(
                            "state",
                            constants.states.QUESTIONANSWER
                          );
    
                          this.setSessionAttribute(
                            "challengeLevelInd",
                            currentChallengeLevel
                          );
                          await this.toIntent("populateQuestions");
                        }
                      }
                    } else {
                      speechOutput =
                        "No matches found for <s> " +
                        topic +
                        ". please go and subscribe for the topics";
                      repromptSpeech = speechOutput;
                    }
                  }
                }
                console.log("Completing Topic Intent");
              }
              else {
                speechOutput =
                  "No topics found in your library, please subscribe topics on myQuiz dot com account ";
                repromptSpeech = speechOutput;
              }
              if (!topicFound || ChallengeLevelCount > 1 || multipleTopic == true) {
                this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
              }
            }
          }
        }
      },
    
      //start of modularCode3.js

      // populateQuestions: async function () {
      //   this.setSessionAttribute("IntentState", "populateQuestions");
      //   var ansData = [];
      //   var randomQuestions = [];
      //   var data;
      //   console.log(
      //     "populateQuestions handler : Populate Questions for " +
      //     this.getSessionAttribute("Topic")
      //   );
      //   var topic = this.getSessionAttribute("Topic");
      //   var testTopic = this.getSessionAttribute("Topic");
      //   var qId = [], qWeightage = [], qChallengeLevel = [];
      //   var speechOutput = "", repromptSpeech = "", images = this;
      //   //If topic selected is not available return a friendly message.
    
      //   //Printing the event request
      //   //Setting null to the session attribute variables
      //   this.setSessionAttribute("correctAnsweredQuestionId", "");
      //   this.setSessionAttribute("wrongAnsweredQuestionId", "");
      //   this.setSessionAttribute("notAttemptedQuestionId", "");
      //   this.setSessionAttribute("correctAnsweredWeightage", "");
      //   this.setSessionAttribute("wrongAnsweredWeightage", "");
      //   this.setSessionAttribute("notAttemptedQuestionWeightage", "");
      //   //First time load set questionCount = 0
      //   //Get questions for the sleected Topic
      //   this.setSessionAttribute("TestIndex", 0);
      //   this.setSessionAttribute("currentQuestionIndex", 0);
      //   this.setSessionAttribute("correctAnswerScore", 0);
      //   this.setSessionAttribute("challengeAdvanceCountQuestions", 0);
      //   this.setSessionAttribute("challengeAdvanceCountCorrectAns", 0);
      //   this.setSessionAttribute("challengeAdvanceCountWrongAns", 0);
      //   this.setSessionAttribute("challengeAdvanceCountNotAttemptedAns", 0);
      //   this.setSessionAttribute("challengeIntermediateCountQuestions", 0);
      //   this.setSessionAttribute("challengeIntermediateCountCorrectAns", 0);
      //   this.setSessionAttribute("challengeIntermediateCountWrongAns", 0);
      //   this.setSessionAttribute("challengeIntermediateCountNotAttemptedAns", 0);
      //   this.setSessionAttribute("challengeStarterCountQuestions", 0);
      //   this.setSessionAttribute("challengeStarterCountCorrectAns", 0);
      //   this.setSessionAttribute("challengeStarterCountWrongAns", 0);
      //   this.setSessionAttribute("challengeStarterCountNotAttemptedAns", 0);
      //   var count = 0;
    
      //   //setting initial values for Multiplayer quiz 
      //   this.setSessionAttribute("firstPlayerTotalQue", 0)
      //   this.setSessionAttribute("firstPlayerCorrectAnsQue", 0)
      //   this.setSessionAttribute("firstPlayerWrongAnsQue", 0)
      //   this.setSessionAttribute("firstPlayerSkipedQue", 0)
    
      //   this.setSessionAttribute("secondPlayerTotalQue", 0)
      //   this.setSessionAttribute("secondPlayerCorrectAnsQue", 0)
      //   this.setSessionAttribute("secondPlayerWrongAnsQue", 0)
      //   this.setSessionAttribute("secondPlayerSkipedQue", 0)
    
      //   this.setSessionAttribute("thirdPlayerTotalQue", 0);
      //   this.setSessionAttribute("thirdPlayerCorrectAnsQue", 0);
      //   this.setSessionAttribute("thirdPlayerWrongAnsQue", 0);
      //   this.setSessionAttribute("thirdPlayerSkipedQue", 0);
    
      //   this.setSessionAttribute("fourthPlayerTotalQue", 0);
      //   this.setSessionAttribute("fourthPlayerCorrectAnsQue", 0);
      //   this.setSessionAttribute("fourthPlayerWrongAnsQue", 0);
      //   this.setSessionAttribute("fourthPlayerSkipedQue", 0);
    
      //   this.setSessionAttribute("firstPlayerSet",
      //     this.getSessionAttribute("firstPlayerName") +
      //     ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //     ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //     ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //     ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //   );
    
      //   this.setSessionAttribute("secondPlayerSet",
      //     this.getSessionAttribute("secondPlayerName") +
      //     ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //     ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //     ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //     ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //   );
    
      //   this.setSessionAttribute("thirdPlayerSet",
      //     this.getSessionAttribute("thirdPlayerName") +
      //     ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
      //     ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
      //     ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
      //     ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
      //   );
    
      //   this.setSessionAttribute("fourthPlayerSet",
      //     this.getSessionAttribute("fourthPlayerName") +
      //     ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
      //     ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
      //     ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
      //     ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
      //   );
    
      //   // var queryString =
      //   //   'call SP_accessQuestion_alexa("' +
      //   //   this.getSessionAttribute("TopicId") +
      //   //   '", "' +
      //   //   this.getSessionAttribute("challengeLevelInd") +
      //   //   '","0","' +
      //   //   constants.questionsToRead +
      //   //   '")';
    
      //   var numberOfQuestionsToAccess = constants.questionsToRead;
      //   this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
      //   if (this.getSessionAttribute("MultiplayerQuizIntent")) {
      //     if (this.getSessionAttribute("MultiplayerQuizIntent") &&
      //       this.getSessionAttribute("numberOfPlayers") === 2) {
      //       numberOfQuestionsToAccess = 10;
      //       this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
      //       this.setSessionAttribute("quizType", "twoPlayerQuiz");
      //       this.setSessionAttribute("thirdPlayerSet", "");
      //       this.setSessionAttribute("fourthPlayerSet", "");
      //     } else if (this.getSessionAttribute("MultiplayerQuizIntent") &&
      //       this.getSessionAttribute("numberOfPlayers") === 3) {
      //       numberOfQuestionsToAccess = 15;
      //       this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
      //       this.setSessionAttribute("quizType", "threePlayerQuiz");
      //       this.setSessionAttribute("fourthPlayerSet", "");
      //     } else if (this.getSessionAttribute("MultiplayerQuizIntent") &&
      //       this.getSessionAttribute("numberOfPlayers") === 4) {
      //       numberOfQuestionsToAccess = 20;
      //       this.setSessionAttribute("numberOfQuestionsToAccess", numberOfQuestionsToAccess);
      //       this.setSessionAttribute("quizType", "fourPlayerQuiz");
      //     }
      //     var queryString =
      //       'call SP_accessQuestion_alexa("' +
      //       this.getSessionAttribute("TopicId") +
      //       '", "' +
      //       this.getSessionAttribute("challengeLevelInd") +
      //       '","' +
      //       numberOfQuestionsToAccess +
      //       '")';
      //   } else {
      //     this.setSessionAttribute("quizType", "singlePlayerQuiz");
      //     this.setSessionAttribute("firstPlayerSet", "");
      //     this.setSessionAttribute("secondPlayerSet", "");
      //     this.setSessionAttribute("thirdPlayerSet", "");
      //     this.setSessionAttribute("fourthPlayerSet", "");
      //     var queryString =
      //       'call SP_accessQuestion_alexa_Dev_2018("' +
      //       this.getSessionAttribute("TopicId") +
      //       '", "' +
      //       this.getSessionAttribute("challengeLevelInd") +
      //       '","0","' +
      //       numberOfQuestionsToAccess +
      //       '")';
      //   }
    
      //   // Database.execute(
      //   //     database => database.query(queryString)
      //   //         .then(r => {
      //   //             rows = r;
      //   //         })
      //   // ).then(() => {
    
      //   // }).catch(err => {
      //   //     console.log('DB Error in NewSession Request: ', err);
      //   //     this.tell(constants.errorPrompt);
      //   // });
    
      //   var rows = await functionCalls.callAPI(queryString);
    
      //   data = rows[0];
      //   this.setSessionAttribute("questionsTaken", data.length);
      //   for (var i in data) {
      //     qId[i] = data[i].questionId;
      //     qWeightage[i] = data[i].weightage;
      //     qChallengeLevel[i] = data[i].challenge;
      //     if (qChallengeLevel[i] === "S") {
      //       this.setSessionAttribute(
      //         "challengeStarterCountQuestions",
      //         this.getSessionAttribute("challengeStarterCountQuestions") + 1
      //       );
      //     } else if (qChallengeLevel[i] === "I") {
      //       this.setSessionAttribute(
      //         "challengeIntermediateCountQuestions",
      //         this.getSessionAttribute("challengeIntermediateCountQuestions") + 1
      //       );
      //     } else {
      //       this.setSessionAttribute(
      //         "challengeAdvanceCountQuestions",
      //         this.getSessionAttribute("challengeAdvanceCountQuestions") + 1
      //       );
      //     }
      //     console.log("Loading questioNs : " + data[i]);
      //   }
      //   console.log("starter -> " + this.getSessionAttribute("challengeStarterCountQuestions"));
      //   console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountQuestions"));
      //   console.log("advanced -> " + this.getSessionAttribute("challengeAdvanceCountQuestions"));
    
      //   var maxQuestionID = arrayFunctions.maxAaaryValue(qId);
      //   console.log("Max Question Id-> " + maxQuestionID);
      //   console.log("Qiestion Id " + qId);
      //   console.log("Questions weightage " + qWeightage);
      //   //Add o session attribute
      //   this.setSessionAttribute("maxQuestionID", maxQuestionID);
      //   questionData = arrayFunctions.shuffleArray(data);
      //   this.setSessionAttribute("questions", questionData);
      //   console.log("Loaded qsTns : " + questionData);
      //   if ((questionData[0].externalReference) !== null &&
      //     (questionData[0].externalReference) !== undefined) {
      //     if ((questionData[0].externalReference).includes('.png') ||
      //       (questionData[0].externalReference).includes('.jpg')) {
      //       console.log(' text -> ' + questionData[0].text);
      //       console.log(' text -> ' + questionData[0].questionId);
      //       this.setSessionAttribute('ImageContainer', 'Yes');
      //       console.log('image8');
      //     } else if ((questionData[0].externalReference).includes('.mp3')) {
      //       console.log('AudioContainer1');
      //       this.setSessionAttribute('AudioContainer', 'Yes');
      //     }
      //   }
      //   //Reading the first Question from the multi dimentional array.
      //   //1 Read the questions
      //   if (this.getSessionAttribute("MultiplayerQuizIntent")) {
      //     this.setSessionAttribute("currentPlayerName", this.getSessionAttribute("firstPlayerName"));
      //     speechOutput =
      //       `Starting Quiz for <s>` +
      //       testTopic +
      //       ` </s>  Question for <s> ` +
      //       this.getSessionAttribute("currentPlayerName") +
      //       ` </s>  <prosody rate="` +
      //       constants.speachSpeed +
      //       `">` +
      //       questionData[0].text +
      //       "  <s> Options are </s>";
      //   } else {
      //     if (this.getSessionAttribute('ImageContainer') === 'Yes') {
      //       // let title = '';
      //       // let content = '';
      //       console.log('image2');
      //       console.log(" image url -> " + questionData[0].externalReference);
      //       imageUrl = questionData[0].externalReference;
      //       images = this.showImageCard(title, content, imageUrl);
      //       // this.showImageCard(title, content, imageUrl).ask(questionData[0].text);
      //       speechOutput =
      //         `Starting  Quiz for <s>` +
      //         testTopic +
      //         ` </s>  Question, <prosody rate="` +
      //         constants.speachSpeed +
      //         `">` +
      //         questionData[0].text +
      //         ", <s> Options are </s>";
      //       // this.setSessionAttribute('ImageContainer', 'No');
      //     } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
      //       console.log('AudioContainer2');
      //       speechOutput =
      //         `Starting  Quiz for <s>` +
      //         testTopic +
      //         ` </s>  Question, <prosody rate="` +
      //         constants.speachSpeed +
      //         `">` +
      //         questionData[0].text +
      //         "<audio src= '" + questionData[0].externalReference + "'/>" +
      //         // this.showImageCard(title, content, imageUrl)  +
      //         " <s> Options are </s>";
      //       // this.setSessionAttribute('AudioContainer', 'No');
      //     }
      //     else {
      //       speechOutput =
      //         `Starting  Quiz for <s>` +
      //         testTopic +
      //         ` </s>  Question, <prosody rate="` +
      //         constants.speachSpeed +
      //         `">` +
      //         questionData[0].text +
      //         ", <s> Options are </s>";
      //     }
      //   }
    
      //   //1 Read the options
      //   //Read the option and output them
      //   this.getSessionAttribute("currentState", "populateQuestions");
      //   var rowData = JSON.parse("[" + questionData[0].options + "]");
      //   var opt = " ";
      //   for (var key = 0; key < rowData.length; key++) {
      //     var dt = rowData[key];
      //     for (var i in dt) {
      //       if (dt[i] != "") {
      //         speechOutput =
      //           speechOutput +
      //           (`<s>` +
      //             i +
      //             `</s>` +
      //             " - " +
      //             dt[i] +
      //             ',<break time="' +
      //             constants.pauseTime +
      //             '"/> ');
      //         opt = opt + i + `,  ` + ',<break time="' +
      //           constants.pauseTime +
      //           '"/> ';
      //         answerText[i] = dt[i];
      //       }
      //     }
      //   }
      //   speechOutput += `</prosody>`;
      //   this.setSessionAttribute("optionsChoice", opt);
      //   console.log("Questoin -> " + speechOutput);
    
      //   this.setSessionAttribute("score", 0);
      //   this.setSessionAttribute("correctAnswerText", questionData[0].answer);
      //   this.setSessionAttribute("questionId", questionData[0].questionId);
      //   this.setSessionAttribute("questionWeightage", questionData[0].weightage);
      //   this.setSessionAttribute("challengeLevelOfEachQue", questionData[0].challenge); //To know the challenge level of 1st question
      //   console.log("Qid - > " + this.getSessionAttribute("questionId"));
      //   console.log("Qweightage - > " + this.getSessionAttribute("questionWeightage"));
      //   console.log("challengeLevelOfEachQue - > " + this.getSessionAttribute("challengeLevelOfEachQue"));
      //   this.setSessionAttribute("UserState", constants.UserState.QuestionsRead);
      //   this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
      //   // this.ask(speechOutput);
      //   //meenu changes session
      //   console.log('Userstate -> ' + this.getSessionAttribute("ChallengeLevelContains"));
      //   console.log('storeResultOnlyOnce -> ' + this.getSessionAttribute("storeResultOnlyOnce"));
      //   if (this.getSessionAttribute('ChallengeLevelContains') !== "Yes" ||
      //     this.getSessionAttribute('ChallengeLevelContains') === undefined) {
      //     if (this.getSessionAttribute("storeResultOnlyOnce") === "Yes" ||
      //       this.getSessionAttribute('storeResultOnlyOnce') === undefined) {
      //       console.log('organisation id -> ' + this.getSessionAttribute("organizationId"));
      //       console.log('organisation id -> ' + this.getSessionAttribute("courseId"));
      //       console.log("starter -> " + this.getSessionAttribute("challengeStarterCountQuestions"));
      //       console.log("intermediate -> " + this.getSessionAttribute("challengeIntermediateCountQuestions"));
      //       console.log("advanced -> " + this.getSessionAttribute("challengeAdvanceCountQuestions"));
      //       if (!this.getSessionAttribute("organizationId") && !this.getSessionAttribute("courseId")) {
      //         this.setSessionAttribute("organizationId", null);
      //         this.setSessionAttribute("courseId", null);
      //       }
      //       var sessionId = "this.getsessionId()";
      //       var date = new Date();
      //       var queryStringResult =
      //         'insert into EduVoiceDB.testResult (userId, topicId, questionsTaken, correctAnswer, totalScore, ' +
      //         'challengeHighCountQuestions, challengeHighCountCorrectAns, challengeHighCountWrongAns,challengeHighCountNotAttemptedAns, challengeIntermediateCountQuestions, ' +
      //         'challengeIntermediateCountCorrectAns, challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns, challengeStarterCountQuestions, ' +
      //         'challengeStarterCountCorrectAns, challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, correctQuestionId, wrongQuestionId, notAttemptedQuestionId, ' +
      //         'correctQuestion_Weightage,wrongQuestion_Weightage, notAttemptedQuestion_Weightage, createdDate, sessionId, ' +
      //         'attemptedQuestions, courseId, organizationId, section, testTypeCode, roleName, firstPlayerSet, ' +
      //         'secondPlayerSet, thirdPlayerSet, fourthplayerSet, quizType) values ("' +
      //         this.getSessionAttribute("UserId") +
      //         '",' +
      //         this.getSessionAttribute("TopicId") +
      //         "," +
      //         this.getSessionAttribute("questionsTaken") +
      //         "," +
      //         this.getSessionAttribute("score") +
      //         "," +
      //         this.getSessionAttribute("correctAnswerScore") +
      //         "," +
      //         this.getSessionAttribute("challengeAdvanceCountQuestions") +
      //         "," +
      //         this.getSessionAttribute("challengeAdvanceCountCorrectAns") +
      //         "," +
      //         this.getSessionAttribute("challengeAdvanceCountWrongAns") +
      //         "," +
      //         this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns") +
      //         "," +
      //         this.getSessionAttribute("challengeIntermediateCountQuestions") +
      //         "," +
      //         this.getSessionAttribute("challengeIntermediateCountCorrectAns") +
      //         "," +
      //         this.getSessionAttribute("challengeIntermediateCountWrongAns") +
      //         "," +
      //         this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns") +
      //         "," +
      //         this.getSessionAttribute("challengeStarterCountQuestions") +
      //         "," +
      //         this.getSessionAttribute("challengeStarterCountCorrectAns") +
      //         "," +
      //         this.getSessionAttribute("challengeStarterCountWrongAns") +
      //         "," +
      //         this.getSessionAttribute("challengeStarterCountNotAttemptedAns") +
      //         ',"' +
      //         this.getSessionAttribute("correctAnsweredQuestionId") +
      //         '","' +
      //         this.getSessionAttribute("wrongAnsweredQuestionId") +
      //         '","' +
      //         this.getSessionAttribute("notAttemptedQuestionId") +
      //         '","' +
      //         this.getSessionAttribute("correctAnsweredWeightage") +
      //         '","' +
      //         this.getSessionAttribute("wrongAnsweredWeightage") +
      //         '","' +
      //         this.getSessionAttribute("notAttemptedQuestionWeightage") +
      //         '","' +
      //         date.getFullYear() +
      //         "/" +
      //         (date.getMonth() + 1) +
      //         "/" +
      //         date.getDate() +
      //         " " +
      //         date.getHours() +
      //         ":" +
      //         date.getMinutes() +
      //         ":" +
      //         date.getSeconds() +
      //         '","' +
      //         sessionId +
      //         '", ' +
      //         this.getSessionAttribute("TestIndex") +
      //         "," +
      //         this.getSessionAttribute("courseId") +
      //         "," +
      //         this.getSessionAttribute("organizationId") +
      //         ', "' +
      //         this.getSessionAttribute("section") +
      //         '", "' +
      //         this.getSessionAttribute("testType") +
      //         '", "' +
      //         this.getSessionAttribute("roleName") +
      //         '", "' +
      //         this.getSessionAttribute("firstPlayerSet") +
      //         '", "' +
      //         this.getSessionAttribute("secondPlayerSet") +
      //         '", "' +
      //         this.getSessionAttribute("thirdPlayerSet") +
      //         '", "' +
      //         this.getSessionAttribute("fourthPlayerSet") +
      //         '", "' +
      //         this.getSessionAttribute("quizType") +
      //         '")';
      //       var resultedRows = await functionCalls.callAPI(queryStringResult);
      //       console.log('resultedRows -> ' + resultedRows);
      //     }
      //   }//meenu
      //   console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
      //   console.log('options access -> ' + opt);
      //   repromptSpeech = `<prosody rate="` +
      //     constants.speachSpeed +
      //     `">` +
      //     `To answer use one of these options ` +
      //     opt +
      //     `</prosody>` +
      //     ` You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>`;
      //   images.ask(
      //     speechOutput,
      //     repromptSpeech, repromptSpeech, goodByeMsg  //Meenu
      //   );
      // },
    // End of modularCode3.js
    
    //Start of modularCode4.js
      // ChallengeLevelIntent: async function () {
      //   var challengeLevel = this.$inputs.challengeLevel.value;
      //   this.setSessionAttribute("IntentState", "ChallengeLevelIntent");
      //   var speechOutput = "", repromptSpeech = "";
      //   if (!this.getSessionAttribute("UserId")) {
      //     speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify" //meenu
      //     repromptSpeech = speechOutput;
      //     this.ask(
      //       speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //meenu
      //     );
      //   } else {
      //     if (this.getSessionAttribute("Topic")) {
      //       var msg;
      //       if (
      //         help.isValidContext(
      //           this.getSessionAttribute("IntentState"),
      //           this.getSessionAttribute("state"),
      //           this
      //         )
      //       ) {
      //         msg = help.isValidContext(
      //           this.getSessionAttribute("IntentState"),
      //           this.getSessionAttribute("state"),
      //           this
      //         );
      //         console.log("ChallengeLevelIntent isValidContext msg -> " + msg);
      //         this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);
      //       } else if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
      //         this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
      //         this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
      //         this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
      //         this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
      //         this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent") {
      //         var txt = help.GetContexBasedHelp(
      //           this.getSessionAttribute("state"),
      //           this.getSessionAttribute("UserState"),
      //           this
      //         );
      //         console.log("Help me text -> " + txt);
      //         console.log("User State-> " + this.getSessionAttribute("UserState"));
      //         console.log("Quiz State-> " + this.getSessionAttribute("state"));
      //         this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      //       } else {
      //         // var challengeLevel;
      //         console.log("questionAnswerHandlers:challengeLevelIntent start ");
      //         //    slotValue = (this.getchallengeLevel() ? this.getchallengeLevel() : "")
      //         var slotValue = challengeLevel;
    
      //         if (
      //           slotValue === "starter" ||
      //           this.getSessionAttribute("challengeLevelInd") === "S"
      //           // this.getSessionAttribute("challengeLevelStarter") === "A"
      //         ) {
      //           challengeLevel = "S";
      //           this.setSessionAttribute("challengeLevelInd", challengeLevel);
      //           slotValue = "starter";
      //         } else if (
      //           slotValue === "intermediate" ||
      //           this.getSessionAttribute("challengeLevelInd") === "I"
      //           // this.getSessionAttribute("challengeLevelIntermediate") === "A"
      //         ) {
      //           challengeLevel = "I";
      //           this.setSessionAttribute("challengeLevelInd", challengeLevel);
      //           slotValue = "intermediate";
      //         } else if (
      //           slotValue === "advanced" ||
      //           this.getSessionAttribute("challengeLevelInd") === "A"
      //           // this.getSessionAttribute("challengeLevelAdvanced") === "A"
      //         ) {
      //           challengeLevel = "A";
      //           this.setSessionAttribute("challengeLevelInd", challengeLevel);
      //           slotValue = "advanced";
      //         } else {
      //           challengeLevel = "S";
      //           slotValue = "starter";
      //           this.setSessionAttribute("challengeLevelInd", challengeLevel);
      //         }
      //         this.setSessionAttribute(
      //           "UserState",
      //           constants.UserState.ChallangeLevelSelected
      //         );
      //         challengLevelSlotValue = slotValue;
      //         // this.handler.state = constants.states.QUESTIONANSWER;
      //         this.setSessionAttribute('ChallengeLevelContains', 'Yes');  //Meenu changes
      //         this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
      //         await this.toIntent("populateQuestions");
      //       }
      //     } else {
      //       speechOutput = "Please select topic before selecting Challenge level. Say <s> Open and the Topic Name</s> or say - more topics to get other topics"; //meenu
      //       repromptSpeech = speechOutput;
      //       this.ask(
      //         speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
      //       );
      //     }
      //   }
      // },
    
      // AnswerIntent: async function () {
      //   // var answer = this.$inputs.Answer.value;
      //   // //TODO : ****************    Ooption needs to be provided to seek th explaination aftere correct/incorrect option before moving tho the next question.
      //   // this.setSessionAttribute("IntentState", "AnswerIntent");
      //   // var msg, speechOutput = "", repromptSpeech = "";
      //   // console.log("Sesion TOPIC called : " + this.getSessionAttribute("TopicId"));
      //   console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
      //   if (!this.getSessionAttribute("UserId")) {
      //     speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify";  //meenu
      //     repromptSpeech = speechOutput;
      //     this.ask(
      //       speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
      //     );
      //   } else {
      //     if (this.getSessionAttribute("IntentState") === "MultiplayerQuizIntent" ||
      //       this.getSessionAttribute("IntentState") === "NumberOfPlayersIntent" ||
      //       this.getSessionAttribute("IntentState") === "FirstPlayerNameIntent" ||
      //       this.getSessionAttribute("IntentState") === "SecondPlayerNameIntent" ||
      //       this.getSessionAttribute("IntentState") === "ThirdPlayerNameIntent" ||
      //       this.getSessionAttribute("IntentState") === "FourthPlayerNameIntent" ||
      //       this.getSessionAttribute("IntentState") === "toCheckPlayersName") {
      //       var txt = help.GetContexBasedHelp(
      //         this.getSessionAttribute("state"),
      //         this.getSessionAttribute("UserState"),
      //         this
      //       );
      //       console.log("Help me text -> " + txt);
      //       console.log("User State-> " + this.getSessionAttribute("UserState"));
      //       console.log("Quiz State-> " + this.getSessionAttribute("state"));
      //       this.ask(txt, txt, constants.HelpPrompt, goodByeMsg);
      //     } else {
      //       var answer = this.$inputs.Answer.value;
      //       //TODO : ****************    Ooption needs to be provided to seek th explaination aftere correct/incorrect option before moving tho the next question.
      //       this.setSessionAttribute("IntentState", "AnswerIntent");
      //       var msg, speechOutput = "", repromptSpeech = "";
      //       console.log("Sesion TOPIC called : " + this.getSessionAttribute("TopicId"));
      //       console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
      //       if (this.getSessionAttribute("TopicId")) {
      //         if (
      //           help.isValidContext(
      //             this.getSessionAttribute("IntentState"),
      //             this.getSessionAttribute("state"),
      //             this
      //           )
      //         ) {
      //           msg = help.isValidContext(
      //             this.getSessionAttribute("IntentState"),
      //             this.getSessionAttribute("state"),
      //             this
      //           );
      //           console.log("AnswerIntent isValidContext msg -> " + msg);
      //           this.ask(msg, msg, constants.HelpPrompt, goodByeMsg);  //meenu
      //         } else {
      //           console.log("start handleAnswers");
      //           console.log("User Answer : " + answer);
      //           // var speechOutput = ""; //meenu
      //           //Start -> For testing that the option user is telling is exist in options or not
      //           // this.setSessionAttribute("IntentState", "AnswerIntent");
      //           var opt = this.getSessionAttribute("optionsChoice");
      //           console.log('options exist or not -> ' + opt.includes(answer));
      //           if (answer.toString().toLowerCase() === 'b.') {
      //             console.log('Correct -> answer -> ' + answer);
      //             answer = "b";
      //           }
      //           if (opt.includes(answer.toString().toUpperCase())) {
      //             console.log(answer.toString().toUpperCase());
      //             console.log('yes u r correct');
      //             answer = answer.toString().toLowerCase();
      //             //In case mo test in progress return a freindly error message. Transfer to NoTestInProgress function
      //             if (!questionData) {
      //               await this.toIntent("NoTestInProgress");
      //             } else {
      //               if (!answer || answer === undefined) {
      //                 console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
      //                 console.log(' options choice -> ' + this.getSessionAttribute("optionsChoice"));  //options choices
      //                 var opt = this.getSessionAttribute("optionsChoice");
      //                 speechOutput = "answer the question by saying - the answer is " + opt;  //meenu
      //                 repromptSpeech = speechOutput;
      //                 this.ask(
      //                   speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
      //                 );
      //               } else {
      //                 var quslen = this.getSessionAttribute("questions").length;
      //                 // this.attributes['firstPlayerScore'] = 0;
      //                 // this.attributes['secondPlayerScore'] = 0;
    
      //                 //	if (answer === "a" || answer === "b" || answer === "c" || answer === "d" || answer === "e") {
      //                 var correctAnswerFlag = false;
    
      //                 answer = answer.toString().toLowerCase();
    
      //                 //Sometime alexa returns a . in addition to the option. below is to address it
      //                 answer = answer.substr(0, 1);
    
      //                 this.setSessionAttribute(
      //                   "currentQuestionIndex",
      //                   this.getSessionAttribute("currentQuestionIndex") + 1
      //                 );
      //                 this.setSessionAttribute(
      //                   "TestIndex",
      //                   this.getSessionAttribute("TestIndex") + 1
      //                 );
    
      //                 if (
      //                   this.getSessionAttribute("correctAnswerText")
      //                     .toString()
      //                     .toLowerCase() === answer.toString().toLowerCase()
      //                 ) {
      //                   if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
      //                     speechOutput = " correct answer ";
      //                   }
      //                   else {
      //                     speechOutput = "";
      //                   }
      //                   correctAnswerFlag = true;
      //                   //storing this score value in to database so, keeping as it is. later can be removed.
      //                   this.setSessionAttribute(
      //                     "score",
      //                     this.getSessionAttribute("score") + 1
      //                   );
      //                   if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
      //                     this.setSessionAttribute(
      //                       "challengeStarterCountCorrectAns",
      //                       this.getSessionAttribute("challengeStarterCountCorrectAns") + 1
      //                     );
      //                   } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
      //                     this.setSessionAttribute(
      //                       "challengeIntermediateCountCorrectAns",
      //                       this.getSessionAttribute("challengeIntermediateCountCorrectAns") + 1
      //                     );
      //                   } else {
      //                     this.setSessionAttribute(
      //                       "challengeAdvanceCountCorrectAns",
      //                       this.getSessionAttribute("challengeAdvanceCountCorrectAns") + 1
      //                     );
      //                   }
    
      //                   console.log("Correct Answer ");
    
      //                   //Meenu changes session
      //                   var UserId = this.getSessionAttribute("UserId");
      //                   var TopicId = this.getSessionAttribute("TopicId");
      //                   var questionsTaken = this.getSessionAttribute("questionsTaken");
      //                   var score = this.getSessionAttribute("score");
      //                   var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
      //                   var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
      //                   var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
      //                   var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
      //                   var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
      //                   var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
      //                   var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
      //                   var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
      //                   var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
      //                   var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
      //                   var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
      //                   var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
      //                   var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
      //                   var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
      //                   var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
      //                   var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
      //                   var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
      //                   var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
      //                   var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
      //                   var TestIndex = this.getSessionAttribute("TestIndex");
      //                   var courseId = this.getSessionAttribute("courseId");
      //                   var organizationId = this.getSessionAttribute("organizationId");
      //                   var section = this.getSessionAttribute("section");
      //                   var testType = this.getSessionAttribute("testType");
      //                   var roleName = this.getSessionAttribute("roleName");
      //                   var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
      //                   var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
      //                   var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
      //                   var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
      //                   var quizType = this.getSessionAttribute("quizType");
      //                   await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
      //                     correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
      //                     correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
      //                     challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
      //                     challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
      //                     challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
      //                     challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
      //                     challengeStarterCountQuestions, challengeStarterCountCorrectAns,
      //                     challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
      //                     courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
      //                     thirdPlayerSet, fourthPlayerSet, quizType);
      //                   //QuestionId which are answered correct
      //                   this.setSessionAttribute(
      //                     "correctAnsweredQuestionId",
      //                     this.getSessionAttribute("correctAnsweredQuestionId") +
      //                     this.getSessionAttribute("questionId") +
      //                     " "
      //                   );
      //                   this.setSessionAttribute(
      //                     "correctAnsweredWeightage",
      //                     this.getSessionAttribute("correctAnsweredWeightage") +
      //                     this.getSessionAttribute("questionWeightage") +
      //                     " "
      //                   );  //Weightage
      //                   this.setSessionAttribute(
      //                     'correctAnswerScore',
      //                     this.getSessionAttribute('correctAnswerScore') +
      //                     this.getSessionAttribute('questionWeightage')
      //                   );
    
      //                   console.log(
      //                     "Correct Answered QuestionId --> " +
      //                     this.getSessionAttribute("correctAnsweredQuestionId")
      //                   );
      //                   console.log(
      //                     "Correct Answered Wightage --> " +
      //                     this.getSessionAttribute("correctAnsweredWeightage")
      //                   );
      //                   console.log("Correct Answered Total Score --> " +
      //                     this.getSessionAttribute('correctAnswerScore'));
    
      //                   //TODO Read questions for the Selected Values
      //                   console.log(
      //                     "Total Question length : " +
      //                     quslen +
      //                     " Test index -> " +
      //                     this.getSessionAttribute("TestIndex")
      //                   );
      //                   this.setSessionAttribute("speechOutput", speechOutput);
      //                 } else {
      //                   if (this.getSessionAttribute('oneTimeTest') !== 'Yes') {
      //                     speechOutput =
      //                       " - ,Incorrect answer, the correct answer is, " +
      //                       this.getSessionAttribute("correctAnswerText") +
      //                       " - <s>" +
      //                       answerText[this.getSessionAttribute("correctAnswerText")] +
      //                       "</s>,  ";
      //                   }
      //                   else {
      //                     speechOutput = "";
      //                   }
    
      //                   //QuestionId which are answered wrong
      //                   this.setSessionAttribute(
      //                     "wrongAnsweredQuestionId",
      //                     this.getSessionAttribute("wrongAnsweredQuestionId") +
      //                     this.getSessionAttribute("questionId") +
      //                     " "
      //                   );
      //                   this.setSessionAttribute(
      //                     "wrongAnsweredWeightage",
      //                     this.getSessionAttribute("wrongAnsweredWeightage") +
      //                     this.getSessionAttribute("questionWeightage") +
      //                     " "
      //                   );
      //                   if (this.getSessionAttribute("challengeLevelOfEachQue") === "S") {
      //                     this.setSessionAttribute(
      //                       "challengeStarterCountWrongAns",
      //                       this.getSessionAttribute("challengeStarterCountWrongAns") + 1
      //                     );
      //                   } else if (this.getSessionAttribute("challengeLevelOfEachQue") === "I") {
      //                     this.setSessionAttribute(
      //                       "challengeIntermediateCountWrongAns",
      //                       this.getSessionAttribute("challengeIntermediateCountWrongAns") + 1
      //                     );
      //                   } else {
      //                     this.setSessionAttribute(
      //                       "challengeAdvanceCountWrongAns",
      //                       this.getSessionAttribute("challengeAdvanceCountWrongAns") + 1
      //                     );
      //                   }
      //                   console.log(
      //                     "Wrong Answered QuestionId --> " +
      //                     this.getSessionAttribute("wrongAnsweredQuestionId")
      //                   );
      //                   console.log(
      //                     "Wrong Answered Wightage --> " +
      //                     this.getSessionAttribute("wrongAnsweredWeightage")
      //                   );
      //                   //Ask for explaination
      //                   this.setSessionAttribute("speechOutput", speechOutput);
    
      //                   //VoiceLabs.track(this.event.session, null, null, null, (error, response) => {
      //                   // this.ask( speechOutput, ', To Explain, try saying, - Explain the Answer or say next question - to continue quiz ');
      //                   //});
      //                 }
      //                 console.log(
      //                   "Current question index -> " +
      //                   this.getSessionAttribute("currentQuestionIndex")
      //                 );
    
    
      //                 // for multiplayer quizes :- 
      //                 //This block of code is for all players 2,3,4, only for wrong and correct answered
      //                 // questions. For skiped questions search for "call next question"
      //                 if (this.getSessionAttribute("MultiplayerQuizIntent") &&
      //                   this.getSessionAttribute("MultiplayerQuizIntent") !== null) {
      //                   MqQuestioncount = 0;
      //                   this.setSessionAttribute("IntentStateForMQ", "");
      //                   //If the user answers correct, then here we're checking the question index to check the question 
      //                   //belongs to which player, and based on that we're throwing next question to next player.
      //                   if (correctAnswerFlag) {
      //                     this.setSessionAttribute("AnsIntentStateForMQ", "AnswerFlag");
      //                     console.log("correct answer " + this.getSessionAttribute("currentQuestionIndex"));
      //                     if (this.getSessionAttribute("numberOfPlayers") === 2) {
      //                       // if (this.getSessionAttribute("currentPlayerName") === this.getSessionAttribute("secondPlayerName")) {
      //                       if ((this.getSessionAttribute("currentQuestionIndex")) % 2 === 0) {
      //                         // secondPlayerScore += 1;
      //                         console.log("correct score of player2");
      //                         this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("secondPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("secondPlayerSet",
      //                           this.getSessionAttribute("secondPlayerName") +
      //                           ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("firstPlayerIntent");
      //                       } else {
      //                         // firstPlayerScore += 1;
      //                         console.log("correct score of player1");
      //                         this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("firstPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("firstPlayerSet",
      //                           this.getSessionAttribute("firstPlayerName") +
      //                           ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("secondPlayerIntent");
      //                       }
      //                     } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
      //                       console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
      //                       if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 0) {
      //                         // thirdPlayerScore += 1;
      //                         // console.log("correct score of player3", thirdPlayerScore);
      //                         this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("thirdPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerSet",
      //                           this.getSessionAttribute("thirdPlayerName") +
      //                           ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
      //                         );
      //                         await this.toIntent("firstPlayerIntent");
      //                       }
      //                       else if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 1) {
      //                         // firstPlayerScore += 1;
      //                         // console.log("correct score of player1", firstPlayerScore);
      //                         this.setSessionAttribute("firstPlayerTotalQue",
      //                           this.getSessionAttribute("firstPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("firstPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("firstPlayerSet",
      //                           this.getSessionAttribute("firstPlayerName") +
      //                           ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("secondPlayerIntent");
      //                       } else {
      //                         // secondPlayerScore += 1;
      //                         // console.log("correct score of player2", secondPlayerScore);
      //                         this.setSessionAttribute("secondPlayerTotalQue",
      //                           this.getSessionAttribute("secondPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("secondPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("secondPlayerSet",
      //                           this.getSessionAttribute("secondPlayerName") +
      //                           ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("thirdPlayerIntent");
      //                       }
      //                     } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
      //                       console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
      //                       if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 0) {
      //                         // thirdPlayerScore += 1;
      //                         // console.log("correct score of player1", thirdPlayerScore);
      //                         this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("thirdPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerSet",
      //                           this.getSessionAttribute("thirdPlayerName") +
      //                           ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
      //                         );
      //                         await this.toIntent("firstPlayerIntent");
      //                       }
      //                       else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 1) {
      //                         // firstPlayerScore += 1;
      //                         // console.log("correct score of player2", firstPlayerScore);
      //                         this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("firstPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("firstPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("firstPlayerSet",
      //                           this.getSessionAttribute("firstPlayerName") +
      //                           ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("secondPlayerIntent");
      //                       } else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 2) {
      //                         // fourthPlayerScore += 1;
      //                         // console.log("correct score of player3", fourthPlayerScore);
      //                         this.setSessionAttribute("fourthPlayerTotalQue", this.getSessionAttribute("fourthPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("fourthPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("fourthPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("fourthPlayerSet",
      //                           this.getSessionAttribute("fourthPlayerName") +
      //                           ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
      //                         );
      //                         await this.toIntent("thirdPlayerIntent");
      //                       } else {
      //                         // secondPlayerScore += 1;
      //                         // console.log("correct score of player4", secondPlayerScore);
      //                         this.setSessionAttribute("secondPlayerCorrectAnsQue",
      //                           this.getSessionAttribute("secondPlayerCorrectAnsQue") + 1);
      //                         this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("secondPlayerSet",
      //                           this.getSessionAttribute("secondPlayerName") +
      //                           ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("fourthPlayerIntent");
      //                       }
      //                     }
      //                   } else {  ///wrong answer starting
      //                     //If the user answers wrong, then here we're checking the question index to check the question belongs to which player, 
      //                     //and based on that we're throwing next question to next player.
      //                     console.log("wrong answer " + this.getSessionAttribute("currentQuestionIndex"));
      //                     console.log("speech output -> " + this.getSessionAttribute("speechOutput"));
      //                     this.setSessionAttribute("AnsIntentStateForMQ", "AnswerFlag");
      //                     if (this.getSessionAttribute("numberOfPlayers") === 2) {
      //                       if (this.getSessionAttribute("currentQuestionIndex") % 2 === 0) {
      //                         // secondPlayerWScore += 1;
      //                         // secondPlayerScore = secondPlayerWScore;
      //                         console.log("wrong score of player2");
      //                         this.setSessionAttribute("secondPlayerTotalQue",
      //                           this.getSessionAttribute("secondPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("secondPlayerWrongAnsQue",
      //                           this.getSessionAttribute("secondPlayerWrongAnsQue") + 1
      //                         );
      //                         this.setSessionAttribute("secondPlayerSet",
      //                           this.getSessionAttribute("secondPlayerName") +
      //                           ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("firstPlayerIntent");
      //                       } else {
      //                         // firstPlayerWScore += 1;
      //                         // firstPlayerScore = firstPlayerWScore;
      //                         console.log("wrong score of player1");
      //                         this.setSessionAttribute("firstPlayerTotalQue",
      //                           this.getSessionAttribute("firstPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("firstPlayerWrongAnsQue",
      //                           this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("firstPlayerSet",
      //                           this.getSessionAttribute("firstPlayerName") +
      //                           ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("secondPlayerIntent");
      //                       }
      //                     } else if (this.getSessionAttribute("numberOfPlayers") === 3) {
      //                       console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
      //                       if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 0) {
      //                         // thirdPlayerWScore += 1;
      //                         // thirdPlayerScore = thirdPlayerWScore;
      //                         // console.log("wrong score of player3", thirdPlayerScore);
      //                         this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerWrongAnsQue",
      //                           this.getSessionAttribute("thirdPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerSet",
      //                           this.getSessionAttribute("thirdPlayerName") +
      //                           ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
      //                         );
      //                         await this.toIntent("firstPlayerIntent");
      //                       } else if ((this.getSessionAttribute("currentQuestionIndex")) % 3 === 1) {
      //                         // firstPlayerWScore += 1;
      //                         // firstPlayerScore = firstPlayerWScore;
      //                         // console.log("wrong score of player1", firstPlayerScore);
      //                         this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("firstPlayerWrongAnsQue",
      //                           this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("firstPlayerSet",
      //                           this.getSessionAttribute("firstPlayerName") +
      //                           ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("secondPlayerIntent");
      //                       } else {
      //                         // secondPlayerWScore += 1;
      //                         // secondPlayerScore = secondPlayerWScore;
      //                         // console.log("wrong score of player2", secondPlayerScore);
      //                         this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("secondPlayerWrongAnsQue",
      //                           this.getSessionAttribute("secondPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("secondPlayerSet",
      //                           this.getSessionAttribute("secondPlayerName") +
      //                           ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("thirdPlayerIntent");
      //                       }
      //                     } else if (this.getSessionAttribute("numberOfPlayers") === 4) {
      //                       console.log("cuurent q inde -> " + this.getSessionAttribute("currentQuestionIndex"));
      //                       if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 0) {
      //                         // thirdPlayerWScore += 1;
      //                         // thirdPlayerScore = thirdPlayerWScore
      //                         // console.log("wrong score of player1", thirdPlayerScore);
      //                         this.setSessionAttribute("thirdPlayerTotalQue", this.getSessionAttribute("thirdPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerWrongAnsQue",
      //                           this.getSessionAttribute("thirdPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("thirdPlayerSet",
      //                           this.getSessionAttribute("thirdPlayerName") +
      //                           ", " + this.getSessionAttribute("thirdPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("thirdPlayerSkipedQue")
      //                         );
      //                         await this.toIntent("firstPlayerIntent");
      //                       }
      //                       else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 1) {
      //                         // firstPlayerWScore += 1;
      //                         // firstPlayerScore = firstPlayerWScore;
      //                         // console.log("wrong score of player2", firstPlayerScore);
      //                         this.setSessionAttribute("firstPlayerTotalQue", this.getSessionAttribute("firstPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("firstPlayerWrongAnsQue",
      //                           this.getSessionAttribute("firstPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("firstPlayerSet",
      //                           this.getSessionAttribute("firstPlayerName") +
      //                           ", " + this.getSessionAttribute("firstPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("firstPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("secondPlayerIntent");
      //                       } else if ((this.getSessionAttribute("currentQuestionIndex")) % 4 === 2) {
      //                         // fourthPlayerWScore += 1;
      //                         // fourthPlayerScore = fourthPlayerWScore;
      //                         // console.log("wrong score of player3", fourthPlayerScore);
      //                         this.setSessionAttribute("fourthPlayerTotalQue", this.getSessionAttribute("fourthPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("fourthPlayerWrongAnsQue",
      //                           this.getSessionAttribute("fourthPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("fourthPlayerSet",
      //                           this.getSessionAttribute("fourthPlayerName") +
      //                           ", " + this.getSessionAttribute("fourthPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("fourthPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("fourthPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("fourthPlayerSkipedQue")
      //                         );
      //                         await this.toIntent("thirdPlayerIntent");
      //                       } else {
      //                         // secondPlayerWScore += 1;
      //                         // secondPlayerScore = secondPlayerWScore;
      //                         // console.log("wrong score of player4", secondPlayerScore);
      //                         this.setSessionAttribute("secondPlayerTotalQue", this.getSessionAttribute("secondPlayerTotalQue") + 1);
      //                         this.setSessionAttribute("secondPlayerWrongAnsQue",
      //                           this.getSessionAttribute("secondPlayerWrongAnsQue") + 1);
      //                         this.setSessionAttribute("secondPlayerSet",
      //                           this.getSessionAttribute("secondPlayerName") +
      //                           ", " + this.getSessionAttribute("secondPlayerTotalQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerCorrectAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerWrongAnsQue") +
      //                           ", " + this.getSessionAttribute("secondPlayerSkipedQue")
      //                         )
      //                         await this.toIntent("fourthPlayerIntent");
      //                       }
      //                     }
      //                   }  //ending wrong answers
      //                 }
      //                 //end of multiplayer quiz
    
      //                 // if (this.getSessionAttribute("twoPlayersQuiz")) {
      //                 //   if (
      //                 //     this.getSessionAttribute("currentQuestionIndex") % 2 ===
      //                 //     0
      //                 //   ) {
      //                 //     if (correctAnswerFlag) {
      //                 //       firstPlayerScore += 1;
      //                 //     }
      //                 //     console.log(
      //                 //       "Player 2's questionIndex ->" +
      //                 //       this.getSessionAttribute("currentQuestionIndex")
      //                 //     );
      //                 //     console.log(
      //                 //       "Plater 2's score ->" +
      //                 //       this.getSessionAttribute("secondPlayerScore")
      //                 //     );
      //                 //     await this.toIntent("SecondPlayer");
      //                 //   } else {
      //                 //     if (correctAnswerFlag) {
      //                 //       secondPlayerScore += 1;
      //                 //     }
      //                 //     console.log(
      //                 //       "Player 1's questionIndex ->" +
      //                 //       this.getSessionAttribute("currentQuestionIndex")
      //                 //     );
      //                 //     console.log(
      //                 //       "Plater 1's score ->" +
      //                 //       this.getSessionAttribute("firstPlayerScore")
      //                 //     );
      //                 //     await this.toIntent("FirstPlayer");
      //                 //   }
      //                 // }
      //                 else {
      //                   if (
      //                     this.getSessionAttribute("currentQuestionIndex") <= quslen
      //                   ) {
      //                     await this.toIntent("getNextQuestion");
      //                     //		}
      //                     //		 else {
      //                     // this.attributes['speechOutput'] = " - ";
      //                     // app.toIntent('endOfTest');
      //                     //		}
      //                   } else {
      //                     this.setSessionAttribute(
      //                       "speechOutput",
      //                       this.getSessionAttribute("speechOutput") +
      //                       " End of this quiz "
      //                     );
      //                     console.log('speechOutput -> Result => ' + this.getSessionAttribute("score"));//Meenu session end change
      //                     await this.toIntent("endOfTest");
      //                   }
      //                 }
      //                 console.log("numberOfPlayers : " + this.getSessionAttribute("numberOfPlayers"));
      //               }
      //             }
      //           }
      //           else {
      //             console.log('Unfortunately, I have to say that, u r wrong');
      //             speechOutput = "To answer this question, please say one of these options, " + opt +
      //               " , or You could also say <s> repeat Question </s> <s>Repeat Options</s>  <s>Next question</s>";
      //             repromptSpeech = speechOutput;
      //             this.ask(speechOutput, repromptSpeech, repromptSpeech, goodByeMsg);
      //           }
      //           //End -> For testing that the option user is telling is exist in options or not
      //         }
      //       } else {
      //         console.log("Intents state -> " + this.getSessionAttribute("IntentState"));
      //         console.log(" state -> " + this.getSessionAttribute("state"));
      //         console.log("user state -> " + this.getSessionAttribute("UserState"));
      //         speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
      //         repromptSpeech = speechOutput;
      //         this.ask(
      //           speechOutput, repromptSpeech, repromptSpeech, goodByeMsg  //meenu
      //         );
      //       }
      //     }
      //   }
      // },
    
      // getNextQuestion: async function () {
      //   console.log("Starting getNextQuestion");
      //   this.setSessionAttribute("IntentState", "getNextQuestion");
      //   var data = [];
      //   var qId = [];
      //   var completedCount = this.getSessionAttribute("TestIndex");
      //   //if the compleated question count is equal to configured count, exit test
      //   var speechoutput = "", repromptSpeech = "";
      //   if (!this.getSessionAttribute("UserId")) {
      //     speechoutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
      //     repromptSpeech = speechoutput;
      //     this.ask(
      //       speechoutput, repromptSpeech, repromptSpeech, goodByeMsg
      //     );
      //   } else {
      //     if (this.getSessionAttribute("TopicId")) {
      //       if (completedCount >= this.getSessionAttribute("numberOfQuestionsToAccess")) {
      //         console.log("end");
      //         await this.toIntent("endOfTest");
      //       }
      //       // if (completedCount >= constants.TestSize) {
      //       //       console.log("end");
      //       //       await this.toIntent("endOfTest");
      //       //     } 
      //       else {
      //         console.log("inside getnextquestion block");
      //         // var rem = completedCount % constants.questionsToRead;  //for multiplayer quiz commented
      //         var rem = completedCount % this.getSessionAttribute("numberOfQuestionsToAccess");
      //         if (
      //           rem === 0 &&
      //           completedCount != 0 &&
      //           // completedCount <= constants.TestSize   //for multiplayer quiz commented
      //           completedCount <= this.getSessionAttribute("numberOfQuestionsToAccess")
      //         ) {
      //           // this.attributes['currentQuestionIndex'] = 0;
      //           this.setSessionAttribute("currentQuestionIndex", 0);
      //           console.log("db connection for nxt qus set");
      //           console.log("Inside if,  Reminder  : " + rem);
      //           // var testTopic = this.attributes['Topic'];
      //           var testTopic = this.getSessionAttribute("Topic");
      //           //reading next set of questions from DB
      //           //Read max id from Sesson atribute
      //           // var maxId = this.attributes['maxQuestionID'];
      //           var maxId = this.getSessionAttribute("maxQuestionID");
      //           if (maxId === null) {
      //             maxId = 0;
      //           }
      //           // var queryString = 'call SP_accessQuestion_alexa_Dev_2018("' + this.attributes['TopicId'] + '", "' + this.attributes['challengeLevelInd'] + '","' + maxId + '","' + constants.questionsToRead + '")';
      //           var queryString =
      //             'call SP_accessQuestion_alexa_Dev_2018("' +
      //             this.getSessionAttribute("TopicId") +
      //             '", "' +
      //             this.getSessionAttribute("challengeLevelInd") +
      //             '","' +
      //             maxId +
      //             '","' +
      //             // constants.questionsToRead +    //for multiplayer quiz commented
      //             this.getSessionAttribute("numberOfQuestionsToAccess") +
      //             '")';
      //           console.log("Query String -> " + queryString);
      //           console.log(
      //             "completed Count -> " +
      //             completedCount +
      //             " Reading next set of questions"
      //           );
      //           let rows;
      //           // Database.execute(
      //           //     database => database.query(queryString)
      //           //         .then(r => {
      //           //             rows = r;
      //           //         })
      //           // ).then(() => {
    
      //           // }).catch(err => {
      //           //     console.log('DB Error: ', err);
      //           //     pointerToThis.tell(constants.errorPrompt);
      //           // });
      //           rows = await functionCalls.callAPI(queryString);
      //           data = rows[0];
      //           for (var i in data) {
      //             qId[i] = data[i].questionId;
      //           }
      //           var maxQuestionID = arrayFunctions.maxAaaryValue(qId);
      //           console.log("Maximum Question Id-> " + maxQuestionID);
      //           console.log("Qiestion Id " + qId);
      //           //Add o session attribute
      //           // this.attributes['maxQuestionID'] = maxQuestionID;
      //           this.setSessionAttribute("maxQuestionID", maxQuestionID);
    
      //           // this.attributes['questions'] = "";
      //           this.setSessionAttribute("questions", "");
    
      //           //nextQuesIndex = this.attributes['currentQuestionIndex'] = 0;
      //           // console.log('Question data erased --->> ' + this.attributes['questions'])
      //           console.log(
      //             "Question data erased --->> " +
      //             this.getSessionAttribute("questions")
      //           );
    
      //           //assign the new list of questions retrived
      //           questionData = arrayFunctions.shuffleArray(data);
      //           // this.attributes['questions'] = questionData;
      //           this.setSessionAttribute("questions", questionData);
    
      //           // if (this.attributes['questions'].length === 0) {
      //           if (this.getSessionAttribute("questions").length === 0) {
      //             console.log("loadQuestions  did not return anything!!!");
      //             await this.toIntent("endOfTest");
      //           } else {
      //             await this.toIntent("ContinueGetNextQuestion");
      //           }
      //         } else {
      //           // this.emitWithState('ContinueGetNextQuestion');
      //           await this.toIntent("ContinueGetNextQuestion");
      //         }
      //       }
      //     } else {
      //       speechoutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>"; //meenu
      //       repromptSpeech = speechoutput;
      //       this.ask(
      //         speechoutput, repromptSpeech, repromptSpeech, goodByeMsg // meenu
      //       );
      //     }
      //   }
      // },
    // End of modularCode4.js

    //starting of modularCode5.js
      // ContinueGetNextQuestion: async function () {
      //   this.setSessionAttribute('IntentState', 'ContinueGetNextQuestion');  //Meenu
      //   this.setSessionAttribute('ImageContainer', 'No');
      //   this.setSessionAttribute('AudioContainer', 'No');
    
      //   nextQuesIndex = this.getSessionAttribute("currentQuestionIndex");
      //   questionData = this.getSessionAttribute("questions");
    
      //   var completedCount = this.getSessionAttribute("TestIndex");
      //   var speechOutput = "", repromptSpeech = "", images = this;
      //   //if the compleated question count is equal to configured count, exit test
      //   console.log("completedCount -> " + completedCount);
      //   if (!this.getSessionAttribute("UserId")) {
      //     speechOutput = "Looks like you are not identified!, Please say, my pin is, and your pin to identify"; //meenu
      //     repromptSpeech = speechOutput;
      //     this.ask(
      //       speechOutput, repromptSpeech, repromptSpeech, goodByeMsg
      //     );
      //   } else {
      //     if (this.getSessionAttribute("TopicId")) {
      //       // if (completedCount >= constants.TestSize) {  //For multiplayer quiz commented
      //       if (completedCount >= this.getSessionAttribute("numberOfQuestionsToAccess")) {
      //         console.log("end");
      //         await this.toIntent("endOfTest");
      //       } else {
      //         console.log(questionData + " and index : " + nextQuesIndex);
      //         console.log("QsTN : " + questionData[nextQuesIndex]);
      //         console.log("QsTN text : " + questionData[nextQuesIndex].text);
    
      //         if ((questionData[nextQuesIndex].externalReference) !== null &&
      //           (questionData[nextQuesIndex].externalReference) !== undefined) {
      //           if ((questionData[nextQuesIndex].externalReference).includes('.png') ||
      //             (questionData[nextQuesIndex].externalReference).includes('.jpg')) {
      //             console.log(' text -> ' + questionData[nextQuesIndex].text);
      //             console.log(' text -> ' + questionData[nextQuesIndex].questionId);
      //             this.setSessionAttribute('ImageContainer', 'Yes');
      //             console.log('image3');
      //           } else {
      //             this.setSessionAttribute('ImageContainer', 'No');
      //             console.log('image4');
      //           }
      //           if ((questionData[nextQuesIndex].externalReference).includes('.mp3')) {
      //             console.log('AudioContainer3');
      //             this.setSessionAttribute('AudioContainer', 'Yes');
      //           } else {
      //             console.log('AudioContainer4');
      //             this.setSessionAttribute('AudioContainer', 'No');
      //           }
      //         }
    
      //         if (this.getSessionAttribute('ImageContainer') === 'Yes') {
      //           // let title = '';
      //           // let content = '';
      //           console.log('image6');
      //           console.log(" image url -> " + questionData[nextQuesIndex].externalReference);
      //           imageUrl = questionData[nextQuesIndex].externalReference;
      //           speechOutput =
      //             ` -  <prosody rate="` +
      //             constants.speachSpeed +
      //             `">` +
      //             questionData[nextQuesIndex].text +
      //             ". <s> Options are </s> ";
      //           images = this.showImageCard(title, content, imageUrl);
      //         } else if (this.getSessionAttribute('AudioContainer') === 'Yes') {
      //           console.log('AudioContainer5');
      //           speechOutput =
      //             ` -  <prosody rate="` +
      //             constants.speachSpeed +
      //             `">` +
      //             questionData[nextQuesIndex].text +
      //             "<audio src= '" + questionData[nextQuesIndex].externalReference + "'/>" +
      //             " <s> Options are </s> ";
      //         } else {
      //           speechOutput =
      //             ` -  <prosody rate="` +
      //             constants.speachSpeed +
      //             `">` +
      //             questionData[nextQuesIndex].text +
      //             " <s> Options are </s> ";
      //         }
      //         //Assigning the next question Options
    
      //         var rowData = JSON.parse(
      //           "[" + questionData[nextQuesIndex].options + "]"
      //         );
      //         var opt = " ";
      //         for (var key = 0; key < rowData.length; key++) {
      //           var dt = rowData[key];
      //           for (var i in dt) {
      //             if (dt[i] != "") {
      //               speechOutput =
      //                 speechOutput +
      //                 (`<s>` +
      //                   i +
      //                   `</s>` +
      //                   " - " +
      //                   dt[i] +
      //                   ',<break time="' +
      //                   constants.pauseTime +
      //                   '"/> ');
      //               opt = opt + i + `,  ` + ',<break time="' +
      //                 constants.pauseTime +
      //                 '"/> ';   //options choices
      //               answerText[i] = dt[i];
      //             }
      //           }
      //         }
      //         speechOutput += `</prosody>`;
      //         console.log('options choices -> ' + opt);
      //         this.setSessionAttribute("optionsChoice", opt);
      //         console.log("Next Question is : " + speechOutput);
    
      //         /////reset he current index to zer
      //         this.setSessionAttribute("currentQuestionIndex", nextQuesIndex);
      //         //populate the answer for the first row in the questions array
      //         this.setSessionAttribute(
      //           "correctAnswerText",
      //           questionData[nextQuesIndex].answer
      //         );
      //         this.setSessionAttribute(
      //           "questionId",
      //           questionData[nextQuesIndex].questionId
      //         );
      //         this.setSessionAttribute(
      //           "questionWeightage",
      //           questionData[nextQuesIndex].weightage
      //         );   //Weightage
      //         this.setSessionAttribute(
      //           "challengeLevelOfEachQue",
      //           questionData[nextQuesIndex].challenge
      //         );   //To know the challenge level of each question after 1st question
      //         console.log('questionWeightage -> ' + this.getSessionAttribute('questionWeightage'));
      //         //incrementing the testIndex by one
      //         this.setSessionAttribute(
      //           "UserState",
      //           constants.UserState.QuestionsRead
      //         );
      //         this.setSessionAttribute("state", constants.states.QUESTIONANSWER);
      //         // VoiceLabs.track(this.event.session, 'ContinueGetNextQuestion', null, null, (error, response) => {
    
      //         console.log(
      //           "After " +
      //           this.getSessionAttribute("speechOutput") +
      //           ` Next Question ` +
      //           speechOutput,
      //           " Repeating the same question - " +
      //           speechOutput +
      //           " Say - the answer is " + opt + " or say Next question to skip this question."
      //         );
    
      //         //Meenu changes session
      //         var UserId = this.getSessionAttribute("UserId");
      //         var TopicId = this.getSessionAttribute("TopicId");
      //         var questionsTaken = this.getSessionAttribute("questionsTaken");
      //         var score = this.getSessionAttribute("score");
      //         var correctAnswerScore = this.getSessionAttribute("correctAnswerScore");
      //         var correctAnsweredQuestionId = this.getSessionAttribute("correctAnsweredQuestionId");
      //         var wrongAnsweredQuestionId = this.getSessionAttribute("wrongAnsweredQuestionId");
      //         var notAttemptedQuestionId = this.getSessionAttribute("notAttemptedQuestionId");
      //         var correctAnsweredWeightage = this.getSessionAttribute("correctAnsweredWeightage");
      //         var wrongAnsweredWeightage = this.getSessionAttribute("wrongAnsweredWeightage");
      //         var notAttemptedQuestionWeightage = this.getSessionAttribute("notAttemptedQuestionWeightage");
      //         var challengeAdvanceCountQuestions = this.getSessionAttribute("challengeAdvanceCountQuestions");
      //         var challengeAdvanceCountCorrectAns = this.getSessionAttribute("challengeAdvanceCountCorrectAns");
      //         var challengeAdvanceCountWrongAns = this.getSessionAttribute("challengeAdvanceCountWrongAns");
      //         var challengeAdvanceCountNotAttemptedAns = this.getSessionAttribute("challengeAdvanceCountNotAttemptedAns");
      //         var challengeIntermediateCountQuestions = this.getSessionAttribute("challengeIntermediateCountQuestions");
      //         var challengeIntermediateCountCorrectAns = this.getSessionAttribute("challengeIntermediateCountCorrectAns");
      //         var challengeIntermediateCountWrongAns = this.getSessionAttribute("challengeIntermediateCountWrongAns");
      //         var challengeIntermediateCountNotAttemptedAns = this.getSessionAttribute("challengeIntermediateCountNotAttemptedAns");
      //         var challengeStarterCountQuestions = this.getSessionAttribute("challengeStarterCountQuestions");
      //         var challengeStarterCountCorrectAns = this.getSessionAttribute("challengeStarterCountCorrectAns");
      //         var challengeStarterCountWrongAns = this.getSessionAttribute("challengeStarterCountWrongAns");
      //         var challengeStarterCountNotAttemptedAns = this.getSessionAttribute("challengeStarterCountNotAttemptedAns");
      //         var TestIndex = this.getSessionAttribute("TestIndex");
      //         var courseId = this.getSessionAttribute("courseId");
      //         var organizationId = this.getSessionAttribute("organizationId");
      //         var section = this.getSessionAttribute("section");
      //         var testType = this.getSessionAttribute("testType");
      //         var roleName = this.getSessionAttribute("roleName");
      //         var firstPlayerSet = this.getSessionAttribute("firstPlayerSet");
      //         var secondPlayerSet = this.getSessionAttribute("secondPlayerSet");
      //         var thirdPlayerSet = this.getSessionAttribute("thirdPlayerSet");
      //         var fourthPlayerSet = this.getSessionAttribute("fourthPlayerSet");
      //         var quizType = this.getSessionAttribute("quizType");
      //         await functionCalls.callSessionFunc(this, UserId, TopicId, questionsTaken, score, correctAnswerScore,
      //           correctAnsweredQuestionId, wrongAnsweredQuestionId, notAttemptedQuestionId,
      //           correctAnsweredWeightage, wrongAnsweredWeightage, notAttemptedQuestionWeightage,
      //           challengeAdvanceCountQuestions, challengeAdvanceCountCorrectAns,
      //           challengeAdvanceCountWrongAns, challengeAdvanceCountNotAttemptedAns,
      //           challengeIntermediateCountQuestions, challengeIntermediateCountCorrectAns,
      //           challengeIntermediateCountWrongAns, challengeIntermediateCountNotAttemptedAns,
      //           challengeStarterCountQuestions, challengeStarterCountCorrectAns,
      //           challengeStarterCountWrongAns, challengeStarterCountNotAttemptedAns, TestIndex,
      //           courseId, organizationId, section, testType, roleName, firstPlayerSet, secondPlayerSet,
      //           thirdPlayerSet, fourthPlayerSet, quizType);
    
      //         console.log('speechOutput -> ' + speechOutput + ' Result => ' + this.getSessionAttribute("score"));//Meenu session end change
      //         repromptSpeech = " Repeating the same question - " +
      //           speechOutput +
      //           " Say - the answer is " + opt + " or say Next question to skip this question."; //meenu
      //         if (this.getSessionAttribute('imageExplanation') !== null &&
      //           this.getSessionAttribute('imageExplanation') !== undefined) {
      //           imageUrl = this.getSessionAttribute('imageExplanation');
      //           images = this.showImageCard(title, content, imageUrl);
      //           console.log(' image url -> ' + imageUrl);
      //         }
      //         else {
      //           this.setSessionAttribute('imageExplanation', null)
      //           images = this;
      //         }
    
    
      //         //Explaination for multiplayer quiz
      //         if (this.getSessionAttribute("MultiplayerQuizIntent")) {
      //           console.log(" current p name -> " + this.getSessionAttribute("currentPlayerName"));
      //           if (this.getSessionAttribute("IntentStateForMQ") === "showingCorrectAnswer") {
      //             images.ask(
      //               this.getSessionAttribute("speechOutput") +
      //               ` Next Question for ` +
      //               this.getSessionAttribute("currentPlayerName") +
      //               speechOutput,
      //               repromptSpeech, repromptSpeech, goodByeMsg
      //             );
      //           } else {
      //             images.ask(
      //               this.getSessionAttribute("speechOutput") +
      //               ` Question for ` +
      //               this.getSessionAttribute("currentPlayerName") +
      //               speechOutput,
      //               repromptSpeech, repromptSpeech, goodByeMsg
      //             );
      //           }
      //         } else {
      //           images.ask(
      //             this.getSessionAttribute("speechOutput") +
      //             ` Next Question ` +
      //             speechOutput,
      //             repromptSpeech, repromptSpeech, goodByeMsg
      //           );
      //         }
      //         // });
      //       }
      //     } else {
      //       speechOutput = "<s>Select topic </s> to open a Quiz, Say <s>Open and topic Name</s> or, say <s>list topics  </s>";
      //       repromptSpeech = speechOutput;
      //       this.ask(
      //         speechOutput, repromptSpeech, repromptSpeech, goodByeMsg // meenu
      //       );
      //     }
      //   }
      // },
}
var constants = Object.freeze({
  appId: 'amzn1.ask.skill.87a20301-9dbf-4bd9-ae4c-10f330e1dc03',
  dynamoDBTableName: 'User',
  questionsToRead: 10, //this should be less than or equal to TestSize
  minNumberOfQuestionsToStartTest: 10, // this determines if the test can be used. should be greater than or equal to TestSize value
  TestSize: 10,
  pauseTime: '0.5s',
  speachSpeed: 'medium',
  topicSimilarityAllowed: 50, //this is the percentage value for percentage of confidance on text match
  // Skill States
  VoiceLabsCode: '7266f010-6884-11a7-11ce-02f814b60257',
  states: {
    ONBOARDING: '',
    MAIN: '_MAIN',
    QUESTIONANSWER: '_QUESTIONANSWER'
  },

  gameRules: 'Here are the quiz rules for multiplayer quiz, first question will populate for first ' +
    'user if he answers correct 1 ponit will be added to his points and suppose if answers wrong then ' +
    'the question will pass to the next player in the quiz and no marks will be given to the first player, ' +
    ' same applies to other players and other questions also. Are you ready to take this quiz ? ' +
    'If yes, please say list topics to choose one of the topic from your subscribed topics',
  exitMessage: ' <prosody rate="medium">Good Bye!</prosody> ',
  errorPrompt: ' <prosody rate="medium"> Sorry, there was an issue with the Skill, please try again Later. </prosody> ',
  HowToUsePrompt: ' <prosody rate="medium"> Say <s>How to Use</s>  or, <s>What are my options!</s> </prosody> ',
  HelpPrompt: ' Say <s>Help Me!</s> ',
  TopicPromptTxt: ' Say <s> Open and the Topic Name</s> Or, <s> Find, and the name of the Topic!</s> ',
  UserState: {
    NewSession: 'NewSession',
    UserSelected: 'UserSelected',
    QuizType: 'MultiplayerQuiz',
    NoOfPlayersSelected: 'NoOfPlayersSelected',
    FirstPlayerNameGiven: 'FirstPlayerNameGiven',
    SecondPlayerNameGiven: 'SecondPlayerNameGiven',
    ThirdPlayerNameGiven: 'ThirdPlayerNameGiven',
    FourthPlayerNameGiven: 'FourthPlayerNameGiven',
    TopicNameSelected: 'TopicNameSelected',
    ChallangeLevelSelected: 'ChallangeLevelSelected',
    QuestionsRead: 'QuestionsRead',
    EndOfTest: 'EndOfTest'
  }

});

module.exports = constants;